import codecs
import os
import traceback

from PIL import ImageFile

from nut.config import cms
from nut.utils.img import ImgResizePolicy


def _get_folder_path(base_path, folder_name):
    folder_path = os.path.join(base_path, folder_name)
    folder_real_path = os.path.join(cms.CMS_STATIC_DIR_PATH, 'images',
                                    folder_path)
    if not os.path.exists(folder_real_path):
        os.makedirs(folder_real_path)
    return folder_path


def handle(data, filename, item_type, item_id,
           action=b'save', check_img=0,
           resize_category=b'banner'):
    if action == b'save':
        return save_img(data, filename, item_type, item_id)

    elif action == b'resize':
        return resize_imgs(data, filename, item_type,
                           item_id, check_img,
                           resize_category)
    elif action == b'resize_with_size':
        return resize_imgs(data, filename, item_type,
                           item_id, check_img,
                           resize_category, True)
    else:
        return [b'500', b'Action Not Support.']


def save_img(data, filename, item_type, item_id):
    img_path = None
    try:
        _, ext = os.path.splitext(filename)
        if not ext:
            p = ImageFile.Parser()
            p.feed(data)
            img = p.close()
            ext = img.format
        else: ext = ext.decode('utf-8')

        img_path = _get_img_path(filename, item_type, item_id)
        if not img_path.endswith(ext):
            img_path = '%s.%s' % (img_path, ext.lower())

        _dump_into_file(data, img_path)
    except:
        traceback.print_exc()
    finally:
        return [b'200', b'img/' + img_path.encode('utf-8')]


def resize_imgs(data, filename, item_type, item_id, check_img, resize_category, isWithSize=False):
    p = ImageFile.Parser()
    p.feed(data)
    img = p.close()
    policy = ImgResizePolicy.getPolicy(resize_category)

    if policy is None:
        return [b'401', b'Policy is not found.']

    if check_img :
        c = policy.checkImg(img)
        if c < 0: return [str(-c).encode('ascii'), b'Invalid image']

    # save base.
    img_path = _get_img_path(filename, item_type, item_id)
    _dump_into_file(data, img_path)

    imgs_with_size = {}
    try:
        # resize and save resizes.
        images = policy.resize(img)
        full_path = _get_full_path(img_path)
        i = full_path.rindex('.')
        p, f = full_path[:i], full_path[i:]
        for _img in images:
            if isinstance(_img, tuple): 
                prefix, _img   = _img
                fname = '%s-%s%s' % (p, prefix, f)
            else: 
                prefix = _img.width
                fname = '%s-w%d%s' % (p, prefix, f)

            _img.save(fname, _img.format, quality=policy.quality)
            _img.close()
            if isWithSize:
                _img = {
                    'imgURL': b'img/' + policy.get_img_path(_img, img_path).encode('utf-8'),
                    'width': _img.width,
                    'height': _img.height
                }
                imgs_with_size[prefix] = _img
    except:
        traceback.print_exc()
    finally:
        img.close()

    if isWithSize:
        return [b'200', bytes(str(imgs_with_size).encode('utf-8'))]
    img_path = policy.get_img_path(img, img_path)

    return [b'200', b'img/' + img_path.encode('utf-8')]


def _get_img_path(filename, item_type, item_id):
    _, file_extension = os.path.splitext(filename)
    fname = (codecs.encode(os.urandom(cms.IMG_FILENAME_LENGTH), 'hex') +
             file_extension)
    item_type_folder = _get_folder_path('',
                                        item_type.decode('utf-8'))
    item_folder = _get_folder_path(item_type_folder,
                                   item_id.decode('utf-8'))

    img_path = os.path.join(item_folder, fname.decode('utf-8'))

    return img_path


def _get_full_path(img_path):
    return os.path.join(cms.CMS_STATIC_DIR_PATH, 'images', img_path)


def _dump_into_file(data, img_path):
    with open(_get_full_path(img_path), 'wb') as f:
        f.write(data)

