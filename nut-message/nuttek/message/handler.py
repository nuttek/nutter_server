import json
import redis
import time

from nuttek import config
from nuttek.db.env import FeedSession
from nuttek.db.feed_models import Timeline, Comments
import logging

dm_rds_cli = redis.Redis(
    host=config.MESSAGE_REDIS_SERVERS['direct_message']['host'],
    port=config.MESSAGE_REDIS_SERVERS['direct_message']['port'],
    db=config.MESSAGE_REDIS_SERVERS['direct_message']['db'])
gm_rds_cli = redis.Redis(
    host=config.MESSAGE_REDIS_SERVERS['group_message']['host'],
    port=config.MESSAGE_REDIS_SERVERS['group_message']['port'],
    db=config.MESSAGE_REDIS_SERVERS['group_message']['db'])

follower_rds_cli = redis.Redis(
    host=config.MESSAGE_REDIS_SERVERS['follower']['host'],
    port=config.MESSAGE_REDIS_SERVERS['follower']['port'],
    db=config.MESSAGE_REDIS_SERVERS['follower']['db'])

DM_KEY = 'dm:%s'
GM_KEY = 'gm:%s'
MSG_KEY = '%s:msg:%d'
USERS_KEY = '%s:users'
UNREAD_DM_KEY = 'user:%d:unread_dms'
UNREAD_GM_KEY = 'user:%d:unread_gms'
UNREAD_COMMENT_KEY = 'user:%d:unread_comments'
UNREAD_LIKE_KEY = 'user:%d:unread_likes'
NEW_FOLLOWER_KEY = 'user:%d:followers'
NEW_FOLLOWER_IDS_KEY = 'user:%d:followers:ids'
NEW_FOLLOWER_IDS_KEY_SEQ = '%s:seq:%d'


class MessageHandler(object):
    def handle(self, cmd, *args):
        method = getattr(self, 'handle_%s' % cmd.decode('utf-8'), None)
        if method:
            args = tuple([arg.decode('utf-8') for arg in args])
            data = method(*args)
            return [b'200', json.dumps(data).encode('utf-8')]
        else:
            return [b'404']

    def handle_read_direct_message(self, dm_id, start_time):
        logging.debug("start_time %s" % start_time)
        return self._handle_read(dm_rds_cli, DM_KEY % dm_id,
                                 config.DIRECT_MESSAGE_MAX_COUNT, start_time)

    def handle_read_group_message(self, gm_id):
        return self._handle_read(gm_rds_cli, GM_KEY % gm_id,
                                 config.GROUP_MESSAGE_MAX_COUNT)

    def _handle_read(self, rds_cli, kname, count_limit, start_time='', user_id=None,
                     new_only=False):
        current_msg_id = int(rds_cli.hget(kname, 'msg_id') or 0)
        last_read_msg_id = 0
        if count_limit >= 0:
            last_read_msg_id = max(current_msg_id - count_limit, 0)
        if new_only and user_id:
            last_read_msg_id = max(
                int(rds_cli.hget(kname, 'last_read_by_%s' % user_id) or
                    last_read_msg_id), last_read_msg_id)
            rds_cli.hset(kname, 'last_read_by_%s' % user_id, current_msg_id)

        message_list = []
        for i in range(current_msg_id, last_read_msg_id, -1):
            message_name = MSG_KEY % (kname, i)
            message_dict = rds_cli.hgetall(message_name)
            type = message_dict.get(b'type')
            if not type:
                type = 1
            else:
                type = int(message_dict[b'type'].decode('utf-8'))

            if message_dict:
                if start_time and int(start_time) >= int(message_dict[b'created'].decode('utf-8')): continue
                message = {
                    'msg_id': i,
                    'user_id': message_dict[b'user_id'].decode('utf-8'),
                    'created': int(message_dict[b'created'].decode('utf-8')),
                    'type': type
                }
                if type == 3:
                    message.update({
                        'share': eval(message_dict[b'share'].decode('utf-8')),
                        'thumbnail': eval(message_dict[b'thumbnail'].decode('utf-8')),
                    })
                else:
                    message.update({
                        'content': message_dict[b'content'].decode('utf-8')
                    })
                message_list.append(message)

        user_count = rds_cli.scard(USERS_KEY % kname)
        return {
            'message_list': message_list,
            'user_count': user_count
        }

    def _get_member_val_from_dict(self, dict, key_name):
        key = dict.get(key_name)
        if key:
            return key.decode('utf-8')

        return None


    def handle_update_current_direct_message_id(self, dm_id, user_id):
        result = self._handle_update_msg_id(dm_rds_cli, DM_KEY % dm_id,
                                            user_id)
        self._remove_unread_list(dm_rds_cli, UNREAD_DM_KEY, user_id, dm_id)
        return result

    def handle_update_current_group_message_id(self, gm_id, user_id):
        result = self._handle_update_msg_id(gm_rds_cli, GM_KEY % gm_id,
                                             user_id)
        self._remove_unread_list(gm_rds_cli, UNREAD_GM_KEY, user_id, gm_id)
        return result

    def _handle_update_msg_id(self, rds_cli, kname, user_id):
        current_msg_id = int(rds_cli.hget(kname, 'msg_id') or 0)
        rds_cli.hset(kname, 'last_read_by_%s' % user_id, current_msg_id)
        return {'success': True}

    def _handle_send(self, rds_cli, kname, message, days_limit,
                     count_limit, start_time):
        message_id = rds_cli.hincrby(kname, 'msg_id')
        message_name = MSG_KEY % (kname, message_id)
        rds_cli.hmset(message_name, message)
        user_id = message.get('user_id')
        rds_cli.sadd(USERS_KEY % kname, user_id)

        if days_limit >= 0:
            seconds = int(days_limit * 24 * 60 * 60)
            rds_cli.expire(message_name, seconds)

        if (count_limit >= 0 and
            count_limit < message_id):
            expired_message_name = MSG_KEY % (kname, message_id - count_limit)
            rds_cli.expire(expired_message_name, 0)

        return self._handle_read(rds_cli, kname, count_limit, start_time, user_id, True)

    def _add_unread_list(self, rds_cli, unread_kname, users_kname,
                         from_user_id, mid):
        user_ids = rds_cli.smembers(users_kname)
        for user_id in user_ids:
            if int(user_id) != int(from_user_id):
                rds_cli.sadd(unread_kname % int(user_id), mid)

    def _remove_unread_list(self, rds_cli, unread_kname, user_id, mid):
        rds_cli.srem(unread_kname % int(user_id), mid)

    def _get_unread_list(self, rds_cli, unread_kname, user_id):
        return [int(mid) for mid in
                rds_cli.smembers(unread_kname % int(user_id))]

    def _get_unread_list_count(self, rds_cli, unread_kname, user_id):
        return {'count': rds_cli.scard(unread_kname % int(user_id))}

    def handle_send_direct_message(self, dm_id, message, to_user_id, start_time):
        kname = DM_KEY % dm_id
        message = eval(message)
        from_user_id = message.get('user_id')
        content = message.get('content')
        created = message.get('created')
        result = self._handle_send(dm_rds_cli, kname, message, config.DIRECT_MESSAGE_MAX_DAYS,
                                   config.DIRECT_MESSAGE_MAX_COUNT, start_time)
        dm_rds_cli.sadd(USERS_KEY % kname, to_user_id)
        self._add_unread_list(dm_rds_cli, UNREAD_DM_KEY, USERS_KEY % kname,
                                 from_user_id, dm_id)
        self._remove_unread_list(dm_rds_cli, UNREAD_DM_KEY, from_user_id, dm_id)
        return result

    def handle_send_group_message(self, gm_id, message):
        kname = GM_KEY % gm_id
        message = eval(message)
        from_user_id = message.get('user_id')
        result = self._handle_send(gm_rds_cli, kname, message, config.GROUP_MESSAGE_MAX_DAYS,
                                   config.GROUP_MESSAGE_MAX_COUNT)
        self._add_unread_list(gm_rds_cli, UNREAD_GM_KEY, USERS_KEY % kname,
                                 from_user_id, gm_id)
        self._remove_unread_list(gm_rds_cli, UNREAD_GM_KEY, from_user_id, gm_id)
        return result

    def handle_get_unread_direct_message(self, user_id):
        return self._get_unread_list(dm_rds_cli, UNREAD_DM_KEY, user_id)

    def handle_get_unread_direct_message_count(self, user_id):
        return self._get_unread_list_count(dm_rds_cli, UNREAD_DM_KEY, user_id)

    def handle_get_unread_group_message(self, user_id):
        return self._get_unread_list(gm_rds_cli, UNREAD_GM_KEY, user_id)

    def handle_get_unread_group_message_count(self, user_id):
        return self._get_unread_list_count(gm_rds_cli, UNREAD_GM_KEY, user_id)

    def handle_add_unread_comments(self, comment_id, user_id, comment_type):
        if comment_type and int(comment_type) == 1:
            key = UNREAD_COMMENT_KEY
        else:
            key = UNREAD_LIKE_KEY
        gm_rds_cli.sadd(key % int(user_id), comment_id)
        return {'success': True}

    def handle_remove_unread_comments(self, comment_id, user_id, comment_type):
        key = self._get_comment_key(comment_type)
        gm_rds_cli.srem(key % int(user_id), comment_id)
        return {'success': True}

    def handle_remove_unread_comments_by_user_id(self, user_id, comment_types):
        if comment_types:
            unread_comment_id_list = []
            comment_types = eval(comment_types) # string passed in, list object expected
            for ct in comment_types:
                if int(ct) == 1:
                    unread_comment_id_list.extend(self.handle_get_unread_comments(user_id,
                                                                                  1))
                    if unread_comment_id_list and len(unread_comment_id_list) > 0:
                        gm_rds_cli.srem(UNREAD_COMMENT_KEY % int(user_id), *[int(comment_id)
                                                                             for comment_id in unread_comment_id_list])
                else:
                    unread_comment_id_list.extend(self.handle_get_unread_comments(user_id,
                                                                                  2))
                    if unread_comment_id_list and len(unread_comment_id_list) > 0:
                        gm_rds_cli.srem(UNREAD_LIKE_KEY % int(user_id), *[int(comment_id)
                                                                             for comment_id in unread_comment_id_list])

        return {'success': True}

    def handle_remove_unread_comments_by_timeline(self, timeline_id):
        session = FeedSession()
        try:
            comment_list = session.query(Comments.id).filter(
                Comments.timeline_id == int(timeline_id)).all()
            timeline_user = session.query(Timeline.user_id).filter(
                Timeline.id == int(timeline_id), Timeline.is_deleted == False)\
                .first()
            if timeline_user and comment_list:
                gm_rds_cli.srem(UNREAD_COMMENT_KEY % int(timeline_user.user_id),
                                *[int(c.id) for c in comment_list])
                gm_rds_cli.srem(UNREAD_LIKE_KEY % int(timeline_user.user_id),
                                *[int(c.id) for c in comment_list])
        finally:
            session.close()
        return {'success': True}

    def _get_comment_key(self, comment_type):
        key = None
        if comment_type:
            if int(comment_type) == 1:
                key = UNREAD_COMMENT_KEY
            else:
                key = UNREAD_LIKE_KEY
        return key

    def handle_get_unread_comments(self, user_id, comment_type):
        key = self._get_comment_key(comment_type)
        return [int(cid) for cid in gm_rds_cli.smembers(key % int(user_id))]


    def handle_get_unread_comments_count(self, user_id, comment_type=1):
        key = self._get_comment_key(int(comment_type))
        return {'count': gm_rds_cli.scard(key % int(user_id))}

    def handle_clear_new_followers_count(self, user_id):
        follower_rds_cli.hset(NEW_FOLLOWER_KEY % int(user_id), 'count', 0)
        follower_rds_cli.hset(NEW_FOLLOWER_KEY % int(user_id), 'date', int(time.time()))
        return {'success': True}

    def handle_set_new_followers(self, from_user_id, to_user_id, count=0, created=0):
        is_follow = True if int(count) > 0 else False
        if created:
            created = created.split('.')[0]
            created = time.mktime(time.strptime(created,'%Y-%m-%d %H:%M:%S'))

        follower_count = int(follower_rds_cli.hget(NEW_FOLLOWER_KEY % int(to_user_id), 'count') or 0)
        if is_follow:
            follower_count = follower_count + int(count)
            follower_rds_cli.hset(NEW_FOLLOWER_KEY % int(to_user_id), 'count', follower_count)
            last_visit_date = follower_rds_cli.hget(NEW_FOLLOWER_KEY % int(to_user_id), 'date') or 0
            if not last_visit_date:
                follower_rds_cli.hset(NEW_FOLLOWER_KEY % int(to_user_id), 'date', int(time.time()))
            follower = {
                'follower_id': from_user_id,
                'follow_date': int(created)
            }
            seq_id = int(follower_rds_cli.hincrby(NEW_FOLLOWER_IDS_KEY % int(to_user_id), 'seq_id') or 0)
            follower_rds_cli.hmset(NEW_FOLLOWER_IDS_KEY_SEQ % (NEW_FOLLOWER_IDS_KEY % int(to_user_id), seq_id), follower)
        else:
            last_visit_date = follower_rds_cli.hget(NEW_FOLLOWER_KEY % int(to_user_id), 'date') or 0

            if last_visit_date and int(last_visit_date) < int(created):
                follower_count = follower_count + int(count)
                follower_rds_cli.hset(NEW_FOLLOWER_KEY % int(to_user_id), 'count', follower_count)
            seq_id = int(follower_rds_cli.hget(NEW_FOLLOWER_IDS_KEY % int(to_user_id), 'seq_id') or 0)
            if seq_id:
                for i in range(int(seq_id), 0, -1):
                    follower = follower_rds_cli.hgetall(NEW_FOLLOWER_IDS_KEY_SEQ % (NEW_FOLLOWER_IDS_KEY % int(to_user_id), seq_id))
                    follower_id = int(follower.get(b'follower_id') or 0)
                    if follower_id and follower_id == int(from_user_id):
                        follower_rds_cli.delete(NEW_FOLLOWER_IDS_KEY_SEQ % (NEW_FOLLOWER_IDS_KEY % int(to_user_id), seq_id))

        return {'success': True}

    def handle_get_new_followers_count(self, to_user_id, count=0):
        follower_count = int(follower_rds_cli.hget(NEW_FOLLOWER_KEY % int(to_user_id), 'count') or 0)
        return {'count': follower_count}

    def handle_get_new_followers(self, to_user_id):
        seq_id = int(follower_rds_cli.hget(NEW_FOLLOWER_IDS_KEY % int(to_user_id), 'seq_id') or 0)
        list_visit_date = follower_rds_cli.hget(NEW_FOLLOWER_KEY % int(to_user_id), 'date') or 0
        followers = []
        for i in range(seq_id, 0, -1):
            follower = follower_rds_cli.hgetall(NEW_FOLLOWER_IDS_KEY_SEQ % (NEW_FOLLOWER_IDS_KEY % int(to_user_id), i))
            if follower:
                follow_date = int(follower.get(b'follow_date'))
                if int(time.time()) - follow_date >= config.NEW_FOLLOWER_EXPIRATION_DAYS * 24 * 3600:
                    follower_rds_cli.delete(NEW_FOLLOWER_IDS_KEY_SEQ % (NEW_FOLLOWER_IDS_KEY % int(to_user_id), i))
                else:
                    f = {
                        'follower_id': follower.get(b'follower_id'),
                        'follow_date': follower.get(b'follow_date')
                    }
                    if follow_date > int(list_visit_date):
                        f.update({'isNew': 1})
                    followers.append(f)

        return {'followers': str(followers)}
