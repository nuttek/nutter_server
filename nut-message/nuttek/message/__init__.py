import traceback

from nuttek.message.handler import MessageHandler


def handle(method, *args, **kw):
    try:
        return MessageHandler().handle(method, *args)
    except Exception as e:
        traceback.print_exc()
        return [b'500', str(e).encode('utf-8')]
