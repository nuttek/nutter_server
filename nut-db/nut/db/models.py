from sqlalchemy import Column, text, PrimaryKeyConstraint
from sqlalchemy import BIGINT, TIMESTAMP, VARCHAR, BOOLEAN, SMALLINT, REAL

from nut.db.env import DeclarativeBase as Base
from nut.utils.misc import get_cdn_image_path
from .base_models import BaseTable


def make_img_url_with_url_prefix(url_prefix, img_name):
    if img_name and url_prefix:
        if url_prefix[-1:] == '/':
            return ''.join([url_prefix, img_name])
        else:
            return '/'.jpin([url_prefix,img_name])

    return img_name


class Users(BaseTable, Base):
    __tablename__ = 'users'

    id = Column(BIGINT, primary_key=True)
    mobile = Column(VARCHAR, index=True, unique=True)
    passwd = Column(VARCHAR)
    sina_weibo_uid = Column(VARCHAR, index=True)
    sina_weibo_oauth2_token = Column(VARCHAR)
    tencent_openid = Column(VARCHAR, index=True)
    openudid = Column(VARCHAR)
    bundle_version = Column(VARCHAR)
    name = Column(VARCHAR, index=True, unique=False)
    profile_img_url = Column(VARCHAR)
    gender = Column(SMALLINT)
    description = Column(VARCHAR)
    sign_up_time = Column(TIMESTAMP, server_default=text('now()'))
    last_login_time = Column(TIMESTAMP, server_default=text('now()'))
    last_logout_time = Column(TIMESTAMP)
    privacy_level = Column(SMALLINT, server_default=text('1'))
    forbid_time = Column(TIMESTAMP, nullable=True)
    user_level = Column(SMALLINT, server_default=text('0'))
    modified = Column(TIMESTAMP)

    def select_field_dict(self):
        return dict(id=self.id,
                    name='%s (id: %d)' % (self.name, self.id),
                    img=self.profile_img_url)

    def basic_profile(self):
        return {
            'id': self.id,
            'name': self.name,
            'profile_img_url': get_cdn_image_path(self.profile_img_url),
            'mobile': self.mobile
        }


class Terminals(BaseTable, Base):
    __tablename__ = 'terminals'

    id = Column(BIGINT, primary_key=True)
    user_id = Column(BIGINT, nullable=True)
    terminal_id = Column(VARCHAR)
    device_token = Column(VARCHAR)
    token_type = Column(VARCHAR, nullable=False, index=True, server_default=text('0'))
    device_model = Column(VARCHAR)
    bundle_version = Column(VARCHAR)
    os_version = Column(VARCHAR)
    factory_os_version = Column(VARCHAR)
    created = Column(TIMESTAMP, server_default=text('now()'))
    last_login_time = Column(TIMESTAMP)
    last_logout_time = Column(TIMESTAMP)
    is_online = Column(BOOLEAN, server_default=text('false'))


class Provinces(BaseTable, Base):
    __tablename__ = 'provinces'

    id = Column(SMALLINT, primary_key=True, autoincrement=False)
    name = Column(VARCHAR, nullable=False)
    is_municipality = Column(BOOLEAN, server_default=text('false'))
    order = Column(BIGINT)
    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    def dict(self):
        return dict(
            id=self.id,
            name=self.name
        )


class Cities(BaseTable, Base):
    __tablename__ = 'cities'

    id = Column(SMALLINT, primary_key=True, autoincrement=False)
    province_id = Column(BIGINT)
    name = Column(VARCHAR, nullable=False)
    is_municipality = Column(BOOLEAN, server_default=text('false'))
    order = Column(BIGINT)
    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    def dict(self):
        return dict(
            id=self.id,
            province_id=self.province_id,
            name=self.name,
            is_municipality=self.is_municipality
        )


class Counties(BaseTable, Base):
    __tablename__ = 'counties'

    id = Column(SMALLINT, primary_key=True, autoincrement=False)
    city_id = Column(SMALLINT)
    name = Column(VARCHAR, nullable=False)
    order = Column(BIGINT)
    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    def dict(self):
        return dict(
            id=self.id,
            city_id=self.city_id,
            name=self.name
        )


class Hospitals(BaseTable, Base):
    __tablename__ = 'hospitals'

    id = Column(SMALLINT, primary_key=True, autoincrement=False)
    name = Column(VARCHAR, nullable=False)
    short_name = Column(VARCHAR, nullable=False)
    type = Column(SMALLINT, nullable=False)
    hosp_type = Column(SMALLINT, nullable=False)
    level = Column(SMALLINT, nullable=False)
    province_id = Column(SMALLINT, nullable=False)
    city_id = Column(SMALLINT, nullable=False)
    county_id = Column(SMALLINT, nullable=False)

    address = Column(VARCHAR)
    zip_code = Column(VARCHAR)
    tel = Column(VARCHAR)
    fax = Column(VARCHAR)
    website = Column(VARCHAR)
    logo = Column(VARCHAR)

    lat = Column(REAL) # latitude
    lon = Column(REAL) # longitude

    traffic = Column(VARCHAR)
    description = Column(VARCHAR)

    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    is_soft_deleted = Column(BOOLEAN, server_default=text('false'))
    order = Column(BIGINT)

    def dict(self):
        from nut.api.cached.province_city import (get_cached_province, get_cached_city, get_cached_county,
                                                  get_cached_level)
        from nut.config import config
        from nut.api.departments import get_dept_dist_url

        return dict(
            id=self.id,
            name=self.name,
            short_name=self.short_name,
            level=get_cached_level(self.level).dict(),
            province=get_cached_province(self.province_id).dict(),
            city=get_cached_city(self.city_id).dict(),
            county=get_cached_county(self.county_id).dict(),
            logo=make_img_url_with_url_prefix(config.HOSP_IMG_URL_PREFIX, self.logo),
            dept_dist=get_dept_dist_url(self.id)
        )

    def full_dict(self):
        from nut.api.cached.province_city import (get_cached_province, get_cached_city, get_cached_county,
                                                  get_cached_level)
        from nut.config import config
        from nut.api.departments import get_dept_dist_url

        return dict(
            id=self.id,
            name=self.name,
            short_name=self.short_name,
            level=get_cached_level(self.level).dict(),
            province=get_cached_province(self.province_id).dict(),
            city=get_cached_city(self.city_id).dict(),
            county=get_cached_county(self.county_id).dict(),
            address=self.address,
            zip_code=self.zip_code,
            tel=self.tel,
            fax=self.fax,
            website=self.website,
            logo=make_img_url_with_url_prefix(config.HOSP_IMG_URL_PREFIX, self.logo),
            geo=dict(lat=self.lat, lon=self.lon),
            traffic=self.traffic,
            description=self.description,
            dept_dist=get_dept_dist_url(self.id)
        )


class Departments(BaseTable, Base):
    __tablename__ = 'departments'

    id = Column(SMALLINT, primary_key=True, autoincrement=False)
    parent_id = Column(SMALLINT)
    name = Column(VARCHAR, nullable=False)
    type = Column(SMALLINT)
    order = Column(BIGINT)

    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    is_soft_deleted = Column(BOOLEAN, server_default=text('false'))

    def dict(self):
        return dict(
            id=self.id,
            parent_id=self.parent_id,
            name=self.name
        )


class Levels(BaseTable, Base):
    __tablename__='levels'

    id = Column(SMALLINT, autoincrement=False)
    type = Column(SMALLINT) # 0 - hosp, 1 - doctor
    name = Column(VARCHAR, nullable=False)

    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    is_soft_deleted = Column(BOOLEAN, server_default=text('false'))
    order = Column(BIGINT)

    __table_args__ = (PrimaryKeyConstraint('id', 'type', name='levels_pkey'),)

    def dict(self):
        return dict(
            id=self.id,
            type=self.type,
            name=self.name
        )


class Doctors(BaseTable, Base):
    __tablename__ = 'doctors'

    id = Column(BIGINT, primary_key=True, autoincrement=False)
    hosp_id = Column(SMALLINT)
    dept_id = Column(SMALLINT)
    level = Column(SMALLINT)
    name = Column(VARCHAR, nullable=False)
    thumbnail = Column(VARCHAR)
    description = Column(VARCHAR)

    created = Column(TIMESTAMP, server_default=text('now()'))
    modified = Column(TIMESTAMP, server_default=text('now()'))

    is_soft_deleted = Column(BOOLEAN, server_default=text('false'))
    order = Column(BIGINT)

    def dict(self):
        from nut.api.departments import get_department_by_id
        from nut.api.hospitals import get_hospital_by_id
        from nut.api.cached.province_city import (get_cached_level)
        from nut.config import config

        return dict(
            id=self.id,
            hospital=get_hospital_by_id(self.hosp_id).dict(),
            department=get_department_by_id(self.dept_id).dict(),
            name=self.name,
            thumbnail=make_img_url_with_url_prefix(config.DOCTOR_IMG_URL_PREFIX,self.thumbnail),
            level=get_cached_level(self.level).dict(),
            description=self.description
        )


class TreatmentStatus(BaseTable, Base):
    __tablename__ = 'treatment_status'

    code = Column(VARCHAR)
    parent_code = Column(VARCHAR)
    order = Column(BIGINT)
    description = Column(VARCHAR)
    previous_step_codes = Column(VARCHAR)
    code_name = Column(VARCHAR)

    __table_args__ = (PrimaryKeyConstraint('code', 'parent_code', name='treatment_status_pkey'),)

    def dict(self):
        return dict(
            id=self.id,
            parent_id=self.parent_id,
            description=self.description
        )


class TreatmentTime(BaseTable, Base):
    __tablename__ = 'treatment_time'

    doctor_id = Column(BIGINT)
    date = Column(VARCHAR)
    time = Column(SMALLINT) # 0 - am, 1 - pm
    original_amount = Column(BIGINT)
    remaining_amount = Column(BIGINT)
    current_registration_id = Column(BIGINT)
    price = Column(REAL)

    __table_args__ = (PrimaryKeyConstraint('doctor_id', 'date', 'time', name='treatment_time_pkey'),)

    def dict(self):
        from nut.api.cached.doctors import get_cached_doctor
        return dict(
            doctor=get_cached_doctor(self.doctor_id),
            date=self.date,
            time=self.time,
            original_amount=self.original_amount,
            remaining_amount=self.remaining_amount,
            price=self.price
        )

    def lineup_dict(self):
        from nut.api.cached.doctors import get_cached_doctor
        return dict(
            doctor=get_cached_doctor(self.doctor_id),
            date=self.date,
            time=self.time,
            original_amount=self.original_amount,
            remaining_amount=self.remaining_amount,
            price=self.price,
            current_registration_id=self.current_registration_id
        )


class TreatmentProgress(BaseTable, Base):
    __tablename__ = 'treatment_progress'

    registration_id = Column(BIGINT)
    user_id = Column(BIGINT)
    patient_id = Column(BIGINT)
    doctor_id = Column(BIGINT)
    status_code = Column(VARCHAR)
    child_status_code = Column(VARCHAR)
    datetime = Column(TIMESTAMP, default=text('now()'))

    __table_args__ = (PrimaryKeyConstraint('registration_id', 'status_code', 'child_status_code',
                                           name='treatment_progress_pkey'),)

    def dict(self):
        return dict(

        )


class TreatmentFee(BaseTable, Base):
    __tablename__ = 'treatment_fee'

    registration_id = Column(BIGINT)
    status = Column(VARCHAR)
    fee = Column(REAL)
    self_finance = Column(REAL)
    medical_insurance = Column(REAL)

    __table_args__ = (PrimaryKeyConstraint('registration_id', 'status', name='treatment_fee_pkey'),)

    def dict(self):
        return dict(

        )


class TreatmentRegistration(BaseTable, Base):
    __tablename__ = 'treatment_registration'

    treatment_id = Column(BIGINT, primary_key=True)
    registration_id = Column(VARCHAR)
    user_id = Column(BIGINT)
    patient_id = Column(BIGINT)
    doctor_id = Column(BIGINT)
    date = Column(VARCHAR)
    time = Column(SMALLINT)
    fake_lineup_num = Column(SMALLINT)

    def dict(self):
        return dict(

        )


class ReimTypes(BaseTable, Base):
    __tablename__ = 'reim_types'

    id = Column(BIGINT, primary_key=True)
    name = Column(VARCHAR)

    def dict(self):
        return dict(
            id=self.id,
            name=self.name
        )


class Patients(BaseTable, Base):
    __tablename__ = 'patients'

    id = Column(BIGINT, primary_key=True)
    user_id = Column(BIGINT)
    name = Column(VARCHAR)
    id_card = Column(VARCHAR)
    mc_card = Column(VARCHAR)
    reim_type = Column(SMALLINT)
    mobile = Column(VARCHAR)

    def dict(self):
        return dict(
            id=self.id,
            name=self.name,
            id_card=self.id_card,
            mc_card=self.mc_card,
            reim_type=self.reim_type,
            mobile=self.mobile
        )


class Favorites(BaseTable, Base):
    __tablename__ = 'favorites'

    user_id = Column(BIGINT)
    obj_type = Column(SMALLINT)
    obj_id = Column(BIGINT)
    created = Column(TIMESTAMP, server_default=text('now()'))

    __table_args__ = (PrimaryKeyConstraint('user_id', 'obj_type', 'obj_id', name='favorites_pkey'),)

    def dict(self):
        return dict(
            type=self.obj_type,
            obj_id=self.obj_id
        )


class Symps(BaseTable, Base):
    __tablename__ = 'symps'

    id = Column(BIGINT, primary_key=True)
    parent_id = Column(BIGINT)
    name = Column(VARCHAR)
    department_id = Column(BIGINT)

    def dict(self):
        return dict(
            id=self.id,
            name=self.name,
            department_id=self.department_id
        )


class Questions(BaseTable, Base):
    __tablename__ = 'questions'

    id = Column(BIGINT, primary_key=True)
    user_id = Column(BIGINT)
    subject = Column(VARCHAR(100))
    description = Column(VARCHAR(500))
    created_time = Column(TIMESTAMP, server_default=text('now()'))

    def short_json(self):
        return dict(
            id=self.id,
            created_time=self.created_time
        )

    def json(self):
        from nut.api.cached.user import get_cached_user
        return dict(
            id=self.id,
            user=get_cached_user(self.user_id),
            subject=self.subject,
            description=self.description,
            created_time=self.created_time
        )


class Answers(BaseTable, Base):
    __tablename__ = 'answers'

    id = Column(BIGINT, primary_key=True)
    question_id = Column(BIGINT)
    user_id = Column(BIGINT)
    description = Column(VARCHAR(500))
    created_time = Column(TIMESTAMP, server_default=text('now()'))

    def short_json(self):
        return dict(
            id=self.id,
            question_id=self.question_id,
            created_time=self.created_time
    )

    def json(self):
        from nut.api.cached.user import get_cached_user

        return dict(
            id=self.id,
            user=get_cached_user(self.user_id),
            question_id=self.question_id,
            description=self.description,
            created_time=self.created_time
        )


class Comments(BaseTable, Base):
    __tablename__ = 'comments'

    id = Column(BIGINT, primary_key=True)
    obj_type = Column(SMALLINT)
    obj_id = Column(BIGINT)
    user_id = Column(BIGINT)
    description = Column(VARCHAR(500))
    created_time = Column(TIMESTAMP, server_default=text('now()'))

    def short_json(self):
        return dict(
            id=self.id,
            created_time=self.created_time
        )

    def json(self):
        return dict(
            id=self.id,
            user_id=self.user_id,
            obj_type=self.obj_type,
            obj_id=self.obj_id,
            description=self.description,
            created_time=self.created_time
        )


class Votes(BaseTable, Base):
    __tablename__ = 'Votes'

    user_id = Column(BIGINT)
    obj_type = Column(SMALLINT)  # 0 - answer
    obj_id = Column(BIGINT)
    action = Column(SMALLINT)  # 0 - downvote; 1 - upvote; 2 - like
    created_time = Column(TIMESTAMP, server_default=text('now()'))

    __table_args__ = (PrimaryKeyConstraint('user_id', 'obj_type', 'obj_id', name='likes_pkey'),)