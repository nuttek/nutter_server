from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from nut.config import config


# Create postgresql + psycopg2 engine.
main_engine = create_engine(
    'postgresql+psycopg2://%s:%s@%s:%s/%s' % (
        config.MAIN_DB_USER, config.MAIN_DB_PASSWORD,
        config.MAIN_DB_HOST, config.MAIN_DB_PORT,
        config.MAIN_DB_DBNAME),echo=False, pool_size=config.DB_POOL_SIZE,
    max_overflow=config.DB_MAX_OVERFLOW)


Session = sessionmaker(bind=main_engine)

DeclarativeBase = declarative_base()
