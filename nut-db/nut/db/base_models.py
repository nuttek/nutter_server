from sqlalchemy.ext.mutable import MutableComposite


class Size(MutableComposite):
    def __init__(self, height, width):
        self.height = height
        self.width = width

    def __setattr__(self, key, value):
        object.__setattr__(self, key, value)
        self.changed()

    def __composite_values__(self):
        return self.height, self.width

    def __eq__(self, other):
        return isinstance(other, Size) and \
                other.height == self.height and \
                other.width == self.width

    def __ne__(self, other):
        return not self.__eq__(other)


class Point(MutableComposite):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __setattr__(self, key, value):
        object.__setattr__(self, key, value)
        self.changed()

    def __composite_values__(self):
        return self.x, self.y

    def __eq__(self, other):
        return isinstance(other, Point) and \
                other.x == self.x and \
                other.y == self.y

    def __ne__(self, other):
        return not self.__eq__(other)


class Position(MutableComposite):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __setattr__(self, key, value):
        object.__setattr__(self, key, value)
        self.changed()

    def __composite_values__(self):
        return self.x, self.y

    def __eq__(self, other):
        return isinstance(other, Position) and \
                other.x == self.x and \
                other.y == self.y

    def __ne__(self, other):
        return not self.__eq__(other)


class BaseTable(object):
    def __init__(self, **kwargs):
        self.update(kwargs)

    def update(self, d):
        if not isinstance(d, dict): return

        for k, v in d.items():
            setattr(self, k, v)
