import glob
from codecs import open  # To use a consistent encoding
from os import path

from setuptools import setup, find_packages


here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nut-db',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # http://packaging.python.org/en/latest/tutorial.html#version
    version='0.0.1',

    description='Database support for nut',
    long_description=long_description,

    url='http://nuttek.com',

    author='nuttek corp',
    author_email='likui@nut.com',

    license='Proprietary',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: Other/Proprietary License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
    ],

    keywords='server db',

    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    namespace_packages=['nut'],

    install_requires=[
        'nut-core>=0.0.1',
        'psycopg2',
        'sqlalchemy',
        'alembic',
    ],

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data={
        # 'sample': ['package_data.dat'],
    },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages.
    # see http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    data_files=[
        ('etc/main', ['migration/main/alembic.ini']),
        ('etc/main/alembic', ['migration/main/alembic/env.py',
                              'migration/main/alembic/script.py.mako']),
        ('etc/main/alembic/versions',
         glob.glob('migration/main/alembic/versions/*.py')),
    ],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            # 'sample=sample:main',
        ],
    },
)
