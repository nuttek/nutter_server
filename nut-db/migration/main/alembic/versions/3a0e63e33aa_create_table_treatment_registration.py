"""create table treatment_registration

Revision ID: 3a0e63e33aa
Revises: 2c405dd6d03
Create Date: 2016-05-06 08:35:32.852141

"""

# revision identifiers, used by Alembic.
revision = '3a0e63e33aa'
down_revision = '2c405dd6d03'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('treatment_registration',
        sa.Column('treatment_id', sa.BIGINT, primary_key=True),
        sa.Column('registration_id', sa.VARCHAR),
        sa.Column('user_id', sa.BIGINT),
        sa.Column('patient_id', sa.BIGINT),
        sa.Column('doctor_id', sa.BIGINT),
        sa.Column('date', sa.VARCHAR),
        sa.Column('time', sa.SMALLINT),
        sa.Column('fake_lineup_num', sa.SMALLINT)
    )

def downgrade():
    op.drop_table('treatment_registration')
