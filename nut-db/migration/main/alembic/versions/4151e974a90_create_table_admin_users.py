"""create table admin users

Revision ID: 4151e974a90
Revises: 548a7ee8716
Create Date: 2016-05-18 16:31:05.925382

"""

# revision identifiers, used by Alembic.
revision = '4151e974a90'
down_revision = '548a7ee8716'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('admin_users',
        sa.Column('id', sa.BIGINT, primary_key=True),
        sa.Column('name', sa.VARCHAR, index=True, unique=True),
        sa.Column('passwd', sa.VARCHAR),
        sa.Column('type', sa.SMALLINT, server_default=sa.text('0'))
    )


def downgrade():
    op.drop_table('admin_users')
