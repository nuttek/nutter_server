"""Create questions related tables

Revision ID: 242da49537d
Revises: 24158ae928e
Create Date: 2016-06-02 11:09:06.556926

"""

# revision identifiers, used by Alembic.
revision = '242da49537d'
down_revision = '24158ae928e'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('questions',
        sa.Column('id', sa.BIGINT, primary_key=True),
        sa.Column('user_id', sa.BIGINT),
        sa.Column('subject', sa.VARCHAR(100)),
        sa.Column('description', sa.VARCHAR(500)),
        sa.Column('created_time', sa.TIMESTAMP, server_default=sa.text('now()'))
                    )

    op.create_table('answers',
        sa.Column('id', sa.BIGINT, primary_key=True),
        sa.Column('question_id', sa.BIGINT),
        sa.Column('user_id', sa.BIGINT),
        sa.Column('description', sa.VARCHAR(500)),
        sa.Column('created_time', sa.TIMESTAMP, server_default=sa.text('now()'))
                    )

    op.create_table('comments',
        sa.Column('id', sa.BIGINT, primary_key=True),
        sa.Column('obj_type', sa.SMALLINT),
        sa.Column('obj_id', sa.BIGINT),
        sa.Column('user_id', sa.BIGINT),
        sa.Column('description', sa.VARCHAR(500)),
        sa.Column('created_time', sa.TIMESTAMP, server_default=sa.text('now()'))
                    )

    op.create_table('votes',
        sa.Column('user_id', sa.BIGINT),
        sa.Column('obj_type', sa.SMALLINT),  # 0 - answer
        sa.Column('obj_id', sa.BIGINT),
        sa.Column('action', sa.SMALLINT),  # 0 - downvote; 1 - upvote; 2 - like
        sa.Column('created_time', sa.TIMESTAMP, server_default=sa.text('now()')),

        sa.PrimaryKeyConstraint('user_id', 'obj_type', 'obj_id', name='votes_pkey'),
    )



def downgrade():
    op.drop_table('questions')
    op.drop_table('answers')
    op.drop_table('comments')
    op.drop_table('likes')