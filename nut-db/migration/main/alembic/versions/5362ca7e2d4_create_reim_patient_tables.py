"""create reim/patient tables

Revision ID: 5362ca7e2d4
Revises: 40600e48e29
Create Date: 2016-05-05 09:53:22.229648

"""

# revision identifiers, used by Alembic.
revision = '5362ca7e2d4'
down_revision = '40600e48e29'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('reim_types',
                    sa.Column('id', sa.BIGINT, primary_key=True),
                    sa.Column('name', sa.VARCHAR)
    )

    op.create_table('patients',
        sa.Column('id', sa.BIGINT, primary_key=True),
        sa.Column('user_id', sa.BIGINT),
        sa.Column('name', sa.VARCHAR),
        sa.Column('id_card', sa.VARCHAR),
        sa.Column('mc_card', sa.VARCHAR),
        sa.Column('reim_type', sa.SMALLINT),
        sa.Column('mobile', sa.VARCHAR)
    )


def downgrade():
    op.drop_table('reim_types')
    op.drop_table('patients')