"""Create tables of Users, Terminals

Revision ID: b3adecbec7
Revises: None
Create Date: 2016-03-21 12:16:37.071784

"""

# revision identifiers, used by Alembic.
revision = 'b3adecbec7'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('users',
                    sa.Column('id', sa.BIGINT, primary_key=True),
                    sa.Column('mobile', sa.VARCHAR, index=True, unique=True),
                    sa.Column('passwd', sa.VARCHAR),
                    sa.Column('sina_weibo_uid', sa.VARCHAR, index=True),
                    sa.Column('sina_weibo_oauth2_token', sa.VARCHAR),
                    sa.Column('tencent_openid', sa.VARCHAR, index=True),
                    sa.Column('openudid', sa.VARCHAR, index=True),
                    sa.Column('bundle_version', sa.VARCHAR),
                    sa.Column('name', sa.VARCHAR, index=True, unique=False),
                    sa.Column('profile_img_url', sa.VARCHAR),
                    sa.Column('gender', sa.SMALLINT),
                    sa.Column('description', sa.VARCHAR),
                    sa.Column('sign_up_time', sa.TIMESTAMP, server_default=sa.text('now()')),
                    sa.Column('last_login_time', sa.TIMESTAMP, server_default=sa.text('now()')),
                    sa.Column('last_logout_time', sa.TIMESTAMP),
                    sa.Column('privacy_level', sa.SMALLINT, server_default=sa.text('1')),
                    sa.Column('forbid_time', sa.TIMESTAMP, nullable=True),
                    sa.Column('user_level', sa.SMALLINT, server_default=sa.text('0')),
                    sa.Column('modified', sa.TIMESTAMP)
                    )

    op.create_table('terminals',
                    sa.Column('id', sa.BIGINT(), primary_key=True),
                    sa.Column('device_token', sa.VARCHAR(), nullable=False),
                    sa.Column('token_type', sa.SMALLINT(), nullable=False, index=True, server_default=sa.text('0')),
                    sa.Column('user_id', sa.BIGINT()),
                    sa.Column('terminal_id', sa.VARCHAR()),
                    sa.Column('device_model', sa.VARCHAR()),
                    sa.Column('bundle_version', sa.VARCHAR()),
                    sa.Column('os_version', sa.VARCHAR()),
                    sa.Column('factory_os_version', sa.VARCHAR()),
                    sa.Column('created', sa.TIMESTAMP(), nullable=True),
                    sa.Column('last_login_time', sa.TIMESTAMP(), nullable=True),
                    sa.Column('last_logout_time', sa.TIMESTAMP(), nullable=True),
                    sa.Column('is_online', sa.BOOLEAN(), server_default=sa.text('false'))
                    )


def downgrade():
    op.drop_table('users')
    op.drop_table('terminals')
