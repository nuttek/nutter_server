"""create table favorites

Revision ID: 548a7ee8716
Revises: 3a0e63e33aa
Create Date: 2016-05-16 11:40:17.848327

"""

# revision identifiers, used by Alembic.
revision = '548a7ee8716'
down_revision = '3a0e63e33aa'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('favorites',
        sa.Column('user_id', sa.BIGINT),
        sa.Column('obj_type', sa.SMALLINT),
        sa.Column('obj_id', sa.BIGINT),
        sa.Column('created', sa.TIMESTAMP, server_default=sa.text('now()')),

        sa.PrimaryKeyConstraint('user_id', 'obj_type', 'obj_id', name='favorites_pkey'),)


def downgrade():
    op.drop_table('favorites')
