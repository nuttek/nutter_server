"""Add DATE/SOFT DELETE FLAG COLUMNS

Revision ID: 4e0205a7478
Revises: 3391dcd6eae
Create Date: 2016-03-29 11:10:20.886451

"""

# revision identifiers, used by Alembic.
revision = '4e0205a7478'
down_revision = '3391dcd6eae'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # table provinces
    op.add_column('provinces', sa.Column('created', sa.TIMESTAMP, server_default='now()'))
    op.add_column('provinces', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))
    op.add_column('provinces', sa.Column('order', sa.BIGINT))

    # table cities
    op.add_column('cities', sa.Column('created', sa.TIMESTAMP, server_default='now()'))
    op.add_column('cities', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))
    op.add_column('cities', sa.Column('order', sa.BIGINT))

    # table counties
    op.add_column('counties', sa.Column('created', sa.TIMESTAMP, server_default='now()'))
    op.add_column('counties', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))
    op.add_column('counties', sa.Column('order', sa.BIGINT))

    # table hospitals
    op.add_column('hospitals', sa.Column('is_soft_deleted', sa.BOOLEAN, server_default='false'))
    op.add_column('hospitals', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))
    op.add_column('hospitals', sa.Column('order', sa.BIGINT))

    # table departments
    op.add_column('departments', sa.Column('is_soft_deleted', sa.BOOLEAN, server_default='false'))
    op.add_column('departments', sa.Column('created', sa.TIMESTAMP, server_default='now()'))
    op.add_column('departments', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))

    # table levels
    op.add_column('levels', sa.Column('is_soft_deleted', sa.BOOLEAN, server_default='false'))
    op.add_column('levels', sa.Column('created', sa.TIMESTAMP, server_default='now()'))
    op.add_column('levels', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))
    op.add_column('levels', sa.Column('order', sa.BIGINT))

    # table doctors
    op.add_column('doctors', sa.Column('is_soft_deleted', sa.BOOLEAN, server_default='false'))
    op.add_column('doctors', sa.Column('created', sa.TIMESTAMP, server_default='now()'))
    op.add_column('doctors', sa.Column('modified', sa.TIMESTAMP, server_default='now()'))
    op.add_column('doctors', sa.Column('order', sa.BIGINT))


def downgrade():
    # No need to drop columns added here, just leave them there, no harm.
    pass
