"""add column patient_id into tables of treatment_progress and treatment_fee

Revision ID: 2c405dd6d03
Revises: 5362ca7e2d4
Create Date: 2016-05-05 15:50:01.550961

"""

# revision identifiers, used by Alembic.
revision = '2c405dd6d03'
down_revision = '5362ca7e2d4'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('treatment_fee', sa.Column('patient_id', sa.BIGINT))
    op.add_column('treatment_progress', sa.Column('patient_id', sa.BIGINT))


def downgrade():
    op.drop_column('treatment_fee', 'patient_id')
    op.drop_column('treatment_progress', 'patient_id')
