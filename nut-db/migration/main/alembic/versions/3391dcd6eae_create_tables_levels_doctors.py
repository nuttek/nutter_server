"""Create Tables of Levels, Doctors

Revision ID: 3391dcd6eae
Revises: 31fc4b4e0ca
Create Date: 2016-03-24 17:22:34.516399

"""

# revision identifiers, used by Alembic.
revision = '3391dcd6eae'
down_revision = '31fc4b4e0ca'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('levels',
                    sa.Column('id', sa.SMALLINT, autoincrement=False),
                    sa.Column('type', sa.SMALLINT),  # 0 - hosp, 1 - doctor
                    sa.Column('name', sa.VARCHAR, nullable=False),

                    sa.PrimaryKeyConstraint('id', 'type', name='levels_pkey'),
                    )

    op.create_table('doctors',
                    sa.Column('id', sa.BIGINT, primary_key=True, autoincrement=False),
                    sa.Column('hosp_id', sa.SMALLINT),
                    sa.Column('dept_id', sa.SMALLINT),
                    sa.Column('level', sa.SMALLINT),
                    sa.Column('name', sa.VARCHAR, nullable=False),
                    sa.Column('thumbnail', sa.VARCHAR),
                    sa.Column('description', sa.VARCHAR)
                    )


def downgrade():
    op.drop_table('levels')
    op.drop_table('doctors')
