"""Create symps table

Revision ID: 24158ae928e
Revises: 4151e974a90
Create Date: 2016-05-29 02:27:25.978250

"""

# revision identifiers, used by Alembic.
revision = '24158ae928e'
down_revision = '4151e974a90'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('symps',
                    sa.Column('id', sa.BIGINT, primary_key=True),
                    sa.Column('parent_id', sa.BIGINT),
                    sa.Column('name', sa.VARCHAR),
                    sa.Column('department_id', sa.BIGINT)
    )


def downgrade():
    op.drop_table('symps')
