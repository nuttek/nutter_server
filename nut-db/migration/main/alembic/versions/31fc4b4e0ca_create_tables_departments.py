"""Create Tables of Departments

Revision ID: 31fc4b4e0ca
Revises: 73a3997f6
Create Date: 2016-03-24 16:29:42.109212

"""

# revision identifiers, used by Alembic.
revision = '31fc4b4e0ca'
down_revision = '73a3997f6'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('departments',
                    sa.Column('id', sa.SMALLINT, primary_key=True, autoincrement=False),
                    sa.Column('parent_id', sa.SMALLINT),
                    sa.Column('name', sa.VARCHAR, nullable=False),
                    sa.Column('type', sa.SMALLINT),
                    sa.Column('order', sa.BIGINT)
                    )


def downgrade():
    op.drop_table('departments')
