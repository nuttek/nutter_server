"""Create tables of Provinces, Citis, Counties, Hospitals

Revision ID: 73a3997f6
Revises: b3adecbec7
Create Date: 2016-03-24 10:44:13.263698

"""

# revision identifiers, used by Alembic.
revision = '73a3997f6'
down_revision = 'b3adecbec7'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('provinces',
                    sa.Column('id', sa.SMALLINT, primary_key=True, autoincrement=False),
                    sa.Column('name', sa.VARCHAR, nullable=False),
                    sa.Column('is_municipality', sa.BOOLEAN, server_default=sa.text('false'))
                    )


    op.create_table('cities',
                    sa.Column('id', sa.SMALLINT, primary_key=True, autoincrement=False),
                    sa.Column('province_id', sa.BIGINT),
                    sa.Column('name', sa.VARCHAR, nullable=False),
                    sa.Column('is_municipality', sa.BOOLEAN, server_default=sa.text('false'))
                    )

    op.create_table('counties',
                    sa.Column('id', sa.SMALLINT, primary_key=True, autoincrement=False),
                    sa.Column('city_id', sa.SMALLINT),
                    sa.Column('name', sa.VARCHAR, nullable=False)
                    )

    op.create_table('hospitals',
                    sa.Column('id', sa.BIGINT, primary_key=True, autoincrement=False),
                    sa.Column('name', sa.VARCHAR, nullable=False),
                    sa.Column('short_name', sa.VARCHAR, nullable=False),
                    sa.Column('type', sa.SMALLINT, nullable=False),
                    sa.Column('hosp_type', sa.SMALLINT, nullable=False),
                    sa.Column('level', sa.SMALLINT, nullable=False),
                    sa.Column('province_id', sa.SMALLINT, nullable=False),
                    sa.Column('city_id', sa.SMALLINT, nullable=False),
                    sa.Column('county_id', sa.SMALLINT, nullable=False),
                    sa.Column('address', sa.VARCHAR),
                    sa.Column('zip_code', sa.VARCHAR),
                    sa.Column('tel', sa.VARCHAR),
                    sa.Column('fax', sa.VARCHAR),
                    sa.Column('website', sa.VARCHAR),
                    sa.Column('logo', sa.VARCHAR),

                    sa.Column('lat', sa.REAL),  # latitude
                    sa.Column('lon', sa.REAL),  # longitude

                    sa.Column('traffic', sa.VARCHAR),
                    sa.Column('description', sa.VARCHAR),

                    sa.Column('created', sa.TIMESTAMP, server_default=sa.text('now()'))
                    )


def downgrade():
    op.drop_table('provinces')
    op.drop_table('cities')
    op.drop_table('counties')
    op.drop_table('hospitals')
