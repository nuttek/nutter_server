"""create treatment related tables

Revision ID: 40600e48e29
Revises: 4e0205a7478
Create Date: 2016-05-04 13:50:18.310083

"""

# revision identifiers, used by Alembic.
revision = '40600e48e29'
down_revision = '4e0205a7478'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('treatment_status',
                    sa.Column('code', sa.VARCHAR),
                    sa.Column('parent_code', sa.VARCHAR),
                    sa.Column('order', sa.BIGINT),
                    sa.Column('description', sa.VARCHAR),
                    sa.Column('previous_step_codes', sa.VARCHAR),
                    sa.Column('code_name', sa.VARCHAR),

                    sa.PrimaryKeyConstraint('code', 'parent_code', name='treatment_status_pkey'),
    )

    op.create_table('treatment_time',
                    sa.Column('doctor_id', sa.BIGINT, autoincrement=False),
                    sa.Column('date', sa.VARCHAR),
                    sa.Column('time', sa.SMALLINT),
                    sa.Column('original_amount', sa.BIGINT),
                    sa.Column('remaining_amount', sa.BIGINT),
                    sa.Column('current_registration_id', sa.BIGINT),
                    sa.Column('price', sa.REAL),

                    sa.PrimaryKeyConstraint('doctor_id', 'date', 'time', name='treatment_time_pkey'),
    )

    op.create_table('treatment_progress',
                    sa.Column('registration_id', sa.BIGINT, primary_key=True, autoincrement=False),
                    sa.Column('user_id', sa.BIGINT),
                    sa.Column('doctor_id', sa.BIGINT),
                    sa.Column('status_code', sa.VARCHAR),
                    sa.Column('child_status_code', sa.VARCHAR),
                    sa.Column('datetime', sa.TIMESTAMP, default=sa.text('now()')),
                    sa.PrimaryKeyConstraint('registration_id', 'status_code', 'child_status_code',
                                            name='treatment_progress_pkey'),
    )

    op.create_table('treatment_fee',
                    sa.Column('registration_id', sa.BIGINT),
                    sa.Column('status', sa.VARCHAR),
                    sa.Column('fee', sa.REAL),
                    sa.Column('self_finance', sa.REAL),
                    sa.Column('medical_insurance', sa.REAL),

                    sa.PrimaryKeyConstraint('registration_id', 'status', name='treatment_fee_pkey'),
    )


def downgrade():
    op.drop_table('treatment_status')
    op.drop_table('treatment_time')
    op.drop_table('treatment_progress')
    op.drop_table('treatment_fee')

