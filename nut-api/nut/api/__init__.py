import logging
import os
import time

from flask import Flask, request, session
from sqlalchemy.orm import scoped_session

from nut.config import config
from nut.db import env
from nut.utils.cache import NutCache

logger = logging.getLogger(__name__)

# The main application
application = Flask(__name__)
application.secret_key = 'XPXu04w-(exW5lm`JT3c'
application.debug = config.ENABLE_DEBUG_MODE

# SQLAlchemy integration
Session = scoped_session(env.Session, scopefunc=lambda: hash(request or 'request'))

# Configuring Flask-Cache
cache = NutCache(application, config={
    'CACHE_TYPE': 'redis',
    'CACHE_DEFAULT_TIMEOUT': config.API_DEFAULT_CACHE_TIMEOUT,
    'CACHE_KEY_PREFIX': 'napi_',
    'CACHE_REDIS_URL': config.API_CACHE_REDIS_URL
})

cache.init_app(application)


def close_db_session(response):
    Session.remove()
    return response

application.teardown_request(close_db_session)


def generate_get_request_string(request):
    args_string = None
    args = request.args
    for k in args.keys():
        v = args.get(k, None)
        if v:
            if args_string:
                args_string = args_string + "&" + "%s=%s" %(k, str(v))
            else:
                args_string = "%s=%s" %(k, str(v))
        else:
            if args_string:
                args_string = args_string + "&" + "%s" % k
            else:
                args_string = "%s" % k
    return request.method + " " + request.full_path + (args_string or "")


def _dump_request_form(request):
    _form_strings = []
    if request.form:
        for k in request.form.keys():
            v = request.form.get(k, None)
            if v:
                _form_strings.append("%s=%s" %(k, str(v)))
    if len(_form_strings) > 0:
        return '&'.join(_form_strings)
    return None


def _dump_request_json(request):
    if request.json:
        return 'json:{%s}' % request.json
    return None


def _dump_request_files(request):
    if request.files:
        return 'files:{%d file(s) uploaded}' % len(request.files)

    return None


def generate_post_request_string(request):
    strings = []

    _return_string = _dump_request_form(request)
    if _return_string:
        strings.append(_return_string)
    _return_string = _dump_request_json(request)
    if _return_string:
        strings.append(_return_string)
    _return_string = _dump_request_files(request)
    if _return_string:
        strings.append(_return_string)

    if len(strings) > 0:
        return (';'+ os.linesep).join(strings)

    return None


def log_request(request):
    try:
        logger.info(generate_get_request_string(request))
        if logger.isEnabledFor(logging.DEBUG):
            post_request_string = generate_post_request_string(request)
            if post_request_string:
                logger.debug("Request:" + os.linesep + post_request_string)
    except:
        pass


@application.before_request
def before_request():
    from nut.api.cached.user import get_cached_user
    from nut.api.cached.terminal import get_cached_terminal

    request.start_time=time.time() * 1000 #in mseconds
    request.user = None
    request.terminal = None
    user_id = None
    terminal_id = None
    logger.info("Before request, session info: {%s}" % (session))
    if 'userid' in session:
        user_id = int(session['userid'])
        request.user = get_cached_user(user_id)
        logger.info("Before request, user info: {user_id:%d, user_obj:%s}" % (user_id, request.user or ""))
    if 'terminalid' in session:
        terminal_id = session['terminalid']
        request.terminal = get_cached_terminal(user_id, terminal_id)
        logger.info("Before request, terminal info: {terminal_id:%s, terminal_obj:%s}"
                    % (terminal_id, request.terminal or ""))
    log_request(request)


def log_response(resp):
    try:
        if logger.isEnabledFor(logging.DEBUG):
            if resp.data:
                _response_data_list = str(resp.data).split('\\n')
                if _response_data_list and len(_response_data_list) > 12:
                    _response_list = _response_data_list[:10]
                    _response_list.append("  .........")
                    _response_list.extend(_response_data_list[-2:])
                    _response_data = os.linesep.join(_response_list)
                else:
                    _response_data = os.linesep.join(_response_data_list)
                _response_string = "Response:" + os.linesep + "Content-type:" + resp.content_type + os.linesep + _response_data
                if _response_string:
                    logger.debug(_response_string)

        escaped_time = time.time() * 1000 - request.start_time
        logger.info("%d msecs used." % int(escaped_time))
    except:
        pass


@application.after_request
def after_request(resp):
    resp.headers['Access-Control-Allow-Origin'] = request.headers.get(
        'Origin', 'api.lovewawa.com.cn')

    resp.headers['Access-Control-Max-Age'] = 10

    log_response(resp)
    return resp


# import all routes here
from . import (user, registration, province_city, hospitals, levels, departments, doctors, treatments, patients,
               favorites, flows, fake_doctors, symps, questions, error_handler)



