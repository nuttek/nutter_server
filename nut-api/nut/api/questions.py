# -*- coding: utf-8 -*-
__author__ = 'brucelee'

from flask import jsonify, request

from nut.api import Session
from nut.api import application as app
from nut.api.cached.user import get_cached_user
from nut.core.errors import BadParameterError, NotFoundError
from nut.db.models import Questions, Answers, Comments, Votes
from nut.utils.decorator import requires_auth, requires_pagination
from nut.utils.misc import get_pagination_objs_and_next_id

MAX_CONTENT_LENGTH = 500
VOTE_ACTIONS = ["downvote", "upvote", "like"]
RECORDS_SIZE = 5


@app.route('/questions', methods=['POST'])
@requires_auth
def do_post_questions():
    user_id = request.user.id

    subject = request.json.get('subject', None)
    description = request.json.get('description', None)

    if subject is None or description is None:
        raise BadParameterError('BadParameterError_Empty_Not_Allowed', '参数不可为空！')
    else:
        if len(description) >= MAX_CONTENT_LENGTH:
            raise BadParameterError('BadParameterError_Exceed_MAX_Length',
                                    '描述长度过长，超过最大允许值（ %d）！' % MAX_CONTENT_LENGTH)

    q = Questions(
        user_id=user_id,
        subject=subject,
        description=description
    )
    Session.add(q)
    Session.commit()

    return jsonify(success=True, question=q.short_json())


@app.route('/questions/<int:q_id>/answers', methods=['POST'])
@requires_auth
def do_post_answers(q_id):
    user_id = request.user.id

    description = request.json.get('description', None)

    if description is None:
        raise BadParameterError('BadParameterError_Empty_Not_Allowed', '参数不可为空！')
    else:
        if len(description) >= MAX_CONTENT_LENGTH:
            raise BadParameterError('BadParameterError_Exceed_MAX_Length',
                                    '描述长度过长，超过最大允许值（ %d）！' % MAX_CONTENT_LENGTH)

    a = Answers(
        question_id=q_id,
        user_id=user_id,
        description=description
    )
    Session.add(a)
    Session.commit()

    return jsonify(success=True, answer=a.short_json())


@app.route('/votes', methods=['POST'])
@requires_auth
def do_post_votes():
    user_id = request.user.id

    type = request.json.get('type', None)
    obj_id = request.json.get('obj_id', None)
    if type is None or obj_id is None:
        raise BadParameterError('BadParameterError', '参数错误！')

    action = request.json.get('action', 0)  # 0 - downvote; 1 - upvote

    if action not in VOTE_ACTIONS:
        raise BadParameterError('BadParameterError_Action_Not_Allowed', '不允许的操作！')

    vote_json, counter_json = handle_vote_action(user_id, type, obj_id, action)

    return jsonify(success=True, vote=vote_json, counter=counter_json)


def get_action_value(action):
    if action == "downvote": return 0
    elif action == "upvote": return 1

    return 2


def handle_vote_action(user_id, type, obj_id, action):
    is_downvote = True
    is_upvote = not is_downvote

    if action == 1:
        is_downvote = False
        is_upvote = not is_downvote

    vote_obj = Session.query(Votes).filter(Votes.user_id == user_id, Votes.obj_type == type,
                                           Votes.obj_id == obj_id).first()

    if not vote_obj:
        vote_obj = Votes(
            user_id=user_id,
            obj_type=type,
            obj_id=obj_id,
            action=action
                  )

        Session.add(vote_obj)
        # ToDo update_redis()
    else:
        if action != vote_obj.action:
            # ToDo update_redis()
            vote_obj.action = action

    Session.commit()

    return (dict(is_upvote=is_upvote, is_downvote=is_downvote,created_time=vote_obj.created_time),
            [dict(type=3, value=100), dict(type=4, value=80)])


@app.route('/comments', methods=['POST'])
@requires_auth
def do_post_comments():
    user_id = request.user.id

    type = request.json.get('type', None)
    obj_id = request.json.get('obj_id', None)
    if type is None or obj_id is None:
        raise BadParameterError('BadParameterError', '参数错误！')

    description = request.json.get('description', None)

    if description is None:
        raise BadParameterError('BadParameterError_Empty_Not_Allowed', '参数不可为空！')
    else:
        if len(description) >= MAX_CONTENT_LENGTH:
            raise BadParameterError('BadParameterError_Exceed_MAX_Length',
                                    '描述长度过长，超过最大允许值（ %d）！' % MAX_CONTENT_LENGTH)

    c = Comments(
        user_id=user_id,
        obj_type=type,
        obj_id=obj_id,
        description=description
    )

    Session.add(c)
    Session.commit()

    return jsonify(success=True, comment=c.short_json())


@app.route('/questions', methods=['GET'])
@requires_pagination
def do_get_questions():

    filter_conditions = []
    if request.next_id:
        filter_conditions.append(Questions.id <= request.next_id)

    question_objs = Session.query(Questions).filter(*filter_conditions)\
        .order_by(Questions.id.desc()).limit(request.length + 1)

    enhanced_q_list = []

    question_objs, next_id = get_pagination_objs_and_next_id(question_objs, request.length)

    for q_obj in question_objs:
        q_json, counter_list, user_list = decorate_question_obj(q_obj)

        enhanced_q_list.append(dict(question=q_json, counters=counter_list, users=user_list))

    return jsonify(success=True, data=enhanced_q_list, next_id=next_id)


def decorate_question_obj(q_obj):
    # ToDo add redis counter handling
    counters = [dict(type=0, value=50),dict(type=2, value=200)]

    answer_user_ids = Session.query(Answers.user_id).filter(Answers.question_id == q_obj.id)\
        .order_by(Answers.created_time.desc()).limit(RECORDS_SIZE)

    user_list = [get_cached_user(u_id).basic_profile() for u_id in answer_user_ids]

    return q_obj.json(), counters, user_list


@app.route('/questions/<int:q_id>/answers', methods=['GET'])
@requires_pagination
def do_get_answers_of_question(q_id):

    filter_conditions = []
    if request.next_id:
        filter_conditions.append(Answers.id <= request.next_id)

    answer_objs = Session.query(Answers).filter(*filter_conditions)\
        .order_by(Answers.id.desc()).limit(request.length + 1)

    enhanced_a_list = []

    answer_objs, next_id = get_pagination_objs_and_next_id(answer_objs, request.length)

    for a_obj in answer_objs:
        a_json, counter_list = decorate_answer_obj(a_obj)

        enhanced_a_list.append(dict(answer=a_json, counters=counter_list))

    counters = [dict(type=0, value=18)]

    return jsonify(success=True, data=enhanced_a_list, next_id=next_id, counters=counters)


def decorate_answer_obj(a_obj):
    # ToDo add redis counter handling
    counters = [dict(type=1, value=80), dict(type=3, value=80),dict(type=4, value=20)]

    return a_obj.json(), counters


@app.route('/comments', methods=['GET'])
@requires_pagination
def do_get_comments():

    type = request.json.get('type', None)
    obj_id = request.json.get('obj_id', None)

    if type is None or obj_id is None:
        raise BadParameterError('BadParameterError', '参数错误！')

    comment_objs = Session.query(Comments).filter(Comments.obj_type == type, Comments.obj_id == obj_id)\
        .limit(request.length + 1)

    comment_objs, next_id = get_pagination_objs_and_next_id(comment_objs, request.length)

    counters = [dict(type=1, value=18)]

    return jsonify(success=True, comments=[c.json() for c in comment_objs], next_id=next_id, counters=counters)

