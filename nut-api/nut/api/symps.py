#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'
import logging, random
from flask import request, jsonify
from sqlalchemy import not_

from nut.api import application as app
from nut.api import Session
from nut.core import errors
from nut.db.models import (Symps)
from nut.utils.decorator import requires_auth

logger = logging.getLogger(__name__)


@app.route('/symps', methods=['GET'])
def do_get_symps():
    parent_symp_objs = Session.query(Symps).filter(Symps.parent_id.is_(None)).order_by(Symps.id.asc()).all()

    s_list = []
    for p_s_obj in parent_symp_objs:
        child_symp_list = get_symp_list(p_s_obj.id)
        s_list.append(dict(id=p_s_obj.id, name=p_s_obj.name, symps=child_symp_list))

    return jsonify(success=True, body_parts=s_list)


def get_symp_list(parent_symp_id):
    c_objs = Session.query(Symps).filter(Symps.parent_id == parent_symp_id).order_by(Symps.id.asc()).all()

    return [c_obj.dict() for c_obj in c_objs]
