#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'
import logging
import random
from flask import request, jsonify
from sqlalchemy import text

from nut.api import application as app, Session
from nut.api import Session
from nut.core import errors
from nut.utils.customerized_data_type import enum
from nut.db.models import Hospitals, Departments, Doctors, TreatmentTime, TreatmentProgress, TreatmentFee
from nut.utils.decorator import requires_auth

STATUSES = dict(
    tr_lineup=[dict(frm=dict(c='ptr', cc='done'), to=dict(c='tr', cc='untr'))],
    tr=[dict(frm=dict(c='tr', cc='untr'), to=dict(c='tr', cc='done'))],
    chk_list_ready=[dict(frm=dict(c='tr', cc='done'), to=dict(c='chk', cc='unpaid'))],
    chk_ready=[dict(frm=dict(c='chk', cc='unchk'), to=dict(c='chk', cc='done'))],
    # disp_ready=[dict(frm=dict(c='tr', cc='done'), to=dict(c='disp', cc='unpaid'))],
    disp_ready=[dict(frm=dict(c='chk', cc='done'), to=dict(c='disp', cc='unpaid'))],
    disp=[dict(frm=dict(c='disp', cc='undisp'), to=dict(c='disp', cc='done'))]
)


@app.route('/his', methods=['POST'])
def status_tranfer():
    from .flows import get_latest_progress, get_flow_status

    registration_id = request.json.get('registration_id', None)
    action = request.json.get('action', None)

    if not action or action not in ['tr_lineup', 'tr', 'chk_list_ready', 'chk_ready', 'disp_ready', 'disp']:
        raise errors.BadParameterError('BadParameterError_Invalid_Action', '不合法的操作！')

    tg_obj = get_latest_progress(registration_id)
    from_c = tg_obj.status_code
    from_cc = tg_obj.child_status_code

    is_status_matched = False
    for status_dict in STATUSES.get(action, None):
        c_ = status_dict.get('frm').get('c')
        cc_ = status_dict.get('frm').get('cc')
        if from_c == c_ and from_cc == cc_:
            is_status_matched = True
            to_c = status_dict.get('to').get('c')
            to_cc = status_dict.get('to').get('cc')

    if not is_status_matched:
        raise errors.BadStatusError('BadStatusError_Wrong_Status', '不可在现有状态下进行此类操作（action=%s）！' % action)

    new_tg_obj = Session.query(TreatmentProgress).filter(TreatmentProgress.registration_id == registration_id,
                                                         TreatmentProgress.status_code == to_c,
                                                         TreatmentProgress.child_status_code == to_cc).first()

    if new_tg_obj:
        raise errors.BadStatusError('BadStatusError_Already_Such_Status', '期望状态已存在！')
    else:
        new_tg_obj = TreatmentProgress(
            registration_id=registration_id,
            user_id=tg_obj.user_id,
            doctor_id=tg_obj.doctor_id,
            patient_id=tg_obj.patient_id,
            status_code=to_c,
            child_status_code=to_cc
        )
        Session.add(new_tg_obj)

    if action in ['chk_list_ready', 'disp_ready']:
        _ready(registration_id, action.split('_')[0])

    Session.commit()

    return jsonify(success=True, status=get_flow_status(to_c, to_cc))


def _ready(registration_id, status):
    tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                TreatmentFee.status == status).first()
    fee = random.randint(88, 388)
    if tf_obj:
        tf_obj.fee = fee
    else:
        tf_obj = TreatmentFee(
            registration_id=registration_id,
            status=status,
            fee=fee
        )

        Session.add(tf_obj)

    tp_obj = Session.query(TreatmentProgress).filter(TreatmentProgress.registration_id == str(registration_id),
                                                     TreatmentProgress.status_code == status,
                                                     TreatmentProgress.child_status_code == 'unpaid').first()

    if tp_obj:
        tp_obj.datetime = text('now()')
    else:
        from .flows import get_latest_progress

        latest_tp_obj = get_latest_progress(registration_id)

        tp_obj = TreatmentProgress(
            registration_id=registration_id,
            user_id=latest_tp_obj.user_id,
            doctor_id=latest_tp_obj.doctor_id,
            patient_id=latest_tp_obj.patient_id,
            status_code=status,
            child_status_code='unpaid'
        )

        Session.add(tp_obj)


'''
    automatically generate treatment records
'''
def generate_treatments(tts, doctor_id):
    import random

    from nut.api.cached.treatments import get_cached_treatment_dates, treatment_periods
    from nut.api.treatments import get_treatments_json, only_one_treatment

    workdays = get_cached_treatment_dates()
    times = treatment_periods

    if tts:
        if len(tts) > 1:
            treatment_days = get_treatments_json(tts)
        elif len(tts) == 1:
            treatment_days = only_one_treatment(tts[0])
    else:
        treatment_days = []

    days_loop = random.randint(6, 8)
    for i in range(days_loop):
        days_num = random.randint(0, 9)
        _date = workdays[days_num]
        if _date in [t['date'] for t in treatment_days]:
            continue

        _times = []
        times_loop = random.randint(2, 5)
        for i in range(times_loop):
            times_num = random.randint(0, 1)
            if times[times_num] in [_t['type'] for _t in _times]:
                continue

            _times.append(dict(type=times[times_num], original_amount=20, remaining_amount=random.randint(5, 20),
                               price=random.randint(8, 35)))

        treatment_days.append(dict(date=_date, times=_times))

        dump_treatment_days_in_db(doctor_id, treatment_days)

    return sorted(treatment_days, key=lambda td: td['date'])


def dump_treatment_days_in_db(doctor_id, treament_days):
    import traceback

    for td in treament_days:
        _date = td.get('date', None)
        if not _date: continue
        _times = td.get('times', None)
        if not _times: continue

        for _t in _times:
            _type = _t.get('type', None)
            _o_amount = _t.get('original_amount', 0)
            _r_amount = _t.get('remaining_amount', 0)
            _price = _t.get('remaining_amount', 0)

            tt_obj = Session.query(TreatmentTime).filter(TreatmentTime.doctor_id == doctor_id,
                                                         TreatmentTime.date == _date,
                                                         TreatmentTime.time == _type).first()
            if tt_obj: continue

            tt_obj = TreatmentTime(
                doctor_id=doctor_id,
                type=_type,
                date=_date,
                original_amount=_o_amount,
                remaining_amount=_r_amount,
                time=_type,
                price=_price
            )
            try:
                Session.add(tt_obj)
            except:
                traceback.print_exc()
    Session.commit()


def generate_registration_id():
    import time

    id = str(int(time.time()*1000))
    return str(int(id[-8:]))


def generate_linup_pos():
    import random

    return str(random.randint(15, 50))


def set_flow_status(registration_id, code, child_code):
    from nut.db.models import TreatmentRegistration

    tr = Session.query(TreatmentRegistration).filter(TreatmentRegistration.registration_id == str(registration_id)).first()
    tr.code = code
    tr.child_code = child_code

    Session.commit()


def set_flow_fee(registration_id, code, child_code):
    import random
    from nut.db.models import TreatmentFee

    fee = random.randint(18, 398)

    tr = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id)).first()
    if code == 'chk':
        type = 100
    elif code == 'disp':
        type = 200

    if not tr:
        tr = TreatmentFee(
            registration_id=registration_id,
            type=type,
            fee=fee
        )
        Session.add(tr)
    else:
        tr.code = code
        tr.type = type

    Session.commit()


def get_fake_chk_report():
    return [
            dict(name='白细胞计数', code_name='WBC', value=13.3, unit='*10^9/L', range='4.0-10.0'),
            dict(name='红细胞计数', code_name='RBC', value=4.91, unit='*10^12/L', range='4.0-5.50'),
            dict(name='血红蛋白测定', code_name='HGB', value=151, unit='g/L', range='110-160'),
            dict(name='红细胞比积', code_name='HCT', value=45.1, unit='%', range='37.0-54.0'),
            dict(name='平均红细胞血红蛋白含量', code_name='MCH', value=30.8, unit='pg', range='27.0-34.0'),
            dict(name='平均红细胞血红蛋白浓度', code_name='MCHC', value=335, unit='g/L', range='320-360'),
            dict(name='红细胞体积分布宽度', code_name='RDW', value=11.8, unit='%', range='11.6-16.5'),

            dict(name='血小板计数', code_name='PLT', value=257, unit='*10^9/L', range='100-300'),
            dict(name='血小板比积', code_name='PCT', value=0.18, unit='%', range='0.10-0.3'),
            dict(name='血小板平均体积', code_name='MPV', value=6.9, unit='fL', range='6.5-11.5'),
            dict(name='血小板体积分布宽度', code_name='PDW', value=18.0, unit='%', range='12.5-22.5'),

            dict(name='淋巴细胞绝对值', code_name='LYM', value=2.1, unit='*10^9/L', range='0.8-4.0'),
            dict(name='单核细胞绝对值', code_name='MON', value=0.5, unit='*10^9/L', range='0.8-4.0'),
            dict(name='中性粒细胞绝对值', code_name='NEUT', value=10.5, unit='*10^9/L', range='2.0-7.0'),
            dict(name='嗜酸性粒细胞绝对值', code_name='EOS', value=0.1, unit='*10^9/L', range='0.0-0.5'),
            dict(name='嗜碱性粒细胞绝对值', code_name='BAS', value=0.1, unit='*10^9/L', range='0.0-0.1'),

            dict(name='淋巴细胞百分率', code_name='LYM%', value=15.5, unit='%', range='20.0-40.0'),
            dict(name='单核细胞百分率', code_name='BAS', value=3.5, unit='%', range='3.0-8.0'),
            dict(name='中性粒细胞百分率', code_name='BAS', value=80.2, unit='%', range='50.0-70.0'),
            dict(name='嗜酸性细胞百分率', code_name='BAS', value=0.4, unit='%', range='0.0-5.0'),
            dict(name='嗜碱性细胞百分率', code_name='BAS', value=0.4, unit='%', range='0.0-0.1')
        ]


def get_fake_med_report():
    return [
            dict(name='青霉素钠针', type='（甲）', value=5, unit='瓶', price=1.5, self_finance=0.0),
            dict(name='5%CS 250ml*1瓶', type='（甲）', value=1, unit='瓶', price=1.91, self_finance=0.0),
            dict(name='0.9%氯化钠注射液 250', type='（甲）', value=1, unit='瓶', price=1.87, self_finance=0.0),
            dict(name='利巴韦林针 0.25g*1支', type='（甲）', value=3, unit='支', price=12.5, self_finance=0.0),
            dict(name='莲花清瘟胶囊 0.35克', type='（甲）', value=1, unit='盒', price=10.8, self_finance=0.0),
            dict(name='复方甘草合剂 180ml*1', type='（甲）', value=1, unit='瓶', price=2.89, self_finance=0.0),
            dict(name='维生素C', type='（甲）', value=2, unit='盒', price=1.8, self_finance=0.0)
        ]


def generate_payment(fee):
    _fee = fee
    medical_insurance = random.randint(_fee - 50, _fee - 5)
    self_finance = _fee - medical_insurance

    return medical_insurance, self_finance
