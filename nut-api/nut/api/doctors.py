#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from flask import request, jsonify, abort

from nut.api import application as app
from nut.api.cached.doctors import (get_cached_all_doctors)
from nut.utils.decorator import conditional_last_modified_date, requires_pagination
from nut.utils.misc import get_pagination_of_objs


@app.route('/doctors', methods=['GET'])
@requires_pagination
def do_get_doctors():
    hospital_id = request.args.get('hospital_id', None)
    department_id = request.args.get('department_id', None)
    level = request.args.get('level', None)

    _doctors = get_doctors_by_conditions(hospital_id, department_id, level)

    _doctors, _next_id = get_pagination_of_objs(_doctors, request.next_id, request.length)
    _doctors = [d.dict() for d in _doctors]

    return jsonify(success=True, doctors=_doctors, next_id=_next_id)


# doctors filtered by combined conditions
def get_doctors_by_conditions(hospital_id, department_id, level):
    doctors = get_cached_all_doctors()

    def h(_d,_h_id):
        if _h_id:
            return _d.hosp_id == int(_h_id)
        return True

    def dept(_d,_d_id):
        if _d_id:
            return _d.dept_id == int(_d_id)
        return True

    def l(_d,_level):
        if _level:
            return _d.level == int(_level)
        return True

    doctors = list(filter(lambda d: h(d, hospital_id) and dept(d, department_id) and l(d, level), doctors))

    return doctors


# doctor fetched by id
def get_doctor_by_id(doctor_id):
    _doctors = get_cached_all_doctors()
    _doctor = list(filter(lambda d: d.id == doctor_id, _doctors))[0]

    return _doctor
