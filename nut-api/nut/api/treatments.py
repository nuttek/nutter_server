#!/usr/bin/env python
# -*- coding: utf-8 -*-
from nut.api.fake_doctors import generate_treatments

__author__ = 'brucelee'
from flask import request, jsonify

from nut.api import application as app
from nut.api import Session
from nut.core import errors
from nut.db.models import Doctors, TreatmentTime

from .fake_doctors import generate_treatments


@app.route('/treatments', methods=['GET'])
def do_get_treatments():

    hosp_id = request.args.get("hospital_id", None)
    dept_id = request.args.get("department_id", None)
    doctor_ids = request.args.get("doctor_ids", None)

    if not doctor_ids and not (hosp_id and dept_id):
        raise errors.BadParameterError('BadParameterError_treatment', '参数错误！')

    if doctor_ids:
        doctor_ids = doctor_ids.split(',')
        doctors = Session.query(Doctors).filter(Doctors.id.in_(doctor_ids)).order_by(Doctors.id.asc()).all()
    else:
        doctors = Session.query(Doctors).filter(Doctors.hosp_id == hosp_id, Doctors.dept_id == dept_id)\
            .order_by(Doctors.id.asc()).all()

    doctor_list = []
    for d in doctors:
        _dict = d.dict()
        _dict.update(treatments=get_treatments(d.id))
        doctor_list.append(_dict)

    return jsonify(success=True, doctors=doctor_list)


def only_one_treatment(t):
    _date = t.date
    _type = t.time
    _o_amount = t.original_amount
    _r_amount = t.remaining_amount

    times = []
    times.append(dict(type=_type, original_amount=_o_amount, remaining_amount=_r_amount, price=t.price))

    return dict(date=_date, times=times)


def get_treatments_json(tts):

    treatments = []

    current_date = None
    times = []
    loop = 1
    for t in tts:
        _date = t.date
        _type = t.time
        _o_amount = t.original_amount
        _r_amount = t.remaining_amount
        _price = t.price

        if not current_date:  # first record
            times.append(dict(type=_type, original_amount=_o_amount, remaining_amount=_r_amount, price=_price))
            current_date = _date
        elif current_date != _date:
            treatments.append(dict(date=current_date, times=times))
            times = []
            times.append(dict(type=_type, original_amount=_o_amount, remaining_amount=_r_amount, price=_price))
            current_date = _date
            if loop == len(tts):
                treatments.append(dict(date=current_date, times=times))
        else:
            times.append(dict(type=_type, original_amount=_o_amount, remaining_amount=_r_amount, price=_price))
            if loop == len(tts):
                treatments.append(dict(date=current_date, times=times))

        loop += 1

    return treatments


def get_treatments(doctor_id, date=None, type=None):
    import datetime

    filter_conditions = [TreatmentTime.doctor_id == doctor_id]

    if date:
        filter_conditions.append(TreatmentTime.date == date)
    else:
        today = datetime.date.today()
        _date = '%d-%02d-%02d' % (today.year, today.month, today.day)
        filter_conditions.append(TreatmentTime.date >= _date)

    if type is not None:
        filter_conditions.append(TreatmentTime.time == type)

    tts = Session.query(TreatmentTime).filter(*filter_conditions)\
        .order_by(TreatmentTime.date.asc()).all()

    if date is not None and type is not None:
        if tts and len(tts) >= 1:
            return only_one_treatment(tts[0])

    return generate_treatments(tts, doctor_id)


def format_treatments(tts, doctor_id):
    tts_size = len(tts)
    if tts and tts_size > 0:
        if tts_size == 1:
            return only_one_treatment(tts[0])

        return get_treatments_json(tts)
    else:
        return generate_treatments(doctor_id)







