#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from flask import request, jsonify

from nut.api import application as app
from nut.api.cached.province_city import (get_cached_provinces_last_modified_date, get_cached_cities_last_modified_date,
                                          get_cached_counties_last_modified_date)
from nut.core.constants import OBJ_TYPE as LMD, HTTP_HEADERS, HTTP_DATE_FORMAT
from nut.utils.decorator import conditional_last_modified_date, requires_pagination
from nut.utils.misc import format_datetime, get_pagination_of_objs
from nut.api.cached.province_city import (get_cached_all_provinces, get_cached_all_cities, get_cached_all_counties,
                                          get_cached_cities, get_cached_counties)


@app.route('/provinces', methods=['GET'])
@conditional_last_modified_date(obj=LMD.PROVINCE)
@requires_pagination
def do_get_provinces():
    cached_provinces = get_cached_all_provinces()

    province_list, _next_id = get_pagination_of_objs(cached_provinces, request.next_id, request.length)

    province_list = [p.dict() for p in province_list]

    resp = jsonify(success=True, provinces=province_list, next_id=_next_id)
    resp.headers[HTTP_HEADERS.LAST_MODIFIED] = format_datetime(get_cached_provinces_last_modified_date(),
                                                               HTTP_DATE_FORMAT, True)

    return resp


@app.route('/provinces/<province_id>/cities', methods=['GET'])
@conditional_last_modified_date(obj=LMD.CITY)
@requires_pagination
def do_get_cities(province_id):
    cached_cities = get_cached_cities(province_id)

    city_list, _next_id = get_pagination_of_objs(cached_cities, request.next_id, request.length)

    city_list = [c.dict() for c in city_list]

    resp = jsonify(success=True, cities=city_list, next_id=_next_id)
    resp.headers[HTTP_HEADERS.LAST_MODIFIED] = format_datetime(get_cached_cities_last_modified_date(int(province_id)),
                                                               HTTP_DATE_FORMAT, True)

    return resp


@app.route('/cities/<city_id>/counties', methods=['GET'])
@conditional_last_modified_date(obj=LMD.COUNTY)
@requires_pagination
def do_get_counties(city_id):
    cached_counties = get_cached_counties(city_id)

    county_list, _next_id = get_pagination_of_objs(cached_counties, request.next_id, request.length)

    county_list = [c.dict() for c in county_list]

    resp = jsonify(success=True, counties=county_list, next_id=_next_id)
    resp.headers[HTTP_HEADERS.LAST_MODIFIED] = format_datetime(get_cached_counties_last_modified_date(city_id),
                                                               HTTP_DATE_FORMAT, True)

    return resp


# for client side to initialize geo data
@app.route('/app_data', methods=['GET'])
def get_all_geo_data():
    from nut.api.cached.hospitals import get_cached_all_hospitals
    from nut.api.departments import get_all_departments
    from nut.api.cached.levels import get_cached_all_levels

    _provinces = get_cached_all_provinces()
    _provinces = [o.dict() for o in _provinces]

    _cities = get_cached_all_cities()
    _cities = [o.dict() for o in _cities]

    _counties = get_cached_all_counties()
    _counties = [o.dict() for o in _counties]

    _hospitals = get_cached_all_hospitals()
    _hospitals = [o.full_dict() for o in _hospitals]

    _departments = get_all_departments()

    _levels = get_cached_all_levels()
    _levels = [l.dict() for l in _levels]

    return jsonify(success=True, provinces=_provinces, cities=_cities, counties=_counties,
                   hospitals=_hospitals, departments=_departments, levels=_levels)
