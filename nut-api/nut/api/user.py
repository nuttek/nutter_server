# -*- coding: utf-8 -*-

import re
import logging

from flask import abort, jsonify, request, session
from sqlalchemy import text
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from nut.config import config
from nut.utils.misc import verify_SSHA_password,  make_SSHA_password, send_apn_message, is_valid_length, get_badge_count
from nut.utils.img import ProfileImageResizePolicy
from nut.api import Session
from nut.api import application as app
from nut.api import cache
from nut.api.cached.user import get_cached_user
from nut.api.cached.terminal import get_cached_terminal
from nut.core.errors import UserError
from nut.utils.decorator import requires_auth, private_api
from nut.db.models import Users, Terminals
from nut.core.pns.constants import PNS_PROVIDER
from nut.core.pns import generate_empty_notification
from .registration import register_mobile_or_reset_password

logger = logging.getLogger(__name__)


@app.route('/users/<user_id>/profile', methods=['POST', 'PUT'])
@requires_auth
def do_put_user_profile(user_id):
    img = request.files.get('profile_img')
    name = request.form.get('nick_name')
    description = request.form.get('description')

    user_id = request.user.id
    user = Session.query(Users).get(user_id)
    is_changed = False

    if name is not None:
        is_name_valid = is_valid_length(name,
                                        config.USER_PROFILE_NAME_MIN_LIMIT,
                                        config.USER_PROFILE_NAME_MAX_LIMIT)
        if is_name_valid < 0:
            raise UserError('USER_PROFILE_NAME_TOO_SHORT',
                            '用户名必须超过%s个字符' %
                            config.USER_PROFILE_NAME_MIN_LIMIT)
        elif is_name_valid > 0:
            raise UserError('USER_PROFILE_NAME_TOO_LONG',
                            '用户名不能超过%s个字符' %
                            config.USER_PROFILE_NAME_MAX_LIMIT)

        if not re.match(config.REGEX_PATTERN_USER_NAME, name):
            raise UserError('USER_PROFILE_BAD_NAME', '用户名含非法字符')

        is_changed = True
        user.name = name

    if description is not None:
        is_desc_valid = is_valid_length(
            description, 0, config.USER_PROFILE_DESCRIPTION_MAX_LIMIT)
        if is_desc_valid > 0:
            raise UserError('USER_PROFILE_DESCRIPTION_TOO_LONG',
                            '简介不能超过%s个字符' %
                            config.USER_PROFILE_DESCRIPTION_MAX_LIMIT)
        is_changed = True
        user.description = description
    if img and img.filename:
        img_path = ProfileImageResizePolicy.postImg(img, user.id)

        if img_path:
            is_changed = True
            user.profile_img_url = img_path
    if is_changed:
        user.modified = text('now()')
        Session.commit()
        cache.delete_memoized(get_cached_user, user.id)

    user = get_cached_user(user.id)
    p = user.basic_profile()

    return jsonify(success=True, user_info=p)


def _reset_password(mobile):
    vcode = request.json.get("vcode", None)
    password = request.json.get("password", None)

    return register_mobile_or_reset_password(mobile, password, vcode, False)


def _change_password():
    old = request.args.get('old_password', None)
    if old is None:
        old = request.json.get('old_password', None)
    new = request.args.get('new_password', None)
    if new is None:
        new = request.json.get('new_password', None)

    if not (old and new):
        raise UserError('INVALID_PARAMS', "参数不正确")

    uid = request.user.id
    user = Session.query(Users).get(uid)

    if user.mobile is None:
        raise UserError('CHANGEPASS_NOT_ALLOWED', "修改密码只能针对手机注册用户")

    if not verify_SSHA_password(user.passwd, old):
        raise UserError("INVALID_PASSWORD", "密码不正确")

    user.passwd = make_SSHA_password(new.encode('utf-8')).decode('utf-8')
    Session.commit()

    return user.id


@requires_auth
def change_password(user_id):
    user_id = _change_password()

    return user_id


@app.route('/users/<mobile_number_or_user_id>/password', methods=['POST', 'PUT'])
def do_put_reset_or_change_password(mobile_number_or_user_id):
    action = request.args.get('action', None)
    if action and action == "reset":
        _reset_password(mobile_number_or_user_id)
        return jsonify(success=True)

    user_id = change_password(mobile_number_or_user_id)
    return jsonify(success=True, user_info=dict(id=user_id))


@app.route('/usercookie/<int:user_id>')
@private_api
def get_usercookie(user_id):
    session['userid'] = user_id
    return jsonify(success=True)


def _update_terminal_info(user_id, terminal_id, is_online=False):
    if not (terminal_id and user_id): return

    _is_got_first_terminal = True

    terminals = Session.query(Terminals)\
        .filter(Terminals.terminal_id == terminal_id)\
        .order_by(Terminals.created.desc())\
        .all()
    if terminals:
        for t in terminals:
            if _is_got_first_terminal:
                t.user_id = user_id
                t.is_online = is_online
                t.last_login_time = text('now()')
                _is_got_first_terminal = False
            else:
                Session.query(Terminals).filter(Terminals.id == t.id).delete()

        Session.commit()


@app.route('/signin', methods=['POST'])
def signin():

    if session.get('userid'):
        do_get_signout()

    # TODO (likui@nut.com) handlers for different login fashions
    mobile = request.json.get("mobile", None)
    if mobile: return signin_by_mobile(mobile)

    sina_weibo_uid = request.json.get('sina_weibo_uid', '').strip() or None
    sina_weibo_oauth2_token = request.json.get('sina_weibo_oauth2_token',
                                               '').strip() or None
    tencent_openid = request.json.get('tencent_openid', '').strip() or None

    if not (sina_weibo_uid or tencent_openid):
        abort(400)

    user_info = {
        'name': request.json.get('name', '').strip(),
        'profile_img_url': request.json.get('profile_img_url', '').strip(),
        'gender': request.json.get('gender', 0),
        'description': request.json.get('description', '').strip(),
        'openudid': request.json.get('openudid', '').strip(),
        'bundle_version': request.json.get('bundle_version', '').strip(),
    }

    try:
        is_new = False
        if sina_weibo_uid:
            user_info.update({
                'sina_weibo_uid': sina_weibo_uid,
                'sina_weibo_oauth2_token': sina_weibo_oauth2_token})
            user = Session.query(Users).filter(
                Users.sina_weibo_uid == sina_weibo_uid).one()
        elif tencent_openid:
            user_info['tencent_openid'] = tencent_openid
            user = Session.query(Users).filter(
                Users.tencent_openid == tencent_openid).one()
        else:
            abort(400)
        for k, v in user_info.items():
            if k not in ('name', 'profile_img_url', 'description'):
                setattr(user, k, v)
        user.last_login_time = text('now()')
    except NoResultFound:
        is_new = True
        user = Users(**user_info)
        Session.add(user)
        Session.commit()

    Session.commit()
    session['userid'] = user.id
    cache.delete_memoized(get_cached_user, user.id)
    user = get_cached_user(user.id)

    '''
    for old client, just postpone terminal data creation to /push_token
    '''
    terminal_id = request.json.get('terminal_id', '').strip()
    if terminal_id:
        _update_terminal_info(user.id, terminal_id, True)
        session['terminalid'] = terminal_id

    badge_count = get_badge_count(user.id)
    send_apn_message(user.id, generate_empty_notification(), badge_count)

    return jsonify(success=True, user_info=_ret_user_profile(user, is_new),
                   badge_count=badge_count, favorites=_get_favorites_with_types(user.id))


def _get_favorites_with_types(user_id, is_entity_required=False):
    from nut.api.favorites import get_favorites_with_types

    f_list = []
    for t in [0, 1]:
        f_objs = get_favorites_with_types(user_id, t, is_entity_required)
        if f_objs:
            _f_objs = f_objs.get('obj_ids', None) or f_objs.get('objs', None)
            if _f_objs and len(_f_objs) > 0:
                f_list.append(f_objs)
    return f_list


def signin_by_mobile(mobile):

    password = request.json.get('password', None)
    terminal_id = request.json.get('terminal_id', None)
    if not terminal_id:
        terminal_id = request.json.get('device_token', None)
        is_old_client = True

    _log_parameters_str = "{mobile:%s, terminal_id:%s}" %(mobile, terminal_id)
    logger.info("Signin with mobile:" + _log_parameters_str)

    if password is None:
        logger.warning("Password is empty:" + _log_parameters_str)
        abort(400)

    user = Session\
                .query(Users)\
                .filter(Users.mobile == mobile)\
                .first()

    if user is None:
        logger.warning("Mobile number doesn't exist:" + _log_parameters_str)
        raise  UserError("USER_NOT_EXISTED", "该手机号未注册")

    if not verify_SSHA_password(user.passwd, password):
        logger.warning("Wrong password:" + _log_parameters_str)
        raise  UserError("INVALID_PASSWORD", "密码不正确")

    if user.profile_img_url is None:
        session['userid'] = user.id
        logger.warning("No profile img set:" + _log_parameters_str +
                       "{user_id:%d, session already set: %s}" % (user.id, str('userid' in session)))

    user.openudid = request.json.get('openudid', '').strip()
    user.bundle_version = request.json.get('bundle_version', '').strip()
    user.last_login_time = text('now()')

    terminal = get_cached_terminal(user.id, terminal_id)
    _update_terminal_info(user.id,terminal_id, True)

    Session.add(user)
    Session.flush()
    Session.commit()

    session['userid'] = user.id
    logger.info("Signin with mobile done" + _log_parameters_str +
                "{user_id:%d, session already set: %s}" % (user.id, str('userid' in session)))
    cache.delete_memoized(get_cached_user, user.id)
    user = get_cached_user(user.id)

    return jsonify(success=True, user_info=_ret_user_profile(user, False),
                   badge_count=get_badge_count(user.id), favorites=_get_favorites_with_types(user.id))


def _ret_user_profile(user, is_new):
    profile = user.basic_profile()
    profile.update({
        'is_new': is_new
    })

    return profile


def do_get_signout():
    user = request.user

    is_dirty_data = False
    if request.user:
        user = Session.query(Users).get(request.user.id)
        user.last_logout_time = text('now()')
        session.pop('userid', None)
        is_dirty_data = True

    if request.terminal:
        terminal = Session.query(Terminals).get(request.terminal.id)
        terminal.last_logout_time = text('now()')
        terminal.is_online = False
        is_dirty_data = True
        session.pop('terminalid', None)

    if is_dirty_data:
        Session.commit()

    if request.user:
        request.user = None
    if request.terminal:
        request.terminal = None


@app.route('/signout')
def signout():
    do_get_signout()
    return jsonify(success=True)


def do_add_terminal_or_token(is_default_PNS_provider=False):
    push_token = request.json.get('push_token', '').strip()
    bundle_version = request.json.get('bundle_version', '').strip()
    device_model = request.json.get('device_model', '').strip()
    os_version = request.json.get('os_version', '').strip()
    factory_os_version = request.json.get('factory_os_version', '').strip()
    terminal_id = request.json.get('terminal_id', '').strip()

    if is_default_PNS_provider:
        # Check and delete push_token which belong to others
        delete_count = Session.query(Terminals).filter(
            Terminals.device_token == push_token,
            Terminals.user_id != request.user.id, Terminals.token_type == PNS_PROVIDER.DEFAULT).delete()
        if delete_count:
            Session.commit()
    try:
        filter_conditions = []
        if is_default_PNS_provider:
            filter_conditions.extend([Terminals.user_id == request.user.id, Terminals.device_token == push_token,
                                      Terminals.token_type == PNS_PROVIDER.DEFAULT])
        else:
            filter_conditions.append(Terminals.terminal_id == terminal_id)

        dt = Session.query(Terminals).filter(*filter_conditions).first()
        if dt:
            dt.device_token = push_token
            dt.bundle_version = bundle_version
            dt.device_model = device_model
            dt.os_version = os_version
            dt.factory_os_version = factory_os_version
            dt.last_login_time = text('now()')
            if is_default_PNS_provider:
                dt.is_online = True
        else:
            user_id = request.user.id if request.user else None
            if is_default_PNS_provider:
                _token_type = PNS_PROVIDER.DEFAULT
            else:
                _token_type = PNS_PROVIDER.JPUSH
            dt = Terminals(
                user_id=user_id,
                device_token=push_token,
                bundle_version=bundle_version,
                device_model=device_model,
                token_type=_token_type,
                os_version=os_version,
                factory_os_version=factory_os_version,
                created=text('now()'),
                last_login_time=text('now()')
            )
            if is_default_PNS_provider:
                dt.terminal_id = push_token
                dt.is_online = True
            else:
                dt.terminal_id = terminal_id
            Session.add(dt)
            Session.flush()
        terminal_id = dt.terminal_id
        Session.commit()
        session['terminalid'] = terminal_id
    except IntegrityError:
        Session.rollback()


@app.route('/terminal', methods=['POST'])
def do_post_terminal():
    do_add_terminal_or_token()
    return jsonify(success=True)


def _check_block_request(blocked_user_id=None):
    if not blocked_user_id:
        blocked_user_id = request.json.get('blocked_user_id')
    if not blocked_user_id:
        abort(400)
    blocked_user_id = int(blocked_user_id)
    blocked_user = get_cached_user(blocked_user_id)
    if not blocked_user:
        raise UserError('BLOCK_FAILED_NOT_EXISTED', '该用户不存在')
    if blocked_user.id == request.user.id:
        raise UserError('BLOCK_FAILED_NOT_ALLOWED', '无法拉黑(或取消拉黑)自己')

    return blocked_user


def get_blocked_value(user_id, blocked_user_id_list):
    if user_id in blocked_user_id_list:
        return True
    return False


@app.route('/user/<uid>', methods=['DELETE'])
@private_api
def remove_user(uid):
    if uid is None: return

    filter_condition = []

    pn = request.args.get("type", None)
    if pn:
        filter_condition.append(Users.mobile == uid)
    else:
        filter_condition.append(Users.id == uid)

    Session.query(Users)\
        .filter(*filter_condition)\
        .delete(synchronize_session=False)
    Session.commit()

    return jsonify(success=True)
