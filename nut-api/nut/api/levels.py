#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from flask import request, jsonify

from nut.api import application as app
from nut.api.cached.levels import get_cached_hospital_levels, get_cached_doctor_levels
from nut.core.constants import LEVELS
from nut.utils.decorator import conditional_last_modified_date, requires_pagination
from nut.utils.misc import get_pagination_of_objs


@app.route('/levels', methods=['GET'])
@conditional_last_modified_date()
@requires_pagination
def do_get_levels():
    type = request.args.get("type", None)
    if int(type) == LEVELS.HOSPITAL:
        cached_levels = get_cached_hospital_levels()
    else:
        cached_levels = get_cached_doctor_levels()

    level_list, _next_id = get_pagination_of_objs(cached_levels, request.next_id, request.length)

    level_list = [h.dict() for h in level_list]

    return jsonify(success=True, levels=level_list, next_id=_next_id)









