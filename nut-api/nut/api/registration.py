# -*- coding: utf-8 -*-

__author__ = 'brucelee'

import hashlib

from flask import jsonify, request, session

from nut.config.config import SIGNATURE_FEED, VCODE_EXPIRATION_IN_SECONDS
from nut.utils.misc import is_legal_mobile_number, genrate_code
from nut.api import Session
from nut.api import application as app
from nut.utils.decorator import requires_auth, private_api
from nut.core.errors import UserError
from nut.utils.misc import post_sms, make_SSHA_password
from nut.db.models import Users
from .rds import get_rcode, save_rcode, delete_rcode, rcode_ttl, has_requestid, save_request_id


def genarate_and_send_vcode(mobile, sig):
    if is_legal_mobile_number(mobile):
        raise UserError("MOBILE_INCORRECT", "手机号输入有误，再来一次吧")

    if _is_legal_signature(sig, mobile):
        save_request_id(sig)

    vcode = get_rcode(mobile)
    if vcode:
        allow_resend = rcode_ttl(mobile) < VCODE_EXPIRATION_IN_SECONDS
        if allow_resend: return jsonify(success=True)

    vcode = genrate_code()
    save_rcode(mobile, vcode)
    send_checkcode(mobile, vcode)


@app.route('/vcode', methods=['GET'])
def do_get_verification_code():

    mobile = request.args.get('mobile', None)
    sig = request.args.get('signature', None)

    genarate_and_send_vcode(mobile, sig)

    return jsonify(success=True)


def register_mobile_or_reset_password(mobile, password, vcode, is_new_registration=True):
    user = _is_mobile_registed(mobile, is_new_registration)

    return save_user_by_mobile_number(mobile, vcode, password, user)


@app.route('/registration', methods=['POST'])
def do_post_registration():
    from nut.api.cached.user import get_cached_user

    mobile = request.json.get("mobile", None)
    vcode = request.json.get("vcode", None)
    password = request.json.get("password", None)
    terminal_id = request.json.get("terminal_id", None)

    # TODO (likui@nut.com) to bind terminal already existed

    user_id = register_mobile_or_reset_password(mobile, password, vcode)

    user = get_cached_user(user_id)
    p = user.basic_profile()

    return jsonify(success=True, user_info=p)


def _is_legal_signature(sig, mobile):
    if sig is None:
        raise UserError("SIG_NOT_FOUND", "Signature not found")

    seed = request.args.get('seed', None)
    if seed is None:
        raise UserError("SIG_NOT_FOUND", "Signature not found")

    csig = _calc_sig(mobile, seed)
    if sig != csig:
        raise UserError("SIG_BAD", "Bad signature")

    if has_requestid(sig):
        raise UserError("SIG_USED", "Signature is used.")

    return True


def _calc_sig(m, seed):
    base = ('%s:%s:%s' % (m, SIGNATURE_FEED, seed))
    return hashlib\
        .md5(base.encode('utf-8'))\
        .hexdigest()


def get_user(mobile):
    return Session.query(Users).filter(Users.mobile == mobile).first()


def send_checkcode(mobile, code):
    post_sms(mobile, "您的验证码为：%s，此验证码十分钟后过期。" % code)


def _is_mobile_registed(mobile, is_new_registration):
    user = get_user(mobile)
    if user:
        if is_new_registration: raise UserError("MOBILE_USED" ,"该手机号已注册，直接登录吧")
    else:
        if not is_new_registration: raise UserError('USER_NOT_EXISTED', '该用户不存在')

    return user


def save_user_by_mobile_number(mobile, code, password, user=None):
    sc = get_rcode(mobile)
    if sc is None:
        raise UserError("MOBILE_VCODE_NOT_EXISTED", "验证码不存在，请确认是否已经发送")

    if sc.decode('utf8') != code:
        raise UserError("MOBILE_VCODE_INCORRECT", "您所输入的验证码不正确")

    if is_simple_password(password):
        raise UserError("MOBILE_PASS_TOO_SIMPLE", "您设置的密码过于简单")

    passwd = make_SSHA_password(password.encode('utf-8')).decode('utf-8')

    if user:
        user.passwd = passwd
    else:
        user_info = {
            "mobile": mobile,
            "name": '%s****%s' % (mobile[:3], mobile[-4:]),
            "gender": "0",
            "passwd": passwd
        }

        user = Users(**user_info)
        Session.add(user)
        Session.flush()

    Session.commit()
    delete_rcode(mobile)

    session['userid'] = user.id
    return user.id


def is_simple_password(passwd): pass


@app.route('/save_muser_profile/<mobile>', methods=['POST'])
@requires_auth
def step3(mobile):
    if mobile is None:
        raise UserError("MOBILE_INCORRECT", "手机号码输入不正确")

    nickname = request.json.get("userName", None)
    desc = request.json.get("desc", None)

    return set_profile(nickname, desc)


def set_profile(nickname, desc):
    if nickname is None :
        raise UserError("INVALID_PARAM", "昵称必须填写")

    user = request.user
    user.name = nickname
    user.description = desc

    Session.add(user)
    Session.flush()
    return jsonify(success=True)


@app.route('/user/bymobile/<mobile>', methods=['DELETE'])
@private_api
def remove_mobile_user(mobile):
    if mobile is None: return

    Session.query(Users)\
        .filter(Users.mobile == mobile)\
        .delete(synchronize_session=False)
    Session.commit()

    return jsonify(success=True)

