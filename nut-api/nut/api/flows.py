#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'
import logging, random
from flask import request, jsonify

from nut.api import application as app
from nut.api import Session
from nut.core import errors
from nut.db.models import (Hospitals, Departments, Doctors, TreatmentTime, TreatmentProgress, TreatmentStatus, Patients,
                           TreatmentRegistration, TreatmentFee)
from nut.utils.decorator import requires_auth
from nut.utils.customerized_data_type import enum

from .treatments import get_treatments

logger = logging.getLogger(__name__)

Default_Flow_Status = dict(status_code='ptr', child_status_code='unreg')

Flow_Status = enum(
    pre_treatment=('ptr','预约'),
    treatment=('tr','就诊'),
    check=('chk','检查'),
    dispense=('disp','取药')
)


@app.route('/flows', methods=['GET'])
@requires_auth
def do_get_flows_current_status():
    registration_id = request.args.get('registration_id', None)

    if not registration_id:
        doctor_id = request.args.get('doctor_id', None)
        date = request.args.get('date', None)
        type = request.args.get('type', None)

        if not (doctor_id and date and type is not None):
            raise errors.BadParameterError('BadParameterError', '参数错误！')

        params = dict(registration_id=None, doctor_id=doctor_id, date=date, type=type)
    else:
        tr = Session.query(TreatmentRegistration).filter(TreatmentRegistration.registration_id == str(registration_id)).first()
        doctor_id = tr.doctor_id
        date = tr.date
        time = tr.time

        params = dict(registration_id=registration_id, doctor_id=doctor_id, date=date, type=time)

    f, parameters = get_flow_current_status(**params)
    resp = handle(f, parameters)
    return resp


def get_flow_current_status(registration_id, doctor_id, date, type):
    if not registration_id:
        status_code = Default_Flow_Status['status_code']
        child_status_code = Default_Flow_Status['child_status_code']

        ts_obj = Session.query(TreatmentStatus).filter(TreatmentStatus.code == status_code).first()
        return ts_obj.code_name, dict(registration_id=registration_id,
                                      doctor_id=doctor_id, date=date, type=type, code=status_code, child_code=child_status_code)
    else:
        tp_obj = Session.query(TreatmentProgress).filter(TreatmentProgress.registration_id == registration_id)\
            .order_by(TreatmentProgress.datetime.desc()).first()
        ts_obj = Session.query(TreatmentStatus)\
            .filter(TreatmentStatus.code == tp_obj.status_code).first()
        return ts_obj.code_name, dict(registration_id=registration_id,
                                      doctor_id=doctor_id, date=date, type=type, code=tp_obj.status_code, child_code=tp_obj.child_status_code)


def handle(f, parameters):
    if f == 'pre_treatment':
        resp = FlowHandler.pre_treatment(**parameters)
    elif f == 'treatment':
        resp = FlowHandler.treatment(**parameters)
    elif f == 'check':
        resp = FlowHandler.check(**parameters)
    else:
        resp = FlowHandler.dispense(**parameters)

    return resp


def get_patient(registration_id):
    tp_obj = Session.query(TreatmentProgress).filter(TreatmentProgress.registration_id == registration_id).first()
    patient_id = tp_obj.patient_id
    p_obj = Session.query(Patients).filter(Patients.id == patient_id).first()

    return p_obj


def get_doctor_dict(doctor_id):
    doctor_obj = Session.query(Doctors).filter(Doctors.id == doctor_id).first()

    return doctor_obj.dict()


class FlowHandler:

    @staticmethod
    def pre_treatment(registration_id, doctor_id, date, type, code, child_code):

        status = get_flow_status(code, child_code)

        if child_code == 'unreg':
            treatment = get_treatments(doctor_id, date, type)

            return jsonify(success=True, treatment=treatment, status=status)
        else:
            p_obj = get_patient(registration_id)

            treatment = get_treatments(doctor_id, date, type)

            doctor = get_doctor_dict(doctor_id)

            if child_code == 'unpaid':
                tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                            TreatmentFee.status == 'ptr').first()
                _fee_dict = fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance)

                return jsonify(success=True, treatment=treatment, doctor=doctor, status=status,
                               patient=p_obj.dict(), fee=_fee_dict)

            return jsonify(success=True, treatment=treatment, doctor=doctor, status=status, patient=p_obj.dict())

    @staticmethod
    def treatment(registration_id, doctor_id, date, type, code, child_code):
        import random

        status = get_flow_status(code, child_code)

        p_obj = get_patient(registration_id)

        doctor_obj = Session.query(Doctors).filter(Doctors.id == doctor_id).first()
        doctor = doctor_obj.dict()

        tr_obj = Session.query(TreatmentRegistration)\
            .filter(TreatmentRegistration.registration_id == str(registration_id)).first()

        pos = tr_obj.fake_lineup_num
        current_pos = pos - random.randint(3, 8)

        return jsonify(success=True, line_up=dict(current_pos=current_pos, pos=pos), doctor=doctor,
                       status=status, patient=p_obj.dict())

    @staticmethod
    def check(registration_id, doctor_id, date, type, code, child_code):
        from nut.db.models import TreatmentFee
        from .fake_doctors import generate_payment

        status = get_flow_status(code, child_code)

        treatment = get_treatments(doctor_id, date, type)

        if child_code == 'unpaid':
            tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                        TreatmentFee.status == 'chk').first()
            is_mi_pay = False
            if tf_obj.medical_insurance is None and tf_obj.self_finance is None:
                is_mi_pay = True

            if is_mi_pay:
                fee = fee_dict(tf_obj.fee)
            else:
                fee = fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance)

            return jsonify(success=True, fee=fee, doctor=get_doctor_dict(doctor_id),
                           status=status, patient=get_patient(registration_id).dict(), treatment=treatment)

        elif child_code == 'unchk':
            tr_obj = Session.query(TreatmentRegistration)\
                .filter(TreatmentRegistration.registration_id == str(registration_id)).first()

            pos = tr_obj.fake_lineup_num
            current_pos = pos - random.randint(3, 8)

            tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                        TreatmentFee.status == 'chk').first()
            fee = fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance)

            return jsonify(success=True, doctor=get_doctor_dict(doctor_id), fee=fee,
                           status=status, patient=get_patient(registration_id).dict(),
                           line_up=dict(current_pos=current_pos, pos=pos), treatment=treatment)
        else:  # 'done'
            from .fake_doctors import get_fake_chk_report

            return jsonify(success=True, doctor=get_doctor_dict(doctor_id),
                           status=status, patient=get_patient(registration_id).dict(),
                           report=get_fake_chk_report(), treatment=treatment)

    @staticmethod
    def dispense(registration_id, doctor_id, date, type, code, child_code):
        from nut.db.models import TreatmentFee
        from .fake_doctors import generate_payment, get_fake_med_report

        status = get_flow_status(code, child_code)

        treatment = get_treatments(doctor_id, date, type)

        if child_code == 'unpaid':
            tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                        TreatmentFee.status == 'disp').first()
            is_mi_pay = False
            if tf_obj.medical_insurance is None and tf_obj.self_finance is None:
                is_mi_pay = True

            if is_mi_pay:
                fee = fee_dict(tf_obj.fee)
            else:
                fee = fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance)

            return jsonify(success=True, fee=fee, doctor=get_doctor_dict(doctor_id), treatment=treatment,
                           status=status, patient=get_patient(registration_id).dict(), report=get_fake_med_report())

        elif child_code == 'undisp':
            tr_obj = Session.query(TreatmentRegistration)\
                .filter(TreatmentRegistration.registration_id == str(registration_id)).first()

            pos = tr_obj.fake_lineup_num
            current_pos = pos - random.randint(3, 8)

            tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                        TreatmentFee.status == 'disp').first()
            fee = fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance)

            return jsonify(success=True, doctor=get_doctor_dict(doctor_id), treatment=treatment,
                           status=status, patient=get_patient(registration_id).dict(), fee=fee,
                           line_up=dict(current_pos=current_pos, pos=pos), report=get_fake_med_report())
        else:  # 'done'
            return jsonify(success=True, doctor=get_doctor_dict(doctor_id), treatment=treatment,
                           status=status, patient=get_patient(registration_id).dict(),
                           report=get_fake_med_report())


@app.route('/flows/registrations', methods=['POST'])
@requires_auth
def do_post_flows_registration():
    from .fake_doctors import generate_registration_id, generate_linup_pos
    from nut.db.models import TreatmentRegistration

    patient_id = request.json.get('patient_id', None)
    doctor_id = request.json.get('doctor_id', None)
    date = request.json.get('date', None)
    type = request.json.get('type', None)

    if not (patient_id and doctor_id and date and type is not None):
            raise errors.BadParameterError('BadParameterError', '参数错误！')

    tr_obj = Session.query(TreatmentRegistration).filter(TreatmentRegistration.patient_id == patient_id,
                                                         TreatmentRegistration.doctor_id == doctor_id,
                                                         TreatmentRegistration.date == date,
                                                         TreatmentRegistration.time == type).first()
    if tr_obj:
        raise errors.AlreadyExistingError('AlreadyExistingError_treatment_registration', '已挂过号！')

    lineup_pos = generate_linup_pos()
    registration_id = '%s%s' % (generate_registration_id(), lineup_pos)

    tr = TreatmentRegistration(
        registration_id=registration_id,
        user_id=request.user.id,
        patient_id=patient_id,
        doctor_id=doctor_id,
        date=date,
        time=type,
        fake_lineup_num=int(lineup_pos)
    )
    Session.add(tr)
    Session.flush()

    # ToDo @likui@nuttek.com use current status to get next status in flow status chain
    tp = TreatmentProgress(
        registration_id=registration_id,
        user_id=request.user.id,
        doctor_id=doctor_id,
        patient_id=patient_id,
        status_code='ptr',
        child_status_code='unpaid'
    )
    Session.add(tp)

    tt_obj = Session.query(TreatmentTime).filter(TreatmentTime.doctor_id == doctor_id,
                                                 TreatmentTime.date == date, TreatmentTime.time == type).first()
    if tt_obj and tt_obj.price:
        fee = tt_obj.price
    elif tt_obj and not tt_obj.price:
        raise errors.DBAccessError('DBAccessError_Wrong_DB_DATA', '数据库数据错误！')
    else:
        raise errors.NotFoundError('NotFoundError_TreatmentTime', '出诊记录未找到！')

    medical_insurance = random.randint(1, fee - 3)
    write_down_fee(registration_id, 'ptr', fee, medical_insurance=medical_insurance,
                   self_finance=(fee - medical_insurance))

    remaining_amount = tt_obj.remaining_amount
    remaining_amount -= 1
    if remaining_amount > 0:
        tt_obj.remaining_amount = remaining_amount
    else:
        tt_obj.remaining_amount = 0

    Session.commit()

    return jsonify(success=True, registration_id=int(registration_id), status=get_flow_status('ptr', 'unpaid'),
                   fee=fee_dict(fee, medical_insurance, (fee - medical_insurance)))


@app.route('/flows/registrations', methods=['GET'])
@requires_auth
def do_get_flows_registration():
    user_id = request.user.id
    status = request.args.get('status', None)
    if status is None or len(status) < 1:
        raise errors.BadParameterError('BadParameterError_Status_Required', 'status字段不能为空！')

    patient_objs = Session.query(Patients).filter(Patients.user_id == user_id).all()
    patient_id_list = [po.id for po in patient_objs]

    tr_objs = Session.query(TreatmentRegistration).filter(TreatmentRegistration.user_id == user_id,
                                                          TreatmentRegistration.patient_id.in_(patient_id_list))\
        .order_by(TreatmentRegistration.date.asc(), TreatmentRegistration.time.asc()).all()

    rp_tuple_list = [(tr_obj.registration_id, tr_obj.patient_id, tr_obj) for tr_obj in tr_objs]

    registration_list = []
    for rp_tuple in rp_tuple_list:
        registration_id = rp_tuple[0]
        patient_id = rp_tuple[1]
        tr_obj = rp_tuple[2]
        tp_obj = get_latest_progress(registration_id)

        if should_be_filtered(status, tp_obj, tr_obj):
            continue

        tr_obj = Session.query(TreatmentRegistration)\
            .filter(TreatmentRegistration.registration_id == registration_id).first()

        treatment = get_treatments(tr_obj.doctor_id, tr_obj.date, tr_obj.time)

        registration_list.append(dict(registration_id=int(registration_id),
                                      doctor=get_doctor_dict(tp_obj.doctor_id),
                                      patient=get_patient(registration_id).dict(),
                                      treatment=treatment,
                                      status=get_flow_status(tp_obj.status_code, tp_obj.child_status_code)))

    return jsonify(success=True, registrations=registration_list)


@app.route('/flows/registrations/<int:registration_id>/detail', methods=['GET'])
@requires_auth
def do_get_flows_registration_detail(registration_id):
    tp_objs = Session.query(TreatmentProgress).filter(TreatmentProgress.registration_id == registration_id)\
        .order_by(TreatmentProgress.datetime.desc()).all()

    flow_list = []
    for tp_obj in tp_objs:
        status = get_flow_status(tp_obj.status_code, tp_obj.child_status_code)

        if tp_obj.child_status_code == 'unpaid':
            _tp_obj = Session.query(TreatmentProgress).filter(TreatmentProgress.status_code == tp_obj.status_code)\
                .order_by(TreatmentProgress.datetime.desc()).first()
            if _tp_obj and _tp_obj.child_status_code != 'unpaid':
                status = dict(code=_tp_obj.status_code, description=status.get('description'),
                              child_status=dict(code='paid', description='已支付'))

        flow = dict(datetime=tp_obj.datetime, status=status)

        if (tp_obj.child_status_code == 'untr' or tp_obj.child_status_code == 'unchk' or
                    tp_obj.child_status_code == 'undisp'):
            pos = int(str(registration_id)[-2:])
            line_up = dict(current_pos=pos - random.randint(1, 5), pos=pos)

            flow.update(line_up=line_up)

        if (tp_obj.child_status_code == 'unpaid' or tp_obj.child_status_code == 'unchk' or
                    tp_obj.child_status_code == 'undisp'):
            tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == registration_id,
                                                        TreatmentFee.status == tp_obj.status_code).first()
            flow.update(fee=fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance))

        if tp_obj.status_code == 'disp':
            from .fake_doctors import get_fake_med_report
            flow.update(report=get_fake_med_report())

        if tp_obj.status_code == 'chk' and (tp_obj.child_status_code == 'done'):
            from .fake_doctors import get_fake_chk_report
            flow.update(report=get_fake_chk_report())

        flow_list.append(flow)

    tr_obj = Session.query(TreatmentRegistration)\
        .filter(TreatmentRegistration.registration_id == str(registration_id)).first()

    return jsonify(success=True, registration_id=registration_id,
                   doctor=get_doctor_dict(tp_objs[0].doctor_id),
                   patient=get_patient(registration_id).dict(),
                   treatment=get_treatments(tp_objs[0].doctor_id, tr_obj.date, tr_obj.time),
                   flows=flow_list)


def should_be_filtered(status, obj, tr_obj):
    if status == 'ongoing':
        return obj.status_code == 'ptr' or (obj.status_code == 'disp' and obj.child_status_code == 'done')
    elif status == 'ready':
        return not (obj.status_code == 'ptr' and
                    (obj.child_status_code == 'unpaid' or obj.child_status_code == 'done'))
    elif status == 'history':
        from datetime import date
        _today = date.today()
        _today_str = '%s-%02d-%02d' % (_today.year, _today.month, _today.day)

        return not ((tr_obj.date < _today_str) or (obj.status_code == 'disp' and obj.child_status_code == 'done'))

    return False


@app.route('/flows/fee', methods=['POST'])
@requires_auth
def do_post_flows_fee():
    registration_id = request.json.get('registration_id', None)
    payment = request.json.get('payment', None)

    is_for_medical_insurance = payment is None

    if not registration_id:
            raise errors.BadParameterError('BadParameterError', '参数错误！')

    tg_obj = get_latest_progress(registration_id)

    if not (tg_obj.status_code in ['chk', 'disp']):
        if not payment or payment <= 0:
            raise errors.BadParameterError('BadParameterError', '参数错误！')

    tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(registration_id),
                                                TreatmentFee.status == tg_obj.status_code).first()

    if not tf_obj:
        raise errors.PaymentError('PaymentError_Not_Found', '没找到应支付记录！')

    if not is_for_medical_insurance:
        if tf_obj.self_finance != float(payment):
            raise errors.PaymentError('PaymentError_Invalid_Fee', '支付金额不正确！')

        if tg_obj.child_status_code == 'done':
            raise errors.PaymentError('PaymentError_Already_Paid', '已支付！')

        if tg_obj.status_code == 'ptr':
            _child_status_code = 'done'
        elif tg_obj.status_code == 'chk':
            _child_status_code = 'unchk'
        elif tg_obj.status_code == 'disp':
            _child_status_code = 'undisp'

        _tp_obj = TreatmentProgress(
            registration_id=tg_obj.registration_id,
            user_id=tg_obj.user_id,
            doctor_id=tg_obj.doctor_id,
            patient_id=tg_obj.patient_id,
            status_code=tg_obj.status_code,
            child_status_code=_child_status_code
        )
        Session.add(_tp_obj)
        Session.commit()
    else:
        from .fake_doctors import generate_payment

        if not (tf_obj.medical_insurance and tf_obj.self_finance):
            medical_insurance, self_finance = generate_payment(tf_obj.fee)

            tf_obj.medical_insurance = medical_insurance
            tf_obj.self_finance = self_finance

            Session.commit()

    tr_obj = Session.query(TreatmentRegistration).filter(TreatmentRegistration.registration_id == str(registration_id)).first()
    p_obj = get_patient(registration_id)

    treatment = get_treatments(tr_obj.doctor_id, tr_obj.date, tr_obj.time)

    doctor = get_doctor_dict(tr_obj.doctor_id)

    _fee_dict = fee_dict(tf_obj.fee, tf_obj.medical_insurance, tf_obj.self_finance)
    if not is_for_medical_insurance:
        status = get_flow_status(_tp_obj.status_code, _tp_obj.child_status_code)
    else:
        status = get_flow_status(tg_obj.status_code, tg_obj.child_status_code)

    return jsonify(success=True, treatment=treatment, doctor=doctor, status=status,
                   patient=p_obj.dict(), fee=_fee_dict)


def get_flow_status(code, child_code):
    code_obj = Session.query(TreatmentStatus).filter(TreatmentStatus.code == code).first()
    child_code_obj = Session.query(TreatmentStatus)\
        .filter(TreatmentStatus.parent_code == code, TreatmentStatus.code == child_code).first()

    return dict(code=code, description=code_obj.description,
                child_status=dict(code=child_code, description=child_code_obj.description))


def write_down_fee(r_id, status, fee, self_finance=None, medical_insurance=None):
    tf_obj = Session.query(TreatmentFee).filter(TreatmentFee.registration_id == str(r_id),
                                                TreatmentFee.status == status).first()
    if tf_obj:
        return

    tf = TreatmentFee(
        registration_id=r_id,
        status=status,
        fee=fee,
        medical_insurance=medical_insurance,
        self_finance=self_finance
    )

    Session.add(tf)
    Session.flush()


def fee_dict(fee, medical_insurance=None, self_finance=None):
    return dict(total=fee, medical_insurance=medical_insurance, self_finance=self_finance)


def get_latest_progress(registration_id):
    tg_obj = Session.query(TreatmentProgress).filter(TreatmentProgress.registration_id == registration_id)\
        .order_by(TreatmentProgress.datetime.desc()).first()
    return tg_obj




