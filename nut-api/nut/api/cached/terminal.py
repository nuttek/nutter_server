__author__ = 'brucelee'

from nut.api import Session
from nut.api import cache
from nut.db.models import Terminals


@cache.multiple_get_memoize()
def get_cached_terminal(user_id, terminal_id):
    if not terminal_id: return None

    if user_id:
        terminal = Session.query(Terminals).\
            filter(Terminals.user_id == user_id, Terminals.terminal_id == terminal_id).first()
    else:
        terminal = Session.query(Terminals).\
            filter(Terminals.terminal_id == terminal_id).first()
    return terminal if terminal else None