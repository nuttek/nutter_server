__author__ = 'brucelee'

from .. import Session
from .. import cache
from nut.utils.misc import get_cdn_image_path
from nut.db.models import Users


@cache.multiple_get_memoize()
def get_cached_user(user_id):
    user = Session.query(Users).get(user_id)
    if user is None: return None

    if user.profile_img_url and not user.profile_img_url.startswith('http'):
        user.heading_img_url = get_cdn_image_path(user.profile_img_url)
    else:
        user.heading_img_url = user.profile_img_url

    return user
