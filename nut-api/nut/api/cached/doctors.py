#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'


from sqlalchemy import not_

from nut.db.models import Doctors
from .. import Session

from .. import cache


# all doctors
@cache.memoize()
def get_cached_all_doctors():
    return Session.query(Doctors)\
        .filter(not_(Doctors.is_soft_deleted)).order_by(Doctors.order).all()


# one doctors
@cache.memoize()
def get_cached_doctor(doctor_id):
    return Session.query(Doctors)\
        .filter(not_(Doctors.is_soft_deleted), Doctors.id==doctor_id).first()