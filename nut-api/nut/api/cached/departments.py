#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'


from sqlalchemy import not_

from nut.db.models import Departments
from .. import Session

from .. import cache


# all departments
@cache.memoize()
def get_cached_all_departments():
    return Session.query(Departments)\
        .filter(not_(Departments.is_soft_deleted)).order_by(Departments.order).all()


# all parent departments
@cache.memoize()
def get_cached_all_parent_departments():
    _all_department = get_cached_all_departments()

    return list(filter(lambda d: d.parent_id is None, _all_department))


# child departments of certain a parent department
@cache.memoize()
def get_cached_child_departments_per_parent_id(parent_id):
    _all_department = get_cached_all_departments()

    return list(filter(lambda d: d.parent_id == parent_id, _all_department))
