#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from sqlalchemy import func, not_

from nut.db.models import Provinces, Cities, Counties, Levels
from .. import Session
from .. import cache


# all provinces
@cache.memoize()
def get_cached_all_provinces():
    return Session.query(Provinces).order_by(Provinces.order).all()


# single province
@cache.memoize()
def get_cached_province(obj_id):
    provinces = get_cached_all_provinces()
    provinces = list(filter(lambda p: p.id == obj_id, provinces))

    return provinces[0]


# all cities
@cache.memoize()
def get_cached_all_cities():
    return Session.query(Cities).order_by(Cities.order).all()


# cities of one province
@cache.memoize()
def get_cached_cities(province_id):
    cities = get_cached_all_cities()

    return list(filter(lambda c: c.province_id == int(province_id), cities))


# single city
@cache.memoize()
def get_cached_city(obj_id):
    cities = get_cached_all_cities()
    cities = list(filter(lambda c: c.id == obj_id, cities))

    return cities[0]


# all counties
@cache.memoize()
def get_cached_all_counties():
    return Session.query(Counties).order_by(Counties.order).all()


# counties of one city
@cache.memoize()
def get_cached_counties(city_id):
    counties = get_cached_all_counties()

    return list(filter(lambda c: c.city_id == int(city_id), counties))


# single county
@cache.memoize()
def get_cached_county(obj_id):
    counties = get_cached_all_counties()
    counties = list(filter(lambda c: c.id == int(obj_id), counties))

    return counties[0]


# all levels
@cache.memoize()
def get_cached_all_levels():
    return Session.query(Levels).order_by(Levels.order, not_(Levels.is_soft_deleted)).all()


# single level
@cache.memoize()
def get_cached_level(obj_id):
    levels = get_cached_all_levels()
    levels = list(filter(lambda c: c.id == int(obj_id), levels))

    return levels[0]


@cache.memoize()
def get_cached_provinces_last_modified_date():
    return Session.query(func.max(Provinces.modified)).scalar()


@cache.memoize()
def get_cached_cities_last_modified_date(province_id):
    return Session.query(func.max(Cities.modified)).filter(Cities.province_id == province_id).scalar()


@cache.memoize()
def get_cached_counties_last_modified_date(city_id):
    return Session.query(func.max(Counties.modified)).filter(Counties.city_id == city_id).scalar()




