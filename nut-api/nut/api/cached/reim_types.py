#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'


from sqlalchemy import not_

from .. import Session
from .. import cache
from nut.db.models import ReimTypes


@cache.memoize()
def get_cached_reim_types(id):
    rt_obj = Session.query(ReimTypes).filter(ReimTypes.id == id).first()

    return rt_obj.dict()





