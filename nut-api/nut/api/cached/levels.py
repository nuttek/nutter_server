#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'


from sqlalchemy import not_

from nut.core.constants import LEVELS
from nut.db.models import Levels
from .. import Session
from .. import cache


# all levels
@cache.memoize()
def get_cached_all_levels():
    return Session.query(Levels).filter(not_(Levels.is_soft_deleted)).all()


# hospital levels
@cache.memoize()
def get_cached_hospital_levels():
    levels = get_cached_all_levels()
    return list(filter(lambda l: l.type == LEVELS.HOSPITAL, levels))


# doctor levels
@cache.memoize()
def get_cached_doctor_levels():
    levels = get_cached_all_levels()
    return list(filter(lambda l: l.type == LEVELS.DOCTOR, levels))



