#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'


from sqlalchemy import not_

from .. import Session
from .. import cache

MAX_WEEKDAYS_REQUIRED = 11

treatment_periods = [0, 1]


# treatment dates
def get_cached_treatment_dates():
    import datetime
    from datetime import date

    today = date.today()

    weekdays = []

    loop = MAX_WEEKDAYS_REQUIRED
    t = 0
    while loop > 0:
        _date = today + datetime.timedelta(days=t)
        t += 1
        weekdays.append('%s-%02d-%02d' % (_date.year, _date.month, _date.day))
        loop -= 1

    return weekdays





