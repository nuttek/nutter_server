#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'


from sqlalchemy import func, not_

from nut.core.constants import LEVELS
from nut.db.models import Hospitals, Levels
from .. import Session
from .. import cache


# all hospitals
@cache.memoize()
def get_cached_all_hospitals():
    return Session.query(Hospitals)\
        .filter(not_(Hospitals.is_soft_deleted)).order_by(Hospitals.order).all()


@cache.memoize()
def get_cached_hospitals_by_city_last_modified_date(city_id):
    return Session.query(func.max(Hospitals.modified))\
        .filter(Hospitals.city_id == city_id, not_(Hospitals.is_soft_deleted)).scalar()


@cache.memoize()
def get_cached_hospitals_by_county_last_modified_date(county_id):
    return Session.query(func.max(Hospitals.modified))\
        .filter(Hospitals.county_id == county_id, not_(Hospitals.is_soft_deleted)).scalar()


@cache.memoize()
def get_cached_hospitals_by_level_last_modified_date(level):
    return Session.query(func.max(Hospitals.modified))\
        .filter(Hospitals.level == level, not_(Hospitals.is_soft_deleted)).scalar()

