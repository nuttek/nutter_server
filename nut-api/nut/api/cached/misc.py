from flask import request
from nut.api import cache
from nut.api.cached.province_city import (get_cached_provinces_last_modified_date,get_cached_cities_last_modified_date,
                                          get_cached_counties_last_modified_date)
from nut.api.cached.hospitals import (get_cached_hospitals_by_city_last_modified_date,
                                      get_cached_hospitals_by_county_last_modified_date,
                                      get_cached_hospitals_by_level_last_modified_date)
from nut.core.constants import OBJ_TYPE, HTTP_DATE_FORMAT
from nut.utils.customerized_data_type import Switch
from nut.utils.misc import format_datetime

__author__ = 'brucelee'


@cache.memoize()
def get_last_modified_date_by_obj(obj, **kwargs):
    for case in Switch(obj):
        if case(OBJ_TYPE.PROVINCE):
            dt = get_cached_provinces_last_modified_date()
        if case(OBJ_TYPE.CITY):
            province_id = kwargs.get("province_id")
            dt = get_cached_cities_last_modified_date(province_id)
        if case(OBJ_TYPE.COUNTY):
            city_id = kwargs.get("city_id")
            dt = get_cached_counties_last_modified_date(city_id)
        if case(OBJ_TYPE.HOSPITAL):
            if request:
                city_id = request.args.get("city_id", None)
                county_id = request.args.get("county_id", None)
                level = request.args.get("level", None)
                if city_id:
                    dt = get_cached_hospitals_by_city_last_modified_date(city_id)
                if county_id:
                    dt = get_cached_hospitals_by_county_last_modified_date(county_id)
                if level:
                    dt = get_cached_hospitals_by_level_last_modified_date(level)

    return format_datetime(dt, HTTP_DATE_FORMAT, True)
