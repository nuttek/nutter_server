#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from flask import request, jsonify, abort

from nut.api import application as app
from nut.api.cached.hospitals import (get_cached_hospitals_by_city_last_modified_date,
                                      get_cached_all_hospitals,
                                      get_cached_hospitals_by_county_last_modified_date,
                                      get_cached_hospitals_by_level_last_modified_date)
from nut.core.constants import OBJ_TYPE as LMD, HTTP_HEADERS, HTTP_DATE_FORMAT
from nut.utils.decorator import conditional_last_modified_date, requires_pagination
from nut.utils.misc import format_datetime, get_pagination_of_objs

DEFAULT_CITY_ID = 2 # tianjin


@app.route('/hospitals', methods=['GET'])
@conditional_last_modified_date(obj=LMD.HOSPITAL)
@requires_pagination
def do_get_hospitals():
    city_id = request.args.get("city_id", None)
    county_id = request.args.get("county_id", None)
    level = request.args.get("level", None)

    is_intelligent = request.args.get("is_intelligent", None)
    if is_intelligent or is_intelligent == 0:
        city_id = DEFAULT_CITY_ID

    if not (city_id or county_id or level):
        abort(400)

    if city_id and county_id:
        abort(400)

    cached_hospitals = get_cached_hospitals_by_conditions(city_id, county_id, level)

    hospital_list, _next_id = get_pagination_of_objs(cached_hospitals, request.next_id, request.length)

    hospital_list = [h.dict() for h in hospital_list]

    resp = jsonify(success=True, hospitals=hospital_list, next_id=_next_id)

    # send last-modified back to client side
    if city_id:
        dt = get_cached_hospitals_by_city_last_modified_date(city_id)
    if county_id:
        dt = get_cached_hospitals_by_county_last_modified_date(county_id)
    if level:
        dt = get_cached_hospitals_by_level_last_modified_date(level)
    resp.headers[HTTP_HEADERS.LAST_MODIFIED] = format_datetime(dt, HTTP_DATE_FORMAT, True)

    return resp


# hospitals filtered by combined conditions
def get_cached_hospitals_by_conditions(city_id, county_id, level):
    hospitals = get_cached_all_hospitals()
    if city_id:
        if level:
            f = lambda h: h.city_id == int(city_id) and h.level == int(level)
        else:
            f = lambda h: h.city_id == int(city_id)
    elif county_id:
        if level:
            f = lambda h: h.county_id == int(county_id) and h.level == int(level)
        else:
            f = lambda h: h.county_id == int(county_id)
    else: # neither city_id nor county_id, only level here
        f = lambda h: h.level == int(level)

    hospitals = list(filter(f, hospitals))

    return hospitals


# get specific hospital
def get_hospital_by_id(doctor_id):
    _hospitals = get_cached_all_hospitals()
    _doctor = list(filter(lambda h: h.id == doctor_id, _hospitals))[0]

    return _doctor



