# -*- coding: utf-8 -*-

__author__ = 'brucelee'

import hashlib

from flask import jsonify, request, session

from nut.api import Session
from nut.api import application as app
from nut.core.errors import UserError, BadParameterError
from nut.db.models import Patients
from nut.utils.decorator import requires_auth

from .rds import get_rcode, delete_rcode


def register_mobile_or_reset_password(mobile, password, vcode, is_new_registration=True):
    user = _is_mobile_registed(mobile, is_new_registration)

    return save_user_by_mobile_number(mobile, vcode, password, user)


@app.route('/patients', methods=['POST'])
@requires_auth
def do_post_patient():
    mobile = request.json.get("mobile", None)
    code = request.json.get("vcode", None)
    name = request.json.get("name", None)
    id_card = request.json.get("id_card", None)
    mc_card = request.json.get("mc_card", None)
    reim_type = request.json.get("reim_type", None)

    if not (name and id_card and reim_type is not None):
        raise UserError("INFO_NOT_ENOUGH", "必填信息未填写！")

    user_id = save_patient_by_mobile_number(request.user.id, mobile, code, name, id_card, mc_card, reim_type)

    return jsonify(success=True, patient_info=dict(id=user_id))


@app.route('/patients', methods=['GET'])
@requires_auth
def do_get_patients():
    user_id = request.user.id

    if not user_id:
        raise BadParameterError('BadParameterError_treatment', '参数错误！')

    ps = Session.query(Patients).filter(Patients.user_id == int(user_id)).all()

    if not ps or not len(ps) > 0:
        return jsonify(success=True, patients=[])

    return jsonify(success=True, patients=[p.dict() for p in ps])


@app.route('/patients/<int:id>', methods=['GET'])
@requires_auth
def do_get_patient_by_id(id):
    user_id = request.user.id

    if not user_id or not id:
        raise BadParameterError('BadParameterError_treatment', '参数错误！')

    p = Session.query(Patients).filter(Patients.user_id == int(user_id), Patients.id == id).first()

    if not p:
        return jsonify(success=True, patient=None)

    return jsonify(success=True, patient=p.dict())


@app.route('/patients/<int:id>', methods=['DELETE'])
@requires_auth
def do_delete_patient_by_id(id):
    user_id = request.user.id

    if not user_id or not id:
        raise BadParameterError('BadParameterError_treatment', '参数错误！')

    row_deleted_count = Session.query(Patients).filter(Patients.user_id == int(user_id), Patients.id == id).delete()
    if row_deleted_count <= 0:
        raise UserError('UserError_patient_not_found', '就诊人不存在！')
    Session.commit()
    return jsonify(success=True)


def save_patient_by_mobile_number(user_id, mobile, code, name, id_card, mc_card, reim_type):
    sc = get_rcode(mobile)
    if sc is None:
        raise UserError("MOBILE_VCODE_NOT_EXISTED", "验证码不存在，请确认是否已经发送")

    if sc.decode('utf8') != code:
        raise UserError("MOBILE_VCODE_INCORRECT", "您所输入的验证码不正确")

    p = Patients(mobile=mobile,
                 name=name,
                 user_id=user_id,
                 id_card=id_card,
                 mc_card=mc_card,
                 reim_type=reim_type
                 )
    Session.add(p)
    Session.flush()
    Session.commit()
    delete_rcode(mobile)

    return p.id

