__author__ = 'brucelee'

from nut.misc import enum

Info = enum(

)

Warning = enum(

)

Error = enum(
    User_UserNameRequired = '用户名不能为空！',
    User_PasswordRequired = '密码不能为空！',
    User_UserExisted = '用户名已存在！',
    User_PermissionRequired = '权限不能为空！',
    User_NothingChanged = '您没有做任何修改!',
    Block_Blocking = '把她移出黑名单才可互动',
    Block_Blocked = '暂时不能操作，对方已设置'
)
