#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from flask import request, jsonify, abort

from nut.api import application as app
from nut.api.cached.departments import (get_cached_all_departments, get_cached_all_parent_departments,
                                        get_cached_child_departments_per_parent_id)
from nut.core.constants import OBJ_TYPE, HTTP_HEADERS, HTTP_DATE_FORMAT
from nut.utils.decorator import conditional_last_modified_date, requires_pagination
from nut.utils.misc import get_pagination_of_objs


@app.route('/departments', methods=['GET'])
@conditional_last_modified_date(obj=OBJ_TYPE.DEPARTMENT)
@requires_pagination
def do_get_departments():
    is_parent_only = request.args.get("is_parent_only", None)
    if is_parent_only is not None:
        if is_parent_only:
            _departments = get_parent_only_departments(True)
    else:
        _departments = get_parent_only_departments()

    # TODO (likui@nuttek.com) add last modified date implementation

    _departments, _next_id = get_pagination_of_objs(_departments, request.next_id, request.length)

    return jsonify(success=True, departments=_departments, next_id=_next_id)


def get_dept_dist_url(hosp_id):
    from nut.config import config

    return config.FAKE_HOSP_DIST_URL


@app.route('/hospitals/<int:hosp_id>/departments', methods=['GET'])
@conditional_last_modified_date(obj=OBJ_TYPE.DEPARTMENT)
@requires_pagination
def do_get_departments_per_hosp_id(hosp_id):

    query_type = request.args.get('q_type', None)
    if query_type and query_type == 'dist':
        return jsonify(success=True, dist_url=get_dept_dist_url(hosp_id))

    # TODO (likui@nuttek.com) add a mapping table of hospital and departments, pick up the data from table doctors

    _departments = get_all_departments()

    # TODO (likui@nuttek.com) add last modified date implementation

    _departments, _next_id = get_pagination_of_objs(_departments, request.next_id, request.length, get_id_func)

    return jsonify(success=True, departments=_departments, next_id=_next_id)


def get_id_func(obj, id_str=None):
    if id_str:
        return obj.get(id_str)
    return obj.get("id")


def get_parent_only_departments():
    return get_all_departments(True)


def get_all_departments(is_parent_only=False):
    _all_parent_departments = get_cached_all_parent_departments()

    department_list = []
    for d in _all_parent_departments:
        _d_dict = d.dict()
        if not is_parent_only:
            _child_department = get_cached_child_departments_per_parent_id(d.id)
            _d_dict.update(child_departments=[_cd.dict() for _cd in _child_department])

        department_list.append(_d_dict)

    return department_list


def get_department_by_id(dept_id):
    _departments = get_cached_all_departments()
    _department = list(filter(lambda d: d.id == dept_id, _departments))[0]

    return _department





