# -*- coding: utf-8 -*-
__author__ = 'brucelee'

from flask import jsonify, request

from nut.api import Session
from nut.api import application as app
from nut.core.errors import BadParameterError, NotFoundError
from nut.db.models import Favorites, Hospitals, Doctors
from nut.utils.decorator import requires_auth


@app.route('/favorites', methods=['POST'])
@requires_auth
def do_post_favorites():
    user_id = request.user.id

    obj_type = request.json.get('type', None)
    obj_id = request.json.get('obj_id', None)

    if not (obj_type is not None and obj_id):
        raise BadParameterError('BadParameterError', '参数错误！')
    else:
        if obj_type not in [0, 1]:
            raise BadParameterError('BadParameterError', '参数错误！')

    f = Session.query(Favorites).filter(Favorites.user_id == user_id, Favorites.obj_type == obj_type,
                                        Favorites.obj_id == obj_id).first()
    if not f:
        f = Favorites(
            user_id=user_id,
            obj_type=obj_type,
            obj_id=obj_id
        )
        Session.add(f)
        Session.commit()

    return jsonify(success=True, favorite=get_single_favorite(obj_type, obj_id))


@app.route('/favorites', methods=['DELETE'])
@requires_auth
def do_delete_favorites():
    user_id = request.user.id

    obj_type = request.args.get('type', None)
    if obj_type:
        obj_type = int(obj_type)
    obj_id = request.args.get('obj_id', None)

    if not (obj_type is not None and obj_id):
        raise BadParameterError('BadParameterError_Parameters_Missing', '参数错误！')
    else:
        if obj_type not in [0, 1]:
            raise BadParameterError('BadParameterError_Invalid_Parameters', '参数错误！')

    f_row_count = Session.query(Favorites).filter(Favorites.user_id == user_id, Favorites.obj_type == obj_type,
                                        Favorites.obj_id == obj_id).delete()
    if f_row_count <= 0:
        raise NotFoundError('NotFoundError_No_Favorite_Found', '请求收藏不存在！')

    Session.commit()

    return jsonify(success=True)


@app.route('/favorites', methods=['GET'])
@requires_auth
def do_get_favorites():
    user_id = request.user.id

    is_entity_required = request.args.get('is_entity_required', False)

    obj_type = request.args.get('type', None)
    if obj_type is not None and int(obj_type) in [0, 1]:
        fs = Session.query(Favorites).filter(Favorites.user_id == user_id, Favorites.obj_type == int(obj_type))\
            .order_by(Favorites.created.desc()).all()
    else:
        fs = Session.query(Favorites).filter(Favorites.user_id == user_id).order_by(Favorites.created.desc()).all()

    f_objs = get_favorites_with_types(user_id, obj_type, is_entity_required)
    if f_objs:
        _f_objs = f_objs.get('obj_ids', None) or f_objs.get('objs', None)
        if not (_f_objs and len(_f_objs) > 0):
            f_objs = None

    return jsonify(success=True, favorites=f_objs)


def get_favorite_obj(type, id):

    if type is not None:
        if type == 0:
            from nut.api.hospitals import get_hospital_by_id

            obj = get_hospital_by_id(id)
            if obj:
                return obj.dict()
        else:
            from nut.api.doctors import get_doctor_by_id

            obj = get_doctor_by_id(id)
            if obj:
                return obj.dict()

    return None


def get_single_favorite(obj_type, obj_id, is_entity=False):
    if is_entity:
        return dict(obj=get_favorite_obj(obj_type, obj_id))

    return dict(obj_id=obj_id)


def get_favorites(fs, is_entity=False):

    if type(is_entity) == str and is_entity in ['True', 'true']:
        is_entity = True
    else:
        is_entity = False

    if is_entity:
        return dict(objs=[get_favorite_obj(f.obj_type, f.obj_id) for f in fs])

    return dict(obj_ids=[f.obj_id for f in fs])


def get_favorites_with_types(user_id, obj_type, is_entity=False):
    fs = Session.query(Favorites).filter(Favorites.user_id == user_id, Favorites.obj_type == obj_type)\
        .order_by(Favorites.created.desc()).all()

    if type(is_entity) == str and is_entity in ['True', 'true']:
        is_entity = True
    else:
        is_entity = False

    if is_entity:
        return dict(type=obj_type, objs=[get_favorite_obj(f.obj_type, f.obj_id) for f in fs])

    return dict(type=obj_type, obj_ids=[f.obj_id for f in fs])