import json

import redis

from nut.config import config

EMPTY_LIST = []
loads = json.loads

def connect_redis():
    return redis.Redis(**config.VCODE_REDIS)

rds = connect_redis()


RCODE_KEY = 'rcode.'   # key used for registering
FRCODE_KEY = 'frcode.' # key used for resetting password

def save_rcode(ph, code):
    _save_kv(RCODE_KEY + ph, code)

def get_rcode(ph):
    return _get_kv(RCODE_KEY + ph)

def delete_rcode(ph):
    return _del_kv(RCODE_KEY + ph)

def save_frcode(ph, code):
    _save_kv(FRCODE_KEY + ph, code)

def get_frcode(ph):
    return _get_kv(FRCODE_KEY + ph)

def delete_frcode(ph):
    _del_kv(FRCODE_KEY + ph)

def _save_kv(k, v):
    f = lambda rds: rds.setex(k, v, 600)
    _r(f)

def _get_kv(k):
    f = lambda rds: rds.get(k)
    return _r(f)

def _del_kv(k):
    f = lambda rds: rds.delete(k)
    _r(f)

def rcode_ttl(k):
    f = lambda rds: rds.ttl(RCODE_KEY + k)
    return _r(f)

def save_request_id(sig):
    f = lambda rds: rds.setex('sig.'+sig, 1, 120)
    _r(f)

def has_requestid(sig):
    f = lambda rds: rds.get('sig.'+sig)
    return _r(f) == b'1'

def _r(fn):
    try:
        return fn(rds)
    except Exception as e:
        if rds.ping(): return

    global rds
    rds = connect_redis()


if __name__ == '__main__':
    import threading
    from nut.utils.http_ import get_

    _url = 'http://yy.nuttek.com/app_data'

    threads = []
    for i in range(0,50):
        _t = threading.Thread(target=get_(_url))
        threads.append(_t)

    for _t in threads:
        _t.setDaemon(True)
        _t.start()