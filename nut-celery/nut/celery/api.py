from celery.result import AsyncResult

from nut.celery import task_logger as logger
from nut.celery.tasks import send_apn as send_apn_task
from nut.celery.tasks import send_jpush as send_jpush_task
from nut.thirdparty.apn import get_apn_message


def send_apn(token, alert, badge):
    args = dict(token_length=len(token or []), alert=alert, badge=badge)
    logger.info("Ready for scheduling SEND_APN_TASK with args: %s" % args)
    message = get_apn_message(token, alert, badge)
    send_apn_task.delay(message)
    logger.info("Scheduled for SEND_APN_TASK with args: %s" % args)


def send_jpush(tokens, message, badge, is_broadcast):
    args = dict(token_length=len(tokens or []), badge=badge, is_broadcast=is_broadcast)
    logger.info("Ready for scheduling SEND_JPUSH_TASK with args: %s" % args)
    result = send_jpush_task.delay(tokens, message, badge, is_broadcast)
    logger.info("State result of SEND_JPUSH_TASK(%s): %s" % (result, (result.state or 'NONE')))
    logger.info("Scheduled for SEND_JPUSH_TASK with args: %s" % args)



