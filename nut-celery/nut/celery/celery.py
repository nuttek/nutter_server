from celery import Celery

from nut.config import config

app = Celery('nutTasks', broker=config.CELERY_BROKER_URL,
             backend=config.CELERY_BACKEND_URL,
             include=['nut.celery.tasks'])

if __name__ == '__main__':
    app.start()
