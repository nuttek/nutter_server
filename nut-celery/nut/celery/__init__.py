from celery import Task
from celery.utils.log import get_logger, get_task_logger

from nut.thirdparty.apn import get_apn_client

logger = get_logger(__name__)
task_logger = get_task_logger(__name__)


class BaseTask(Task):
    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        _args = dict(status=status, retval=retval, task_id=task_id)
        task_logger.info("Entering into callback after_return with args %s" % _args)

        if einfo:
            task_logger.exception(einfo)

    def on_success(self, retval, *args, **kwargs):
        task_logger.info("Entering into callback on_success...")
        super(BaseTask, self).on_success(retval, *args, **kwargs)
        task_logger.info("Return value %s" % retval)
        if retval:
            for token, reason in retval.failed.items():
                code, errmsg = reason
                task_logger.info('Device failed: %s, reason: %s', token, errmsg)

            for code, errmsg in retval.errors:
                task_logger.info('Error: %r', errmsg)

            if retval.needs_retry():
                self.retry()

    def on_failure(self, task_id, *args, **kwargs):
        task_logger.info("Entering into callback on_failure...")

class APNTask(BaseTask):
    abstract = True
    _client = None

    @property
    def client(self):
        if self._client is None:
            self._client = get_apn_client()
        return self._client