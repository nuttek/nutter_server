import traceback
from nut.thirdparty.errors import APIResponseError
from nut.thirdparty.weibo import WeiboAPIClient

from nut.celery import APNTask, BaseTask
from nut.celery import task_logger as logger
from nut.celery.celery import app
from nut.core.pns.jpush_client import send as jpush_send


@app.task(base=APNTask)
def send_apn(message):
    try:
        logger.info("Sending via apn message with message:%s" % message)
        send_apn.client.send(message)
    except:
        traceback.print_exc()
        logger.exception('Can not connect to APNs, looks like network is down')


@app.task(base=BaseTask)
def send_jpush(tokens, message, badge, is_broadcast):
    try:
        args = dict(token_length=len(tokens or []), message=message, badge=badge, is_broadcast=is_broadcast)
        logger.info('Sending notification via JPush with args: %s' % args)
        jpush_send(is_broadcast, tokens, message, badge)
    except:
        traceback.print_exc()
        logger.exception('Fail to send a notfication to JPush!!!')

