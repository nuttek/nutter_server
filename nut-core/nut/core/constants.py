#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

''

from nut.utils.customerized_data_type import enum

HTTP_DATE_FORMAT = '%a, %d %b %Y %H:%M:%S GMT'


PNS_BROADCAST_TAGS = enum(
    ALL=0,
    TOKENS=999
)

HTTP_HEADERS = enum(
    LAST_MODIFIED='Last-Modified',
    IF_MODIFIED_SINCE='If-Modified-Since'
)

OBJ_TYPE = enum(
    PROVINCE='province',
    CITY='city',
    COUNTY='county',
    HOSPITAL='hospital',
    LEVEL='level',
    DEPARTMENT='department'
)

LEVELS = enum(
    HOSPITAL=0,
    DOCTOR=1
)
