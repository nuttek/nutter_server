

class GenericError(Exception):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        pass


class UserError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }


class BadParameterError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }

class BadStatusError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }

class AlreadyExistingError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }


class NotFoundError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }


class GenericBlockError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': 'BLOCK_ERROR',
                        'errMsg': self.msg
                    }
                }


class NotModified(GenericError):
    def __init__(self, err_code=304, msg='Not Modified'):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }


class InvalidImageException(GenericError):
    def __init__(self, msg):
        self.message = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': 'INVALID_IMAGE',
                        'errMsg': '图片上传失败：' + self.message
                    }
                }


class PaymentError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }


class DBAccessError(GenericError):
    def __init__(self, err_code, msg):
        self.code = err_code
        self.msg = msg

    def dict(self):
        return {
                    'success': False,
                    'errorResponse': {
                        'errCode': self.code,
                        'errMsg': self.msg
                    }
                }