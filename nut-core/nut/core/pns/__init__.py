# -*- coding: utf-8 -*-

__author__ = 'brucelee'

def generate_notification_with_extras(message, notify_type=None, id=None):
    extras=None
    if not (notify_type is None or id is None):
        extras = dict(notify_type=notify_type, id=id)

    if extras:
        return dict(message=message, extras=extras)

    return dict(message=message)

def generate_empty_notification():
    return dict(message='')



