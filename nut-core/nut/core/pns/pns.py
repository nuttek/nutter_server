# -*- coding: utf-8 -*-
__author__ = 'Bruce'

import logging

from .constants import PNS_PROVIDER
from nut.celery.api import send_apn, send_jpush

logger = logging.getLogger(__name__)

class PushNotificationService(object):
    __service_handler_type__ = None

    @classmethod
    def get_subclasses(cls, clazz):
        return clazz.__subclasses__()

    def get_service_handler(self, handler_type):
        if type is None or self.__service_handlers__ is None:
            return None
        return self.__service_handlers__.get(handler_type, None)

    def __init__(self):
        self.__service_handlers__ = dict([(c.__service_handler_type__, c())
                         for c in PushNotificationService.get_subclasses(PushNotificationService)])
        if not self.__service_handlers__:
            logger.info("PushNotifcationService handler list: %s" % (''.join(['('+str(h)+')' for h in self.__service_handlers__])))
        self.is_token_add_required = True
        self.is_broadcast = False

    def set_token_add_required(self, flag):
        self.is_token_add_required = flag

    def set_is_broadcast(self, flag):
        self.is_broadcast = flag

    def add_tokens(self, tokens, is_broadcast=False):
        for token in tokens:
            token_type = token.get('type', PNS_PROVIDER.DEFAULT)
            handler = self.get_service_handler(token_type)
            handler.set_token_add_required(not is_broadcast)
            handler.set_is_broadcast(is_broadcast)
            device_token = token.get('token', None)
            if device_token:
                handler.add_single_token(device_token)

    def add_single_token(self, single_token):
        self.token_list.append(single_token)

    def get_tokens(self):
        return self.token_list

    '''
    message only includes alter,
    badge always is '+1' for JPush, and 'increment' for apns_client,
    sound is 'default'
    '''
    def send(self, message, badge, tokens, is_broadcast=False):
        self.add_tokens(tokens, is_broadcast)
        for type, handler in self.__service_handlers__.items():
            handler.send(message, badge)



class DefaultPushNotificationService(PushNotificationService):
    __service_handler_type__ = PNS_PROVIDER.DEFAULT


    def __init__(self):
        self.token_list = []
        self.is_token_add_required = True
        self.is_broadcast = False

    def add_single_token(self, single_token):
        self.token_list.append(single_token)

    def get_tokens(self):
        return self.token_list

    def send(self, message, badge):
        args = dict(message=message, badge=badge)
        logger.debug("[DefaultPushNotificationService] entering with args:%s" % args)
        if badge is None:
            badge = 'increment'
        if self.token_list and len(self.token_list) > 0:
            alert = message.get("message", None)
            if alert:
                args = dict(message=message, badge=badge)
                logger.debug("[DefaultPushNotificationService] ready to send with args:%s" % args)
                send_apn(self.token_list, alert, badge)


class JPushNotificationService(PushNotificationService):
    __service_handler_type__ = PNS_PROVIDER.JPUSH

    def __init__(self):
        self.token_list = []
        self.is_token_add_required = True
        self.is_broadcast = False

    def add_single_token(self, single_token):
        if self.is_token_add_required:
            self.token_list.append(single_token)

    def get_tokens(self):
        return self.token_list

    def send(self, message, badge):
        args = dict(message=message, badge=badge)
        logger.debug("[DefaultPushNotificationService] entering with args:%s" % args)
        if badge is None:
            badge = '+1'
        if not self.is_broadcast and not (self.token_list and len(self.token_list) > 0): return

        args = dict(token_length=len(self.token_list or []), message=message, badge=badge, is_broadcast=self.is_broadcast)
        logger.debug("[DefaultPushNotificationService] ready to send with args:%s" % args)
        send_jpush(self.token_list, message, badge, self.is_broadcast)





