# -*- coding: utf-8 -*-
__author__ = 'Bruce'

import copy

import jpush as jpush

from nut.config.config import ENABLE_DEBUG_MODE
from nut.core.pns.constants import JPUSH_APP_KEY, JPUSH_MASTER_SECRET


def _device_regid(*types):
    '''
    override method of JPush, because in JPush 3.1.1 it has issue for key naming,
    it needs registration_id but in the logic it uses registration_ids
    '''
    registration_id = {}
    registration_id["registration_id"] = types
    return registration_id

def send(is_broadcast, tokens, message, badge='+1'):
    _jpush = jpush.JPush(JPUSH_APP_KEY, JPUSH_MASTER_SECRET)

    push = _jpush.create_push()
    is_apns_prod = False if ENABLE_DEBUG_MODE else True
    push.options = {'apns_production': is_apns_prod}

    alert = message.get("message", None)
    if not alert:
        sound = ''
    else:
        sound = 'default'
    extras = message.get("extras", None)
    if extras:
        ios_msg = jpush.ios(alert=alert, badge=badge, sound=sound, extras=extras)
        android_msg = jpush.android(alert=alert, extras=extras)
    else:
        ios_msg = jpush.ios(alert=alert, badge=badge, sound=sound)
        android_msg = jpush.android(alert=alert)

    push.notification = jpush.notification(alert=alert, android=android_msg, ios=ios_msg)

    push.platform = jpush.all_
    if is_broadcast:
        if not is_apns_prod: # dev environment
            _send_notification_to_android_in_dev_env(push)
            push.platform = jpush.platform('ios')
        push.audience = jpush.all_
    else:
        if not tokens or (tokens and len(tokens) <= 0):
            return
        else:
            reg_ids = tuple(tokens)
            push.audience = jpush.audience(
                        _device_regid(*reg_ids)
            )

    push.send()


def _send_notification_to_android_in_dev_env(push):
    _andr_push = copy.deepcopy(push)
    _andr_push.platform = jpush.platform('android')
    reg_ids = []
    if reg_ids and len(reg_ids) > 0:
        _andr_push.audience = jpush.audience(
                    _device_regid(*reg_ids)
        )
        _andr_push.send()

if __name__ == '__main__':
    from nut.pns.pns import PushNotificationService
    from nut.pns.constants import PNS_PROVIDER

    tokens = [dict(token='01046df9aff', type=PNS_PROVIDER.JPUSH)]
    message = dict(message='欢迎到娃娃公社来！！！ ［Test from JPush］')
    for i in range(1, 5000):
        pns = PushNotificationService()
        pns.send(message, '+1', tokens, False)