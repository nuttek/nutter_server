__author__ = 'brucelee'

import os
import logging
import logging.config

from nut.config import config
from nut.utils.file_sys import get_absolute_cur_path, get_absolute_parent_path


cur_path = get_absolute_cur_path(__file__)
parent_path = get_absolute_parent_path(cur_path)
full_path_conf_file = parent_path + os.sep + config.CONFIG_DIR_NAME + os.sep + config.CONFIG_FILE_NAME
try:
    logging.config.fileConfig(full_path_conf_file)
except Exception as e:
    import traceback
    traceback.print_exc()


def get_current_app():
    from nut.api import application as api_app
    from nut.web import application as web_app

    return api_app or web_app
