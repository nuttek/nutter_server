ENABLE_DEBUG_MODE = True

API_SUPPORT_EMAIL = 'service@nuttek.com'
API_DEFAULT_CACHE_TIMEOUT = 30 * 60 # seconds
API_PAGE_SIZE_LIMIT = 15

PASSWORD_SALT_LENGTH = 20 # bytes

USER_PROFILE_NAME_MAX_LIMIT = 30
USER_PROFILE_NAME_MIN_LIMIT = 4
USER_PROFILE_DESCRIPTION_MAX_LIMIT = 140

TIMEDELTA_FROM_UTC_TO_LOCAL_TIMEZONE = -8 * 60 * 60

SINA_WEIBO_API_PREFIX = 'https://api.weibo.com/2/'

# configuration
CONFIG_DIR_NAME = 'config'
CONFIG_FILE_NAME = 'logging.conf'

# 手机验证码失效时间 （seconds）
VCODE_EXPIRATION_IN_SECONDS = 9 * 60

# 加密算法的Feed，用于生成校验Request合法性的签名
SIGNATURE_FEED = '{nuttek.com@2016}'

# 手机号码校验正则表达式
REGEX_PATTERN_MOBILE_NUMBER = "^1\d{10}$"

# 用户名验证正则表达式
REGEX_PATTERN_USER_NAME ='^[\u4e00-\u9fa5a-zA-Z\d_-]+$'

DOCTOR_IMG_URL_PREFIX = 'http://images.eztcn.com.cn/images/doctor/ezt2.0/'
HOSP_IMG_URL_PREFIX = 'http://images.eztcn.com.cn/images/hospital/eztcn2.0/'

LICENSE_PAGE_URL = 'license.html'

FAKE_HOSP_DIST_URL = 'http://survey.nuttek.com/d2.html'

FAKE_USER_LICENSE_URL = 'http://wx.nuttek.com/template?template=license.html'

try:
    # import all configs here
    from .db import *
    from .redis import *
    from .apn import *
    from .cdn import *
    from .client import *
    from .celery import *
    from .cms import *
    from .sms import *
    from .wx import *

    from .local_config import *
except ImportError:
    pass

