__author__ = 'brucelee'

import os, sys

_virtual_env_path = os.environ.get('VIRTUAL_ENV', sys.prefix)

IMG_FILENAME_LENGTH = 16  # bytes
CMS_STATIC_DIR_PATH = os.path.join(_virtual_env_path, 'var', 'cms_contents')