__author__ = 'brucelee'

API_CACHE_REDIS_URL = 'redis://127.0.0.1:6379/1'

MESSAGE_REDIS_SERVERS = {
    'direct_message': {
        'host': '127.0.0.1',
        'port': 6379,
        'db': 2,
    },
    'group_message': {
        'host': '127.0.0.1',
        'port': 6379,
        'db': 2,
    },
    'fake_user_message': {
        'host': '127.0.0.1',
        'port': 6379,
        'db': 6,
    },
    'follower': {
        'host': '127.0.0.1',
        'port': 6379,
        'db': 2,
    }
}

VCODE_REDIS = {
    'host': '127.0.0.1',
    'port': 6379,
    'db': 3
}