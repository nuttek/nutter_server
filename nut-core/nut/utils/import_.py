# -*- coding: utf-8 -*-

__author__ = 'brucelee'

import os
import re

# TODO （likui@nuttek.com）complete the logic of dynamic importing
def dynamic_import(module_dir, module_regex):

    if not os.path.exists(module_dir):
        return

    module_compiler = re.compile(module_regex)

    list_dirs = os.walk(module_dir)
    for dirName, subdirList, fileList in list_dirs:
        for f in fileList:
            file_name = f

            if file_name[0:4] == "app_" and file_name[-3:] == ".py":
                impPath = ""
                if dirName[-1:] != "/":
                    impPath = dirName.replace("/",".")[2:]
                else :
                    impPath = dirName.replace("/",".")[2:-1]

                if impPath != "":
                    exe_str = "from " + impPath+"."+file_name[0:-3]+" import * "
                else:
                    exe_str = "from " +file_name[0:-3]+" import *"
                exec(exe_str,globals())


if __name__ == "__main__":
    dynamic_import('/Users/brucelee/projects/nuttek_server/yy/nut-core/nut/config', '.*_config\\.py')