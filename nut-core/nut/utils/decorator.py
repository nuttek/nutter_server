#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'brucelee'

from functools import wraps

from flask import request, abort

from nut.core.constants import HTTP_HEADERS
from nut.config import config
from nut.core.errors import UserError


def conditional_last_modified_date(obj=None):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            from nut.api.cached.misc import get_last_modified_date_by_obj
            from nut.core.errors import NotModified

            _header_lmd = request.headers.get(HTTP_HEADERS.IF_MODIFIED_SINCE)
            if _header_lmd:
                if obj:
                    cached_lmd = get_last_modified_date_by_obj(obj, **kwargs)
                    if cached_lmd and not (cached_lmd > _header_lmd):
                        raise NotModified

            return func(*args, **kwargs)
        return wrapper
    return decorator


def requires_pagination(func):
    @wraps(func)
    def decorated(*args, **kw):
        limit = config.API_PAGE_SIZE_LIMIT
        length = request.args.get('length', limit)
        next_id = request.args.get('next_id', 0)

        try:
            request.length = int(length)
        except:
            abort(400)

        try:
            request.next_id = int(next_id)
        except:
            request.next_id = next_id

        return func(*args, **kw)
    return decorated


def requires_auth(func):
    @wraps(func)
    def decorated(*args, **kw):
        user = request.user
        if user is None:
            abort(401)
        return func(*args, **kw)
    return decorated


def private_api(func):
    @wraps(func)
    def decorated(*args, **kw):
        scope = request.args.get('scope', None)
        if scope == 'private':
            return func(*args, **kw)
        abort(403)
    return decorated


def requires_privacy_allow(func):
    @wraps(func)
    def decorated(user_id, *args, **kw):
        from nut.api.cached.user import get_cached_user

        user = get_cached_user(user_id)
        if not user:
            raise UserError('USER_NOT_EXISTED', '该用户不存在')

        return func(user_id, *args, **kw)
    return decorated

