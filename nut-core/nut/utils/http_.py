__author__ = 'brucelee'


import json
import urllib3

from nut.utils.customerized_data_type import enum

http_methods = enum(
    get='GET',
    post='POST',
    put='PUT',
    delete='DELETE'
)

_http = urllib3.PoolManager()

def get_(url, fields=None, headers=None):
    resp = _http.request(http_methods.get, url, fields, headers)

    return json.loads(str(resp.data,'utf-8'))
