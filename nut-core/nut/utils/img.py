from nut.core.errors import InvalidImageException

__author__ = 'brucelee'
from decent import client


class ImgResizePolicy(object):
    __policy_name__ = None
    __item_type__ = None
    __r_width__ = None
    __r_height__ = None

    quality = 75

    @staticmethod
    def getPolicy(name):
        if name is None or __policies__ is None:
            return None

        return __policies__.get(name, None)

    @classmethod
    def checkImg(cls, img): pass

    @classmethod
    def resize(cls, img, *args): pass

    @classmethod
    def get_img_path(cls, img, fname):
        '''return a new file name
        '''
        return fname

    @classmethod
    def extraUrls(cls, file_path):
        '''return extra urls for a sepcific pat, e.g:
        if file_path is 'imgfile.jpg', then this function will return
        a dict represented as:

        {
            "w1242_url": "imgfile-w1242_url.jpg",
            "w750_url": "imgfile-w750_url.jpg",
            "w640_url": "imgfile-w640_url.jpg"
        }

        :param file_path, the file path passed for constructing

        return path dict
        '''
        pass


    @classmethod
    def postImg(cls, img, item_id, action=b'resize'):
        filename, data = b'notset.jpg', img
        if getattr(img, 'stream', None): 
            filename, data = img.filename.encode('utf-8'), img.stream.read()

        if img and item_id:
            req = [b'/services/nut/cms',
                   b'req',
                   data,
                   filename,
                   cls.__item_type__,
                   str(item_id).encode('utf-8'),
                   action,
                   b'1',
                   cls.__policy_name__
                   ]
            resp = client.request(*req)
            return cls.onResponse(resp)

        raise InvalidImageException('图片上传有误，请联系相关人员进行解决')


    @classmethod
    def postImgDirect(cls, img, item_id, action=b'resize'):
        from nut.cms import resize_imgs
        filename, data = b'notset.jpg', img
        if getattr(img, 'stream', None):
            filename, data = img.filename.encode('utf-8'), img.stream.read()

        code, fpath = resize_imgs(data, filename,
                    cls.__item_type__,
                    str(item_id).encode('utf-8'),
                    False,
                    cls.__policy_name__)
        if code != b'200':raise InvalidImageException('图片上传有误，请联系相关人员进行解决')

        return fpath.decode()

    @classmethod
    def onResponse(cls, resp): pass

class DirectSaveImgPolicy(ImgResizePolicy):
    @classmethod
    def postImg(cls, img, item_id):
        return super(DirectSaveImgPolicy, cls).postImg(img, item_id, b'save')

    @classmethod
    def onResponse(cls, resp):
        if resp[1] == b'200':
            return resp[2].decode('utf-8')
        raise InvalidImageException('保存图片失败。')


class ProfileImageResizePolicy(ImgResizePolicy):
    __r_width__ = 240
    __r_height__ = 240

    __policy_name__ = b'profile-img'
    __item_type__ = "user_profile".encode('ascii')
    size = (__r_width__, __r_height__)

    @classmethod
    def checkImg(cls, img):
        (w, h) = img.size
        if w < 120: return -404

        return 1

    @classmethod
    def onResponse(cls, resp):
        if resp[0] != b'rep':
            return ''
        if resp[1] == b'200':
            return resp[2].decode('utf-8')
        if resp[1] == b'404':
            raise InvalidImageException('图片有点儿小。')

    @classmethod
    def resize(cls, img):
        (w, h) = img.size
        if w <= 180: # old client.
            return [img]

        return [img.resize(cls.size)]

    @classmethod
    def get_img_path(cls, img, fname):
        '''return a new file name
        '''
        (w, h) = img.size
        if w <= 180: return fname

        dxes = fname.split('.')
        return '%s-w240.%s' % (dxes[0], dxes[1])



def get_sizes(s, ws) :
    return [(w, int(w / s)) for w in ws]


try:

    def get_subclasses(clazz):
        clazzes = clazz.__subclasses__()
        results = clazzes.copy()
        for clz in clazzes:
            results.extend(get_subclasses(clz))

        return results

    __policies__ = dict([(c.__policy_name__, c)
                         for c in get_subclasses(ImgResizePolicy)])
except: pass

