__author__ = 'brucelee'


class DottableDict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.__dict__ = self


def enum(**named_values):
    return type('Enum', (), named_values)


class Switch(object):
    def __init__(self, v):
        self.v = v
        self.fail = False

    def __iter__(self):
        yield self.match
        raise StopIteration

    def match(self, *args):
        if self.fail or not args:
            return True
        elif self.v in args:
            self.fail = True
            return True
        else:
            return False


if __name__ == "__main__":
    v = 'eleven'

    for case in Switch(v):
        if case('one'):
            print('one')
            break
        if case('ten'):
            print('ten')
            break
        if case():
            print('Default')

