__author__ = 'brucelee'

from sqlalchemy import and_, not_

from nut.api import Session
from nut.db.models import Hospitals
from nut.utils.customerized_data_type import enum
from nut.utils.http_ import get_

COORD_SYS = enum(
    g="gps",
    mb="mapbar",
    b="baidu"
)

AMAP_COORDS_COVERSION_URL = 'http://restapi.amap.com/v3/assistant/coordinate/convert?'
AMAP_WEB_KEY = '2f0d56807271a495f695d658a2b14c8f'


def convert_float_per_fine(fine):
    pass

class Geo:
    @staticmethod
    def covert_coord(self, src, location_list):
        pass


class AMap(Geo):
    @staticmethod
    def covert_coord(src, location_list, output_fmt):
        for loc in location_list:
            if not loc.lat or not loc.lon: continue

            _loc = '%s,%s' % (loc.lon, loc.lat)

            parms = dict(
                key=AMAP_WEB_KEY,
                locations=_loc,
                coordsys=src,
                output=output_fmt
            )

            parm_list = ['%s=%s' % (x, parms[x]) for x in parms.keys()]

            query_str = '&'.join(parm_list)
            _url = AMAP_COORDS_COVERSION_URL + query_str

            json_str = get_(_url)
            converted_loc = json_str['locations']
            converted_lon, converted_lat = converted_loc.split(',')

            converted_lon = float('%0.4f' % float(converted_lon))
            converted_lat = float('%0.4f' % float(converted_lat))

            print('[%s] - [%f, %f]' % (_loc, converted_lon, converted_lat))

            loc.lon = converted_lon
            loc.lat = converted_lat


if __name__ == "__main__":
    location_objs = Session.query(Hospitals)\
        .filter(and_(not_(Hospitals.lat is None), not_(Hospitals.lon is None))).all()
    AMap.covert_coord(COORD_SYS.b, location_objs, 'JSON')
    Session.commit()
