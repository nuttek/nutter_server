__author__ = 'thomas'

from flask import current_app
from flask.ext.cache import Cache
import functools, logging

logger = logging.getLogger(__name__)


class NutCache(Cache):
    def multiple_get_memoize(self, timeout=None, make_name=None, unless=None):

        def memoize(f):

            @functools.wraps(f)
            def decorated_function(*args, **kwargs):

                #: bypass cache
                if callable(unless) and unless() is True:
                    return f(*args, **kwargs)

                mkeys = kwargs.pop('mkeys', None)
                if mkeys is None:
                    return decorated_function.get_single_val(*args, **kwargs)

                _keys = [decorated_function.make_cache_key(f, k) for k in mkeys]
                _vals = self.cache.get_many(*_keys)

                for i in range(len(_vals)):
                    if _vals[i]: continue

                    key = mkeys[i]
                    rv = f(key)
                    _vals[i] = rv

                    try:
                        self.cache.set(_keys[i], rv,
                                   timeout=decorated_function.cache_timeout)
                    except Exception:
                        if current_app.debug:
                            raise
                        logger.exception("Exception possibly due to cache backend.")

                return dict(zip(mkeys, _vals))

            def get_single_val(*args, **kwargs):
                try:
                    cache_key = decorated_function.make_cache_key(f, *args, **kwargs)
                    rv = self.cache.get(cache_key)
                except Exception:
                    if current_app.debug:
                        raise
                    logger.exception("Exception possibly due to cache backend.")
                    return f(*args, **kwargs)

                if rv is None:
                    rv = f(*args, **kwargs)
                    try:
                        self.cache.set(cache_key, rv,
                                   timeout=decorated_function.cache_timeout)
                    except Exception:
                        if current_app.debug:
                            raise
                        logger.exception("Exception possibly due to cache backend.")
                return rv

            decorated_function.uncached = f
            decorated_function.cache_timeout = timeout
            decorated_function.get_single_val = get_single_val
            decorated_function.make_cache_key = self._memoize_make_cache_key(
                                                make_name, decorated_function)
            decorated_function.delete_memoized = lambda: self.delete_memoized(f)

            return decorated_function
        return memoize
