import hashlib
import json
import os
import traceback
import time
import re
import logging
from base64 import urlsafe_b64decode, urlsafe_b64encode
from datetime import timedelta, datetime
from functools import wraps
from random import choice, randint

from psycopg2 import errorcodes
from sqlalchemy import not_
from sqlalchemy.exc import DataError, IntegrityError, ProgrammingError
from decent import client
from werkzeug.exceptions import abort

from nut.config import config
from nut.core.pns.pns import PushNotificationService

logger = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

mobile_number_regex_compiler = re.compile(config.REGEX_PATTERN_MOBILE_NUMBER)


def is_legal_mobile_number(mobile_number):
    if mobile_number is None: return True
    return not mobile_number_regex_compiler.match(mobile_number.strip())


def get_page_link(sn):
    if sn is None: return None
    return 'http://%s/pages/%s.whtml' % (config.WEB_SERVER_ADDRESS, sn)


def make_SSHA_password(password, salt=None):
    if salt is None:
        salt = os.urandom(config.PASSWORD_SALT_LENGTH)
    h = hashlib.sha1(password)
    h.update(salt)
    return urlsafe_b64encode(h.digest() + salt)


def verify_SSHA_password(hashed, password):
    decoded = urlsafe_b64decode(hashed)
    salt = decoded[config.PASSWORD_SALT_LENGTH:]
    return hashed.encode('utf-8') == make_SSHA_password(
        password.encode('utf-8'), salt)


def post_image_to_cms(img, item_type, item_id):
    if img and item_type and item_id:
        req = [b'/services/nut/cms', b'req', img.stream.read(),
               img.filename.encode('utf-8'), item_type.encode('utf-8'),
               str(item_id).encode('utf-8')]
        resp = client.request(*req)
        if resp[0] == b'rep' and resp[1] == b'200':
            return resp[2].decode('utf-8')
    return ''


def post_sms(mobile, content):
    req = [b'/services/nut/sms', b'req',
           mobile.encode('utf-8'), content.encode('utf-8')]
    resp = client.request(*req)
    # TODO: more loggings.
    return True


def catch_integrity_errors(session, error_callback):
    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kw):
            try:
                return func(*args, **kw)
            except (DataError, IntegrityError, ProgrammingError) as e:
                session.rollback()
                reason = str(e)
                if e.orig.pgcode == errorcodes.UNIQUE_VIOLATION:
                    reason = '数据已存在, %r' % e.params
                return error_callback(success=False,
                                      error_reason=reason)
            except:
                session.rollback()
                traceback.print_exc()
                return error_callback(success=False, error_reason='系统错误')
        return wrapped
    return decorator


def format_datetime(dt, format=None, to_utc=False):
    if dt:
        if to_utc:
            dt = timedelta(
                seconds=config.TIMEDELTA_FROM_UTC_TO_LOCAL_TIMEZONE) + dt
        format = format or '%Y-%m-%d %H:%M:%S'
        return dt.strftime(format)
    return dt


def parse_datetime(dt_str, format=None, from_utc=False):
    if dt_str:
        format = format or '%Y-%m-%d %H:%M:%S'
        dt = datetime.strptime(dt_str, format)
        if from_utc:
            dt = dt - timedelta(
                seconds=config.TIMEDELTA_FROM_UTC_TO_LOCAL_TIMEZONE)
        return dt
    return None


def timestamp_2_datetime(timestamp):
    if timestamp:
        return parse_datetime(time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(int(timestamp))))
    return timestamp


def format_count(count):
    if count < 10000:
        return str(count)
    elif count < 100000000:
        return '%.1f万' % (count / 10000)
    else:
        return '%.1f亿' % (count / 100000000)


def allowed_image_filename(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_new_follower_remain_days():
    return config.NEW_FOLLOWER_EXPIRATION_DAYS


def get_bool_value(s):
    if s and s.lower() == str(True).lower(): return True
    return False


def _to_datetime(dt_str, format=None):
    if dt_str:
        if format:
            dt = datetime.strptime(dt_str, format)
        else:
            try:
                dt = datetime.strptime(dt_str, "%a %b %d %H:%M:%S %Y")
            except:
                dt = datetime.strptime(dt_str, "%a, %d %b %Y %H:%M:%S %Z")
        return dt
    return None


R = 'ABCDEF'
RE = re.compile(r'[%s]' % R, re.IGNORECASE|re.UNICODE)


def enctid(tid):
    '''Simple chars replacement'''

    s = list(str(tid))
    s[0], s[-1] = s[-1], s[0]
    if len(s) > 2:
        s[1], s[-2] = s[-2], s[1]
    return ''.join([x + choice(R) for x in s])


def dectid(tid):
    s = list(RE.sub('', tid))
    s[0], s[-1] = s[-1], s[0]
    if len(s) > 2:
        s[1], s[-2] = s[-2], s[1]
    return ''.join(s)


def fmtTlResp(tl, version=None):
    if not isTlofVersion11(version): return tl

    _user_id = tl.get("userID", None)
    _user_name = tl.get("userName", None)
    _desc = tl.get("desc", None)
    _user_level = tl.get("userLevel", None)
    _profile_img_url = tl.get("profileImgURL", None)
    _is_official = tl.get("isOfficial", None)
    _gender = tl.get("gender", None)
    _followed = tl.get("followed", None)
    _owner = dict()
    if _user_id is not None:
        _owner.update(dict(userID=_user_id))
        del(tl["userID"])
    if _user_name is not None:
        _owner.update(dict(userName=_user_name))
        del(tl["userName"])
    if _desc is not None:
        _owner.update(dict(desc=_desc))
        del(tl["desc"])
    if _user_level is not None:
        _owner.update(dict(userLevel=_user_level))
        del(tl["userLevel"])
    if _profile_img_url is not None:
        _owner.update(dict(profileImgURL=_profile_img_url))
        del(tl["profileImgURL"])
    if _is_official is not None:
        _owner.update(dict(isOfficial=_is_official))
        del(tl["isOfficial"])
    if _gender is not None:
        _owner.update(dict(gender=_gender))
        del(tl["gender"])
    if _followed is not None:
        _owner.update(dict(followed=_followed))
        del(tl["followed"])

    tl.update(owner=_owner)

    return tl


def get_cdn_image_path(image_path):
    if not image_path: return ''
    if 'http' in image_path:
        return image_path
    elif image_path:
        return config.CDN_URL_PREFIX + image_path
    else:
        return ''


def get_full_url(obj, img_url):
    return ''.join([get_url_prefix(''), os.sep, img_url])


def request_message_serv(*args):
    req = []
    for r in ['/services/nut/message', 'req'] + list(args):
        req.append(str(r).encode('utf-8'))
    resp = client.request(*req)
    if resp[0] == b'rep':
        if resp[1] == b'200':
            return json.loads(resp[2].decode('utf-8'))
        else:
            abort(int(resp[1]))
    else:
        abort(500)


def _format_apn_message(message):
    alert = message.get("message", None)
    if not alert: return message

    if len(alert) > config.APN_MESSAGE_SIZE_LIMIT:
        alert = '%s...' % alert[:config.APN_MESSAGE_SIZE_LIMIT]
        message.update(message=alert)
    return message


def send_apn_message(user_id, message, badge, is_online_only=True):
    message = _format_apn_message(message)

    filter_conditions = [Terminals.user_id == user_id]
    if is_online_only:
        filter_conditions.append(Terminals.is_online)
    device_tokens = Session.query(Terminals.token_type, Terminals.device_token).filter(
        *filter_conditions).all()
    device_token_list = [{
                             'type': token.token_type,
                             'token': token.device_token
                         }
                         for token in device_tokens]
    args = dict(token_length=len(device_token_list or []), message=message, badge=badge)
    logger.debug("Ready to send apn message, args:%s" % args)
    _send_apn_message(device_token_list, message, badge)


def _send_apn_message(device_tokens, message, badge, is_broadcast=False):
    pns = PushNotificationService()
    pns.send(message, badge, device_tokens, is_broadcast)


def broadcast_apn_message(message, badge, is_online_only=False):
    from nut.api import Session
    from nut.db.models import Terminals, Users

    _format_apn_message(message)

    filter_conditions = [not_(Users.is_fake)]
    device_token_list = []
    if is_online_only:
        filter_conditions.append(Terminals.is_online)
    device_tokens = Session.query(Terminals.token_type, Terminals.device_token)\
        .outerjoin(Users, Terminals.user_id == Users.id).\
        filter(*filter_conditions).all()

    if device_tokens:
        device_token_list = [{
                             'type': token.token_type,
                             'token': token.device_token
                         }
                         for token in device_tokens]
    _send_apn_message(device_token_list, message, badge, True)


def genrate_code(length=6):
    _ = '0123456789'
    return ''.join([_[randint(0, 9)] for x in range(length)])


def is_valid_length(check_str, min_length, max_length):
    str_length = len(check_str)
    utf_str_length = len(check_str.encode('utf-8'))
    noascii_count = (utf_str_length - str_length) / 2
    ascii_count = str_length - noascii_count
    total_length = ascii_count + 2 * noascii_count
    if total_length < min_length:
        return -1
    if total_length > max_length:
        return 1
    return 0


def get_pagination_of_objs(objs, next_id, length, get_id_func=None):
    _index = 0
    _next_id = None
    if next_id:
        if get_id_func:
            obj_ids = [get_id_func(obj) for obj in objs]
        else:
            obj_ids = [obj.id for obj in objs]
        _index = obj_ids.index(next_id)

    objs = objs[_index: (_index + length + 1)]

    if objs:
        if len(objs) > length:
            last_obj = objs[-1]
            objs = objs[:len(objs) - 1]
            if get_id_func:
                _next_id = get_id_func(last_obj)
            else:
                if hasattr(last_obj, 'get'):
                    _next_id = last_obj.get('id')
                else:
                    _next_id = last_obj.id

    return objs, _next_id


def get_url_prefix(obj):
    return ''


def get_badge_count(user_id):
    return 0


def get_pagination_objs_and_next_id(objs, length):
    objs_length = len(objs)
    if objs_length > length:
        obj_list = objs[:length]
        next_id = objs[length]
    else:
        obj_list = objs
        next_id = None

    return obj_list, next_id

