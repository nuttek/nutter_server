from flask import render_template

from nut.web import application as app


@app.route('/password')
def password():
    return render_template('password.html')


