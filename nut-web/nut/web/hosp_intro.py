from flask import render_template

from nut.web import application as app


@app.route('hosp_intro')
def hosp_intro():
    return render_template('hosp_intro.html')

