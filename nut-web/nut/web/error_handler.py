from flask import jsonify, request

from nut.web import application as app
from nut.core.errors import UserError


@app.errorhandler(UserError)
def error_handle(e):
    message = e.dict()
    resp = jsonify(message)
    return resp, 200


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'success': False,
        'errorResponse': {
            'errCode': 404,
            'errMsg': 'Not Found: ' + request.url
        }
    }
    resp = jsonify(message)
    resp.status_code = 404
    return resp


@app.errorhandler(400)
def bad_request(error=None):
    message = {
        'success': False,
        'errorResponse': {
            'errCode': 400,
            'errMsg': 'Bad Request'
        }
    }
    resp = jsonify(message)
    resp.status_code = 400
    return resp


@app.errorhandler(401)
def unauthorized(error=None):
    message = {
        'success': False,
        'errorResponse': {
            'errCode': 401,
            'errMsg': 'Unauthorized'
        }
    }
    resp = jsonify(message)
    resp.status_code = 401
    return resp


@app.errorhandler(403)
def unauthorized(error=None):
    message = {
        'success': False,
        'errorResponse': {
            'errCode': 403,
            'errMsg': 'Forbidden'
        }
    }
    resp = jsonify(message)
    resp.status_code = 403
    return resp


@app.errorhandler(500)
@app.errorhandler(Exception)
def server_exception(error=None):
    app.logger.exception(error)
    message = {
        'success': False,
        'errorResponse': {
            'errCode': 'SERVER_ERROR',
            'errMsg': '系统错误'
        }
    }
    resp = jsonify(message)
    resp.status_code = 500
    return resp
