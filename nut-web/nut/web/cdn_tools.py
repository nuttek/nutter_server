__author__ = 'thomas'
import hmac
import time
import uuid
from urllib.parse import quote as urlencode
from urllib.parse import urlencode as toquerystring
from base64 import encodebytes as base64_encoded
from hashlib import sha1

import requests

from nut.config import CDN_ACCESS_KEY

access_id = CDN_ACCESS_KEY['id']
access_secret = (CDN_ACCESS_KEY['secret'] + '&').encode('ascii')
stks = 'GET&%2F&'
cdn_server_address = 'https://cdn.aliyuncs.com'

def flush_file(fname):
    params = dict(Action='RefreshObjectCaches', ObjectPath=fname)
    url = compose_url(params)
    try:
        rsp = requests.get(url)
        print('CDN log: %s' % rsp.text)
    except Exception as e:
        print(e)

def compose_url(user_params):
    '''Compose the api's url.
    '''
    timestamp = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())

    parameters = { \
            'Format'        : 'JSON', \
            'Version'       : '2014-11-11', \
            'AccessKeyId'   : access_id, \
            'SignatureVersion'  : '1.0', \
            'SignatureMethod'   : 'HMAC-SHA1', \
            'SignatureNonce'    : str(uuid.uuid1()), \
            'TimeStamp'         : timestamp, \
    }

    parameters.update(user_params)

    parameters['Signature'] = sig(parameters)

    return cdn_server_address + "/?" + toquerystring(parameters)

def sig(params):
    '''Compute signature based on the parameters.
    '''
    ks = ['%s=%s' % (k, _encode(v)) for k, v in
          sorted(params.items(), key=lambda c: c[0])]

    s = stks + _encode('&'.join(ks))

    h = hmac.new(access_secret, s.encode('ascii'), sha1)

    return base64_encoded(h.digest()).strip()


def _encode(s):
    '''URL encoded a string.'''

    return urlencode(s, '')\
        .replace('+', '%20')\
        .replace('*', '%2A')\
        .replace('%7E', '~')

if __name__ == '__main__':
    flush_file('http://web.lovewawa.com.cn/static/img/app-icon.png')