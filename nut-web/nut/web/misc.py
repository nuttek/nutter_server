__author__ = 'bruceelee'

import urllib

from flask import render_template, request, jsonify

from nut.web import application as app
from nut.api import Session
from nut.db.models import Hospitals as h, Doctors as d


@app.route('/wawa-img-proxy')
def img_proxy():
    url = request.args.get('url', None)
    resp = urllib.request.urlopen(url)

    status = resp.status
    headers = resp.headers
    data = resp.readall()

    return data, status, {'Content-Type': headers.get('content-type'),
                          'Access-Control-Allow-Origin': '*'}


@app.route('/template')
def page_forwarder():
    template_name = request.args.get('template')
    return render_template(template_name)


@app.route('/imgs_page')
def get_imgs_page():

    return render_template('images.html')


@app.route('/imgs', methods=['GET'])
def get_img():
    from nut.db.models import make_img_url_with_url_prefix
    from nut.config import config

    hosp_objs = Session.query(h.id, h.name, h.logo).all()
    hosp_objs = [dict(id=o.id, name=o.name,
                      img_url=make_img_url_with_url_prefix(config.HOSP_IMG_URL_PREFIX, o.logo)) for o in hosp_objs]

    doctor_objs = Session.query(d.id, d.name, d.thumbnail).all()
    doctor_objs = [dict(id=o.id, name=o.name,
                        img_url=make_img_url_with_url_prefix(config.DOCTOR_IMG_URL_PREFIX,
                                                             o.thumbnail)) for o in doctor_objs]

    return jsonify(success=True, hosp_objs=hosp_objs, doctor_objs=doctor_objs)


@app.route('/imgs', methods=['DELETE'])
def clear_img():

    id = request.args.get('id', None)
    type = request.args.get('type', None)

    if not (id and type):
        return jsonify(success=False)

    is_data_dirty = False
    if type == 'hosp':
        obj = Session.query(h).get(int(id));
        if obj:
            obj.logo = None
            is_data_dirty = True
    else:
        obj = Session.query(d).get(int(id));
        if obj:
            obj.thumbnail = None
            is_data_dirty = True

    if is_data_dirty:
        Session.commit()

    return jsonify(success=True)

