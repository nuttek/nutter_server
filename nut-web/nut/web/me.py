from flask import render_template
from flask.ext.login import login_required, current_user

from nut.web import application as app


@app.route('/me')
@login_required
def do_get_me_page():
    data = current_user.basic_profile()
    return render_template('me.html', **data)


