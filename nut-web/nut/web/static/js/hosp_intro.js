

$(document).ready(function () {
    $(".card.left").click(function () {
        var self = $(this);
        window.location.href = "/hospital/" + self.data("id") + "/description";
    });
    
    $(".card.center").click(function () {
        var self = $(this);
        var href = self.data("href");
        window.location.href = href;

        return false;
    });

    $(".cell").click(function () {
        var self = $(this);
        var hosp_id = self.data("id");
        
        if(hosp_id != undefined){
            var dept_id = localStorage.getItem("dept_id");
            if(dept_id != undefined && dept_id != ""){
                window.location.href = "/treatments";
            }
            else{
                localStorage.setItem("hosp_id", hosp_id);
                localStorage.setItem("is_treatment", "1");
                window.location.href = "/page?b_type=dept";
            }

            return false;
        }
        self.removeClass("hover");
    });

    $(".cell").visits(
        function () {
            $(this).addClass("hover");
        }
    );

});
