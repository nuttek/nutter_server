
$(document).ready(function () {
    $('#btn-logout').click(function(){
        var req_url = '/signout';
        $.ajax({
            url: req_url,
            method: 'GET',
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {
                if (!data['success']) {
                    alert(data['message']);
                } else {
                    window.location.href = '/me';
                }
            }
        });
        return false;
    });

});
