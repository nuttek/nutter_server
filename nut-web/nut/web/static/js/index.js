
var doc = $(document);

doc.ready(function () {
    $('div.cardf').click(function(){
        window.location.href = '/home?action=' + $(this).data().action;
    });

    $("#region_dropdown").click(function(e){
        window.location.href = '/region';
    });

    $("div.media").click(function(e){
        self = $(this);
        bType = self.data("b-type");

        if(bType != "hosp" && bType != "dept" && bType != "body" && bType != "doctor"){
            return false;
        }

        if(bType == "hosp" || bType == "dept"){
            localStorage.removeItem('hosp_id');
            localStorage.removeItem('dept_id');
        }

        window.location.href = '/page?b_type=' + bType;
    });

    $("#searchbtn").click(function(){
       window.location.href = "/search/page";
    });
    
    init();

});

function init(){
    $(".loading").spin();
    setInterval(bigImgLazyload, 1000);
}

function bigImgLazyload(){
    $("img.lazy").lazyload();
    $(".loading").spin(false);
}
