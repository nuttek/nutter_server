var feed = '{nuttek.com@2016}';

$(document).ready(function () {
    var numbers = /^1\d{10}$/;

    $('#vcode-button').click(function (e) {
        var mobile = $("input[name=username]").val();
        if (!numbers.test(mobile)) {
            alert("请输入有效的手机号码！");
            return false;
        }
        var time = 30;
        function timeCountDown() {
            if (time == 0) {
                clearInterval(timer);
                $('#vcode-button').addClass('button-vcode enable').removeClass('button-vcode disable').html("发送验证码");
                $('#vcode-button').removeAttr("disabled");
                return true;
            }
            $('#vcode-button').attr("disabled", "true");
            $('#vcode-button').html(time + "S后再次发送");
            time--;
            return false;
        }

        $(this).addClass('button-vcode disable').removeClass('button-vcode enable');
        timeCountDown();
        var timer = setInterval(timeCountDown, 1000);

        var ts = (new Date()).valueOf();
        var sig = $.md5(mobile + ':' + feed + ':' + ts);

        var req_url = '/vcode?mobile=' + mobile + '&sig=' + sig + '&seed=' + ts;
        $.ajax({
            url: req_url,
            method: 'GET',
            contentType: false,
            processData: false,
            cache: false,
            success: function (data) {
                if (!data['success']) {
                    alert(data['message']);
                } else {

                }
            }
        });
    });

    $('#submit-btn').click(function (e) {
        var formData = new FormData();

        formData.append("username", $('input[name=username]').val());
        formData.append("vcode", $('input[name=vcode]').val());
        formData.append("password", $('input[name=password]').val());

        action = $('#action').val();

        if(action){
            formData.append("action", action);
        }

        var req_url = '/registration';
        $.ajax({
            url: req_url,
            method: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            data: formData,
            success: function (data) {
                if (!data['success']) {
                    errResp = data['errorResponse']
                    alert(errResp['errMsg']);
                    $('input[name=username]').focus();
                    return false;
                } else {
                    window.location.href = '/signin';
                }
            }
        });
    });
});
