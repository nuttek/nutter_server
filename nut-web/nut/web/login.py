from flask import render_template, request, redirect, url_for, jsonify
from flask.ext.login import UserMixin, login_user, logout_user, current_user

from . import login_manager
from nut.core.errors import UserError
from nut.api.registration import register_mobile_or_reset_password
from nut.db.models import Users
from nut.web import Session
from nut.web import application as app
from nut.utils.misc import verify_SSHA_password

login_manager.login_view = '/signin'


class LoginUser(Users, UserMixin):
    pass


@login_manager.user_loader
def load_user(userid):
    return Session.query(LoginUser).get(int(userid))


def do_get(**args):
    return render_template('signin.html', **args)


@app.route('/signin')
def do_get_login():
    return render_template('signin.html', next='/')


@app.route('/signin', methods=['POST'])
def post_login():
    username = request.form.get('username', None)
    password = request.form.get('password', None)
    _next = request.form.get('next', '')

    if username is None:
        return do_get(msg='用户名不能为空！', next=_next)
    if password is None:
        return do_get(msg='密码不能为空！', next=_next)

    query_obj = Session.query(LoginUser)
    user = query_obj.filter_by(mobile=username).first()
    if not user:
        return do_get(msg='用户不存在！', next=_next)

    if not verify_SSHA_password(user.passwd, password):
        return do_get(msg='密码错误！', next=_next)

    login_user(user)

    if _next:
        return redirect(_next)

    data = current_user.basic_profile()
    return render_template('me.html', **data)


@app.route('/registration')
def do_get_registration():
    action = request.args.get('action', None)
    if action and action == 'reset':
        return render_template('registration.html', action='reset')

    return render_template('registration.html', next='/')


@app.route('/registration', methods=['POST'])
def do_post_registration():
    mobile = request.form.get('username', None)
    vcode = request.form.get('vcode', None)
    password = request.form.get('password', None)

    action = request.form.get('action', None)

    if not (action and action == "reset"):
        user = Session.query(Users).filter(Users.mobile == mobile).all()
        if user:
            raise UserError("User_Already_Existed", '用户已存在，请直接登录！')

    user_id = register_mobile_or_reset_password(mobile, password, vcode)

    return jsonify(success=True, user_info=dict(id=user_id))


@app.route('/vcode', methods=['GET'])
def do_get_vcode():
    from nut.api.f import vcode

    mobile = request.args.get('mobile', None)
    sig = request.args.get('sig', None)

    vcode(mobile, sig)

    return jsonify(success=True)


@app.route('/signout')
def signout():
    logout_user()
    return jsonify(success=True)
