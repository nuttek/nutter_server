import os

from flask import (request, render_template, send_from_directory, jsonify)

from nut.config import config
from nut.web import application as app
from nut.utils.customerized_data_type import Switch
from nut.api import Session
from nut.db.models import Hospitals

WEEKDAY_NAMES = ['星期一','星期二','星期三','星期四','星期五','星期六','星期日']


@app.route('/home')
def index():
    action = request.args.get('action', None)

    if action and action == 'hosp':
        return render_template('hosp_intro.html')

    return render_template('index.html')


@app.route('/img/<path:path>')
def image(path):
    return send_from_directory(os.path.join(
        config.CMS_STATIC_DIR_PATH, 'images'), path)


@app.route('/region')
def do_get_region():
    return render_template('region.html')


@app.route('/page')
def do_get_page():
    b_type = request.args.get("b_type")
    dept_id = request.args.get("dept_id")

    data = {}
    if dept_id:
        data.update(dept_id=dept_id)
    url = get_page_url(b_type)

    return render_template(url, **data)


@app.route('/hospital/<int:hosp_id>')
def do_get_hospital_info_page(hosp_id):
    hosp_obj = Session.query(Hospitals).filter(Hospitals.id == hosp_id).first()

    return render_template("hosp_intro.html", **(hosp_obj.full_dict()))


@app.route('/doctors/<int:doctor_id>/page')
def do_get_doctor_info_page(doctor_id):

    return render_template("doctor_intro.html", **(dict(doctor_id=doctor_id)))


@app.route('/hospital/<int:hosp_id>/description')
def do_get_hospital_detail_info_page(hosp_id):
    hosp_obj = Session.query(Hospitals).filter(Hospitals.id == hosp_id).first()

    return render_template("hosp_intro_detail.html", **(hosp_obj.full_dict()))


@app.route('/treatments')
def do_get_treatments_page():

    return render_template("hosp_treatment.html")


@app.route('/hospital/<int:hosp_id>/dept/<int:dept_id>/treatments')
def do_get_hospital_dept_treatments_page(hosp_id, dept_id):
    from nut.api.treatments import generate_and_get_treatments
    from nut.api.cached.treatments import get_cached_treatment_dates
    from nut.utils.misc import parse_datetime

    doctor_list = generate_and_get_treatments(hosp_id, dept_id)

    days = get_cached_treatment_dates()

    day_list = []
    for day in days:
        dt = parse_datetime(day, '%Y-%m-%d')
        _day = dict(date=day, day='%d/%d' %(dt.month, dt.day), weekday_name=WEEKDAY_NAMES[dt.weekday()])
        day_list.append(_day)

    return jsonify(success=True, days=day_list, doctors=doctor_list)


@app.route('/search/page')
def do_get_search_page():
    return render_template("search.html")


@app.route('/symps/page')
def do_get_symps_page():
    _id = request.args.get('id')
    d = dict(id=_id)
    return render_template("symps.html", **d)


def get_page_url(b_type):
    url = "hosp_list.html"

    if b_type == "hosp":
        url = "hosp_list.html"
    if b_type == "dept":
        url = "dept.html"
    if b_type == "doctor":
        url = "expert.html"
    if b_type == "body":
        url = "body.html"

    return url




