from decent.web import libs
from flask import Flask, request
from flask.ext.assets import Environment, Bundle
from flask.ext.login import LoginManager
from sqlalchemy.orm import scoped_session
from werkzeug import url_decode

from nut.config import config
from nut.db import env


# The main application
application = Flask(__name__)


class MethodRewriteMiddleware(object):

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if 'METHOD_OVERRIDE' in environ.get('QUERY_STRING', ''):
            args = url_decode(environ['QUERY_STRING'])
            method = args.get('__METHOD_OVERRIDE__')
            if method:
                method = method.encode('ascii', 'replace')
                environ['REQUEST_METHOD'] = method
        return self.app(environ, start_response)

application.wsgi_app = MethodRewriteMiddleware(application.wsgi_app)

application.secret_key = 'XPXu04w~(exW5lm`JT3c'
application.debug = config.ENABLE_DEBUG_MODE

# Flask-Login
login_manager = LoginManager(application)


# SQLAlchemy integration
Session = scoped_session(env.Session, scopefunc=lambda: hash(request))


@application.after_request
def after_request(resp):
    resp.headers['Access-Control-Allow-Origin'] = request.headers.get('Origin', 'yy.nuttek.com')

    resp.headers['Access-Control-Max-Age'] = 10

    return resp


def r(_):
    Session.remove()

application.teardown_request(r)

# flask-assets environment
assets = Environment(application)
assets.debug = True

# import all routes here
application.register_blueprint(libs.blueprint, url_prefix='/libs')
from . import (index, login, me, error_handler, misc, password)


# minimized css files
portal_css = Bundle(
    'lib/css/bootstrap.min.css',
    'lib/css/bootstrap-theme.min.css',
    'lib/css/bootstrap-select.min.css',
    'lib/css/bootstrap-spinner.css',
    'lib/css/bootstrap-datetimepicker.min.css',
    'lib/css/bootstrap-dialog.min.css',
    'lib/css/font-awesome.min.css',
    'lib/css/jquery.fancybox.css',
    'css/button.css',
    'css/album.css',
    filters='cssmin', output='gen/portal.css')
assets.register('portal_css', portal_css)

# minimized js files
portal_js = Bundle(
    'lib/js/jquery-1.11.3.min.js',
    'lib/js/jquery-ui.js',
    'lib/js/bootstrap.min.js',
    'lib/js/jquery.spinner.min.js',
    'lib/js/bootstrap-datetimepicker.min.js',
    'lib/js/bootstrap-datetimepicker.zh-CN.js',
    'lib/js/bootstrap-typeahead.min.js',
    'lib/js/bootstrap-dialog.min.js',
    filters='jsmin', output='gen/portal.js')
assets.register('portal_js', portal_js)
