import traceback

import requests

from nut.config import config

provider = None


class SMSProvider(object):
    def __init__(self): pass

    def sendto(self, mobile, content): pass


class ChuangLanSMSProvider(SMSProvider):
    def __init__(self):
        super().__init__()
        ip, account, pwd, pid = (config.IP,
                                 config.SMS_USER,
                                 config.SMS_PASSWORD,
                                 config.SMS_PID)

        self.url = 'http://%s/msg/HttpBatchSendSM?' \
                   'account=%s&pswd=%s&product=%s&needstatus=true' \
                   % (ip, account, pwd, pid)

    def sendto(self, mobile, content):
        data = dict(mobile=mobile, msg=content)
        res = requests.post(self.url, data)
        if res.status_code != 200 or res.text is None:
            return False

        text = res.text

        print("Server respond:" + text)
        # TODO, logging the req/res
        return text.split('\n')[0].split(',')[1] == '0'


def sent_to(mobile, content):
    if not mobile or not content:
        raise Exception("Wrong sms parameter.")

    if provider is None:
        init_provider()

    return provider.sendto(mobile, content)


def handle(mobile, content):
    # TODO: more logging
    try:
        sent_to(mobile, content)
    except:
        traceback.print_exc()
    finally:
        return [b'200']


def init_provider():
    global provider
    try:
        if provider is None:
            clazz = globals().get(config.SMS_CLASS, ChuangLanSMSProvider)
            print("Using class: %r to processing SMS." % clazz)
            provider = clazz()
    except: pass


def main():
    import argparse, sys
    parser = argparse.ArgumentParser(
        description='Using mobile number and content to send the text msg.')
    parser.add_argument('-m', default='13687300724', help='The mobile number to send')
    parser.add_argument('-c', default='We are building the fucking future',
                        help='Content using to sending the testing text msg.')

    conf = dict(ip=config.IP, user=config.SMS_USER, pwd=config.SMS_PASSWORD, pid=config.SMS_PID)

    args = parser.parse_args()

    sys.stdout.write(
        '\tSending to mobile (%s): "%s" with content "%s", ' % (conf, args.m, args.c))
    ret = sent_to(args.m, args.c)
    sys.stdout.write(" --> is success? %s" % ret)

if __name__ == '__main__':
    init_provider()
    main()