import os
import sys

from nut import config


def monkey_patch():
    import OpenSSL
    import logging
    import six

    LOG = logging.getLogger(__name__)

    from apnsclient.backends.stdio import Certificate

    def load_context(self, cert_string=None, cert_file=None, key_string=None,
                     key_file=None, passphrase=None,
                     context_method=OpenSSL.SSL.TLSv1_METHOD):
        """ Initialize and load certificate context. """
        context = OpenSSL.SSL.Context(context_method)
        if passphrase is not None and not isinstance(passphrase, six.binary_type):
            passphrase = six.b(passphrase)

        if cert_file:
            # we have to load certificate for equality check. there is no
            # other way to obtain certificate from context.
            if LOG.isEnabledFor(logging.DEBUG):
                LOG.debug("Certificate provided as file: %s", cert_file)

            with open(cert_file, 'rb') as fp:
                cert_string = fp.read()
        else:
            if LOG.isEnabledFor(logging.DEBUG):
                LOG.debug("Certificate provided as string")

        cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert_string)
        context.use_certificate(cert)

        if not key_string and not key_file:
            if LOG.isEnabledFor(logging.DEBUG):
                LOG.debug("Private key provided with certificate %s %s passphrase",
                            'file' if cert_file else 'string',
                            'with' if passphrase is not None else 'without')

            # OpenSSL is smart enought to locate private key in the certificate
            args = [OpenSSL.crypto.FILETYPE_PEM, cert_string]
            if passphrase is not None:
                args.append(passphrase)

            pk = OpenSSL.crypto.load_privatekey(*args)
            context.use_privatekey(pk)
        elif key_file and passphrase is None:
            if LOG.isEnabledFor(logging.DEBUG):
                LOG.debug("Private key provided as file without passphrase: %s", key_file)

            context.use_privatekey_file(key_file, OpenSSL.crypto.FILETYPE_PEM)
        else:
            if key_file:
                if LOG.isEnabledFor(logging.DEBUG):
                    LOG.debug("Private key provided as file withpassphrase: %s", key_file)

                # key file is provided with passphrase. context.use_privatekey_file
                # does not use passphrase, so we have to load the key file manually.
                with open(key_file, 'rb') as fp:
                    key_string = fp.read()
            else:
                if LOG.isEnabledFor(logging.DEBUG):
                    LOG.debug("Private key provided as string %s passphrase",
                                'with' if passphrase is not None else 'without')

            args = [OpenSSL.crypto.FILETYPE_PEM, key_string]
            if passphrase is not None:
                args.append(passphrase)

            pk = OpenSSL.crypto.load_privatekey(*args)
            context.use_privatekey(pk)

        # check if we are not passed some garbage
        context.check_privatekey()
        return context, cert

    Certificate.load_context = load_context

monkey_patch()

from apnsclient import Message, APNs, Session

def get_apn_client():
    if config.ENABLE_DEBUG_MODE:
        address = 'push_sandbox'
        cert_filename = config.APN_DEV_CERT_FILE
    else:
        address = 'push_production'
        cert_filename = config.APN_PRODUCTION_CERT_FILE
    cert_file = os.path.join(os.environ.get('VIRTUAL_ENV', sys.prefix),
                             'etc', 'nut-apn', 'certs',
                             cert_filename)
    session = Session()
    return APNs(session.get_connection(address, cert_file=cert_file))

def get_apn_message(token_list, alert, badge):
    if not isinstance(token_list, list):
        token_list = [token_list]
    if not alert:
        sound = ''
    else:
        sound = 'default'
    return Message(token_list, alert=alert, sound=sound, badge=badge)
