class APIError(Exception):
    def __init__(self, url, code, message, api_code, api_message):
        self.url = url
        self.code = code
        self.message = message
        self.api_code = api_code
        self.api_message = api_message
        super(APIError, self).__init__(code, message)

    def format(self, code, message):
        return '[%s]: %s, ' % (code, message) if code else ''

    def __str__(self):
        return 'Request: %s failed, %s%s' % (
            self.url,
            self.format(self.code, self.message),
            self.format(self.api_code, self.api_message))


class APIRequestError(APIError):
    def __init__(self, request, code, message, api_code=0, api_message=''):
        self.request = request
        super(APIRequestError, self).__init__(self.get_url(), code, message,
                                              api_code, api_message)

    def get_url(self):
        if self.request.body:
            return '%s?%s' % (self.request.url, self.request.body)
        return self.request.url


class APIResponseError(APIRequestError):
    def __init__(self, response, code=0, messgae='', api_code=0, api_message=''):
        self.response = response
        super(APIResponseError, self).__init__(response.request,
                                               code or response.status_code,
                                               messgae or response.text,
                                               api_code, api_message)
