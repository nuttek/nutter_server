import ssl
from requests import Session
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager

from nut.thirdparty.errors import APIResponseError


class APIHTTPAdapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections,
                                       maxsize=maxsize,
                                       block=block,
                                       ssl_version=ssl.PROTOCOL_TLSv1)

apiSession = Session()
apiSession.mount('https://', APIHTTPAdapter())


class BaseAPIClient(object):
    api_prefix = NotImplemented

    def getPage(self, path, params, api_prefix=None):
        url_prefix = api_prefix or (self.api_prefix + path)
        resp = apiSession.get(url_prefix, params=params, verify=True)
        return self.handleResponse(resp)

    def postPage(self, path, data, api_prefix=None):
        url_prefix = api_prefix or (self.api_prefix + path)
        resp = apiSession.post(url_prefix, data=data, verify=True)
        return self.handleResponse(resp)

    def handleResponse(self, resp):
        if resp.status_code != 200:
            raise APIResponseError(resp)

        if 'application/json'.upper() in resp.headers['Content-Type'].upper():
            return resp.json()
        else:
            return resp.text
