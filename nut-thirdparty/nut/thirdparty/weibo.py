from nut import config
from nut.thirdparty.base import BaseAPIClient
from nut.thirdparty.errors import APIResponseError


class WeiboAPIClient(BaseAPIClient):
    def __init__(self, access_token):
        self.api_prefix = config.SINA_WEIBO_API_PREFIX
        self.access_token = access_token
        super(WeiboAPIClient, self).__init__()

    def handleResponse(self, resp):
        data = super(WeiboAPIClient, self).handleResponse(resp)
        if 'error' in data:
            raise APIResponseError(resp, api_code=data['error'],
                                   api_message=data.get('error_code', ''))
        return data

    def getUserInfo(self, uid):
        params = {
            'access_token': self.access_token,
            'uid': uid
        }
        return self.getPage('users/show.json', params)

    def getFriendUidList(self, uid):
        params = {
            'access_token': self.access_token,
            'uid': uid,
            'count': 500
        }
        data = self.getPage('friendships/friends/bilateral/ids.json', params)
        return data.get('ids', [])
